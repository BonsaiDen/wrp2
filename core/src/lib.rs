// Features -------------------------------------------------------------------
#![feature(custom_attribute)]
#![feature(if_while_or_patterns)]


// Modules --------------------------------------------------------------------
mod client;
mod peer;
pub mod phase;
mod simulation;
mod socket;
mod traits;
pub mod util;

#[cfg(test)] mod test;


// Exports --------------------------------------------------------------------
pub use self::traits::{Input, State, StateConfig, Handler, RTCConnection, JSONSocket};
pub use self::client::{Client, Config, Event, Session};
pub use self::peer::{Peer, PeerID};
pub use self::simulation::SimulationFrame as Frame;


// Server only Features -------------------------------------------------------
#[cfg(feature="server")] mod server;
#[cfg(feature="server")] pub use self::server::Server;

