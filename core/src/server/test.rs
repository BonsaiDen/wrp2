// Internal Dependencies ------------------------------------------------------
use crate::{Config, Client};
use crate::test::{MockClientHandler, WebSocketHandler};
use crate::test::server::{MockServer, MockServerInfo};


// Tests ----------------------------------------------------------------------
#[test]
fn test_server_client_handling() {

    let mut client_one = Client::new(MockClientHandler::new(Config::default()), 0.0);
    client_one.handler.connect_web_socket();
    assert!(MockServer::urls().is_empty(), "Should not connect to server before first tick");
    client_one.tick(0.0);

    assert_eq!(MockServer::urls(), vec!["localhost/session-test".to_string()], "Should connect to server after first tick");
    assert_eq!(MockServer::info("localhost/session-test"), Some(MockServerInfo {
        logs: vec![],
        connection_count: 1
    }));

    MockServer::tick_all();

    assert_eq!(MockServer::info("localhost/session-test"), Some(MockServerInfo {
        logs: vec![
            "[Peer#0 in \"session-test\"] [Joined] (1 in session).".to_string()
        ],
        connection_count: 1
    }));

    // TODO second client

    // TODO third client fail due to id pool exhaustion

    // TODO disconnect first client

    // TODO third client

}

// TODO test client advertisment via server

