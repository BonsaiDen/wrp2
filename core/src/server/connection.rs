// STD Dependencies -----------------------------------------------------------
use std::sync::{Arc, Mutex};
use std::collections::{HashMap, VecDeque};


// External Dependencies ------------------------------------------------------
use rand::prelude::*;


// Internal Dependencies ------------------------------------------------------
use crate::util::Sum;
use crate::peer::PeerID;
use crate::socket::{SocketRequest, SocketResponse};


// Statics --------------------------------------------------------------------
static VALID_GROUP_CHARS: &str = "abcdefgihjklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-";


// Traits ---------------------------------------------------------------------
pub trait ConnectionSocket {
    fn send_response(&mut self, response: SocketResponse);
    fn log(&self, msg: &str);
}


// Implementation -------------------------------------------------------------
pub struct ConnectionHandler<T: ConnectionSocket> {
    sessions: Arc<Mutex<HashMap<String, Vec<SessionPeer<T>>>>>,
    id_pool: Arc<Mutex<VecDeque<PeerID>>>
}

impl<T: ConnectionSocket> ConnectionHandler<T> {

    pub(crate) fn new(max_connections: u16) -> Self {
        let mut rng = rand::thread_rng();
        let mut pool: Vec<PeerID> = (0..max_connections).map(PeerID).collect();
        pool.shuffle(&mut rng);
        Self::with_id_pool(pool)
    }

    pub(crate) fn with_id_pool(id_pool: Vec<PeerID>) -> Self {
        Self {
            sessions: Arc::new(Mutex::new(HashMap::new())),
            id_pool: Arc::new(Mutex::new(VecDeque::from(id_pool)))
        }
    }

    pub(crate) fn add_socket(&mut self, socket: T) -> Connection<T> {
        if let Some(id) = self.id_pool.lock().expect("ID Pool poisoned").pop_front() {
            Connection::pending(socket, id, self.id_pool.clone(), self.sessions.clone())

        } else {
            Connection::failed()
        }
    }

}

struct SessionPeer<T: ConnectionSocket> {
    socket: T,
    id: PeerID
}

pub enum Connection<T: ConnectionSocket> {
    Pending(PendingHandle<T>),
    Active(ConnectedHandle<T>),
    Failed
}

impl<T: ConnectionSocket> Connection<T> {

    fn pending(
        socket: T,
        id: PeerID,
        id_pool: Arc<Mutex<VecDeque<PeerID>>>,
        sessions: Arc<Mutex<HashMap<String, Vec<SessionPeer<T>>>>>

    ) -> Self {
        Connection::Pending(PendingHandle::new(socket, id, id_pool, sessions))
    }

    fn failed() -> Self {
        Connection::Failed
    }

    pub(crate) fn connect(&mut self, url: &str) -> Result<(), String> {
        match self {
            Connection::Pending(s) => {
                let session_id = Self::session_id_from_url(url)?;
                *self = Connection::Active(s.connect(session_id)?);
                Ok(())
            },
            Connection::Active(_) => Ok(()),
            Connection::Failed => Err("Maximum number of peers exceeded".to_string())
        }
    }

    pub(crate) fn message(&mut self, text: &str) -> Result<(), String> {
        match self {
            Connection::Pending(_) => Ok(()),
            Connection::Active(s) => s.message(text),
            Connection::Failed => Err("Maximum number of peers exceeded".to_string())
        }
    }

    pub(crate) fn close(&mut self) {
        match self {
            Connection::Active(s) => s.close(),
            Connection::Pending(_) | Connection::Failed => {}
        }
        *self = Connection::Failed;
    }

    fn session_id_from_url(url: &str) -> Result<String, String> {
        let mut parts = url.split('/');
        parts.next();// Skip initial slash

        if let Some(session_id) = parts.next() {
            let session_id = session_id.replace(|c| !VALID_GROUP_CHARS.contains(c), "");
            if session_id.len() >= 3 {
                return Ok(session_id.to_string());
            }
        }

        Err("Invalid session id".into())
    }

}

pub struct PendingHandle<T: ConnectionSocket> {
    id: PeerID,
    socket: Option<T>,
    id_pool: Arc<Mutex<VecDeque<PeerID>>>,
    sessions: Arc<Mutex<HashMap<String, Vec<SessionPeer<T>>>>>
}

impl<T: ConnectionSocket> PendingHandle<T> {

    fn new(
        socket: T,
        id: PeerID,
        id_pool: Arc<Mutex<VecDeque<PeerID>>>,
        sessions: Arc<Mutex<HashMap<String, Vec<SessionPeer<T>>>>>

    ) -> Self {
        Self {
            id,
            socket: Some(socket),
            id_pool,
            sessions
        }
    }

    fn connect(&mut self, session_id: String) -> Result<ConnectedHandle<T>, String> {
        if let Ok(mut sessions) = self.sessions.lock() {

            let socket = self.socket.take().unwrap();
            let peers = sessions.entry(session_id.clone()).or_insert_with(Vec::new);
            let handle = ConnectedHandle {
                id: self.id,
                session_id,
                seed: random(),
                bytes_incoming: Sum::new(8, 0),
                id_pool: self.id_pool.clone(),
                sessions: self.sessions.clone()
            };

            handle.log(&socket, &format!("[Joined] ({} in session).", peers.len() + 1));
            peers.push(SessionPeer {
                socket,
                id: self.id
            });

            Ok(handle)

        } else {
            Err("Connection failed".to_string())
        }

    }

}

pub struct ConnectedHandle<T: ConnectionSocket> {
    id: PeerID,
    session_id: String,
    seed: u32,
    bytes_incoming: Sum<u32>,
    id_pool: Arc<Mutex<VecDeque<PeerID>>>,
    sessions: Arc<Mutex<HashMap<String, Vec<SessionPeer<T>>>>>
}

impl<T: ConnectionSocket> ConnectedHandle<T> {

    fn message(&mut self, text: &str) -> Result<(), String> {
        if let Ok(mut sessions) = self.sessions.lock() {

            let peers = sessions.get_mut(&self.session_id).ok_or_else(||
                "Connection failed".to_string()
            )?;

            let request = SocketRequest::from_str(&text).map_err(|_|
                "Connection failed".to_string()
            )?;

            // Limit incoming bandwidth...
            self.bytes_incoming.push(text.len() as u32);

            // ... and close connection in case it exceeds the limit
            if self.bytes_incoming.get() > 16384 {
                Err("Bandwidth exceeded".to_string())

            } else {
                self.reply(self.id, peers, request);
                Ok(())
            }

        } else {
            Ok(())
        }
    }

    fn reply(&self, id: PeerID, peers: &mut Vec<SessionPeer<T>>, request: SocketRequest) {

        let source_index = peers.iter().position(|c| c.id == id).unwrap_or(256) as u32;
        let peer_count = peers.len();
        for (peer_index, peer) in peers.iter_mut().enumerate() {

            if let Some(peer) = match request.to {
                Some(to_id) if to_id == peer.id => {
                    self.log(&peer.socket, &format!("[Message] To Peer#{}", to_id));
                    Some(peer)
                },
                _ => {
                    if peer_index == 0 {
                        self.log(&peer.socket, &format!("[Broadcast] ({} in session)", peer_count));
                    }
                    Some(peer)
                }
            } {
                peer.socket.send_response(SocketResponse {
                    local: id == peer.id,
                    from: id,
                    index: source_index,
                    seed: self.seed,
                    request: request.clone()
                });
            }
        }

    }

    fn close(&mut self) {
        if let Ok(mut sessions) = self.sessions.lock() {
            if let Some(peers) = sessions.get_mut(&self.session_id) {
                let peer_count = peers.len();
                peers.retain(|c| {
                    if c.id == self.id {
                        self.log(&c.socket, &format!("[Left] ({} in session).", peer_count - 1));
                        false

                    } else {
                        true
                    }
                });
            }
        }
    }

    fn log(&self, socket: &T, msg: &str) {
        socket.log(&format!("[Peer#{} in \"{}\"] {}", self.id, self.session_id, msg));
    }

}

impl<T: ConnectionSocket> Drop for ConnectedHandle<T> {
    fn drop(&mut self) {
        self.id_pool.lock().expect("ID Pool poisoned").push_back(self.id)
    }
}

