// STD Dependencies -----------------------------------------------------------
use std::fmt::Debug;
use std::net::ToSocketAddrs;


// External Dependencies ------------------------------------------------------
use chrono;
use ws::{Builder, Error, Factory, Sender, Message, Settings, CloseCode, Handshake, Handler, ErrorKind};


// Internal Dependencies ------------------------------------------------------
use crate::socket::SocketResponse;


// Modules --------------------------------------------------------------------
pub mod connection;
use self::connection::{Connection, ConnectionHandler, ConnectionSocket};

#[cfg(test)] mod test;

// WebSocekt Server Implementation --------------------------------------------
struct ServerFactory {
    manager: ConnectionHandler<Sender>
}

impl ServerFactory {
    fn new(max_connections: u16) -> Self {
        Self {
            manager: ConnectionHandler::new(max_connections)
        }
    }
}

impl Factory for ServerFactory {

    type Handler = Connection<Sender>;

    fn connection_made(&mut self, socket: Sender) -> Self::Handler {
        println!("[{}] [TCP] Incoming connection", chrono::Local::now());
        self.manager.add_socket(socket)
    }

}

impl ConnectionSocket for Sender {

    fn send_response(&mut self, response: SocketResponse) {
        self.send(Message::Text(response.to_string())).ok();
    }

    fn log(&self, msg: &str) {
        println!("[{}] {}", chrono::Local::now(), msg);
    }

}

impl<T: ConnectionSocket> Handler for Connection<T> {

    fn on_open(&mut self, shake: Handshake) -> Result<(), Error> {
        self.connect(shake.request.resource()).map_err(|e| Error {
            kind: ErrorKind::Protocol,
            details: e.into()
        })
    }

    fn on_message(&mut self, msg: Message) -> Result<(), Error> {
        if let Message::Text(ref text) = msg {
            self.message(text).map_err(|e| Error {
                kind: ErrorKind::Protocol,
                details: e.into()
            })

        } else {
            Err(Error {
                kind: ErrorKind::Protocol,
                details: "Unsupported message type".into()
            })
        }
    }

    fn on_close(&mut self, _: CloseCode, _: &str) {
        self.close()
    }

}


// Public API -----------------------------------------------------------------
pub struct Server;
impl Server {
    pub fn run<A: ToSocketAddrs + Debug>(addr: A, max_connections: u16) -> Result<(), Error> {
        println!("[{}] Server started", chrono::Local::now());
        Builder::new().with_settings(Settings {
            tcp_nodelay: true,
            ..Default::default()

        }).build(ServerFactory::new(max_connections))?.listen(addr)?;
        Ok(())
    }
}

