// Internal Dependencies ------------------------------------------------------
use crate::{simulation::FrameID, peer::PeerID, traits::State};


// Traits ---------------------------------------------------------------------
pub(crate) trait SimulationState<S: State> {
    fn create(frame: (FrameID, FrameID), frame_speed: f64, state: S) -> ClientPhase<S>;
}


// Phase Encapsulation --------------------------------------------------------
#[derive(Debug, Clone, PartialEq)]
pub(crate) enum ClientPhase<S: State> {
    Disconnected,
    Connecting(Connecting::State<S>),
    Hosting(Hosting::State<S>),
    Peering(Peering::State<S>),
    Started(Started::State<S>),
    Starting(Starting::State<S>),
    ConnectionLost,
    Disconnecting,
    SimulatingOnline(SimulatingOnline::State<S>),
    SimulatingOffline(SimulatingOffline::State<S>),
    PausedOffline(PausedOffline::State<S>),
    Synchronizing(Synchronizing::State),
    SynchronisationLost(SynchronisationLost::State)
}

impl<S: State> ClientPhase<S> {

    pub(crate) fn hosting_id() -> u8 {
        2
    }

    pub(crate) fn peering_id() -> u8 {
        3
    }

    pub(crate) fn id(&self) -> u8 {
        match self {
            ClientPhase::Disconnected => 0,
            ClientPhase::Connecting(_) => 1,
            ClientPhase::Hosting(_) => Self::hosting_id(),
            ClientPhase::Peering(_) => Self::peering_id(),
            ClientPhase::Started(_) => 4,
            ClientPhase::Starting(_) => 5,
            ClientPhase::ConnectionLost => 6,
            ClientPhase::Disconnecting => 7,
            ClientPhase::SimulatingOnline(_) => 8,
            ClientPhase::SimulatingOffline(_) => 9,
            ClientPhase::PausedOffline(_) => 10,
            ClientPhase::Synchronizing(_) => 11,
            ClientPhase::SynchronisationLost(_) => 12
        }
    }

}


// Macros ---------------------------------------------------------------------
macro_rules! phase_trait_empty {
    ($command:ty) => {

        use crate::{Handler, Peer};

        #[allow(unused_variables)]
        pub trait Phase: Handler {
            fn enter(&mut self, peers: &[Peer<Self>], tick_time: f64) {}
            fn tick(&mut self, peers: &[Peer<Self>], tick_time: f64) -> $command;
            fn draw(&mut self, peers: &[Peer<Self>], draw_time: f64, t: f32);
            fn leave(&mut self, peers: &[Peer<Self>], tick_time: f64) {}
        }

    }
}

macro_rules! phase_trait {
    ($command:ty) => {

        use crate::{Handler, Peer};

        #[allow(unused_variables)]
        pub trait Phase: Handler {
            fn enter(&mut self, phase: &State, peers: &[Peer<Self>], tick_time: f64) {}
            fn tick(&mut self, phase: &State, peers: &[Peer<Self>], tick_time: f64) -> $command;
            fn draw(&mut self, phase: &State, peers: &[Peer<Self>], draw_time: f64, t: f32);
            fn leave(&mut self, phase: &State, peers: &[Peer<Self>], tick_time: f64) {}
        }

    }
}

macro_rules! phase_trait_state {
    ($command:ty) => {

        use crate::{Handler, Peer};

        #[allow(unused_variables)]
        pub trait Phase<S: HandlerState>: Handler {
            fn enter(&mut self, phase: &State<S>, peers: &[Peer<Self>], tick_time: f64) {}
            fn tick(&mut self, phase: &State<S>, peers: &[Peer<Self>], tick_time: f64) -> $command;
            fn draw(&mut self, phase: &State<S>, peers: &[Peer<Self>], draw_time: f64, t: f32);
            fn leave(&mut self, phase: &State<S>, peers: &[Peer<Self>], tick_time: f64) {}
        }

    }
}



// Enums -----------------------------------------------------------------------
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum SyncError {
    HashTimeout(u64),
    HashMismatch(u64, PeerID),
    Timeout
}


// Phase Implementation Modules -----------------------------------------------
#[allow(non_snake_case)]
pub mod Disconnected {

    use crate::traits::State as HandlerState;

    #[derive(Debug, Clone, PartialEq)]
    pub struct State;

    #[must_use]
    #[derive(Debug, Clone, PartialEq)]
    pub enum Command<S: HandlerState> {
        Connect {
            host: String,
            session_id: String
        },
        Start(S::Config)
    }

    phase_trait_empty!(Option<Command<Self::State>>);

}

#[allow(non_snake_case)]
pub mod Connecting {

    pub use crate::Session as State;
    use crate::traits::{State as HandlerState, StateConfig};

    #[must_use]
    #[derive(Debug, Clone, PartialEq)]
    pub enum Command {
        Disconnect
    }

    pub(crate) fn new<S: HandlerState>(
        host: String,
        session_id: String

    ) -> super::ClientPhase<S> {
        let state_config = S::Config::default();
        super::ClientPhase::Connecting(
            State {
                host,
                id: session_id,
                state_hash: state_config.hash_value(),
                state_config
            }
        )
    }

    phase_trait_state!(Option<Command>);

}

#[allow(non_snake_case)]
pub mod Hosting {

    pub use crate::Session as State;
    use crate::traits::State as HandlerState;

    #[must_use]
    #[derive(Debug, Clone, PartialEq)]
    pub enum Command<S: HandlerState> {
        Start,
        SetConfig(S::Config),
        Disconnect
    }

    pub(crate) fn from_session<S: HandlerState>(session: State<S>) -> super::ClientPhase<S> {
        super::ClientPhase::Hosting(session)
    }

    phase_trait_state!(Option<Command<S>>);

}

#[allow(non_snake_case)]
pub mod Peering {

    pub use crate::Session as State;
    use crate::traits::State as HandlerState;

    #[must_use]
    #[derive(Debug, Clone, PartialEq)]
    pub enum Command<S: HandlerState> {
        RequestConfig(S::Config),
        Disconnect
    }

    pub(crate) fn from_session<S: HandlerState>(session: State<S>) -> super::ClientPhase<S> {
        super::ClientPhase::Peering(session)
    }

    phase_trait_state!(Option<Command<S>>);

}

#[allow(non_snake_case)]
pub mod Started {

    pub use crate::Session as State;
    use crate::traits::State as HandlerState;

    pub(crate) fn from_session<S: HandlerState>(session: State<S>) -> super::ClientPhase<S> {
        super::ClientPhase::Started(session)
    }

    #[must_use]
    #[derive(Debug, Clone, PartialEq)]
    pub enum Command {
        Disconnect
    }

    phase_trait_state!(Option<Command>);

}

#[allow(non_snake_case)]
pub mod Starting {

    pub use crate::Session as State;
    use crate::traits::State as HandlerState;

    pub(crate) fn from_session<S: HandlerState>(session: State<S>) -> super::ClientPhase<S> {
        super::ClientPhase::Starting(session)
    }

    #[must_use]
    #[derive(Debug, Clone, PartialEq)]
    pub enum Command {
        Disconnect
    }

    phase_trait_state!(Option<Command>);

}

#[allow(non_snake_case)]
pub mod ConnectionLost {

    #[derive(Debug, Clone, PartialEq)]
    pub struct State;

    #[must_use]
    #[derive(Debug, Clone, PartialEq)]
    pub enum Command {
        Connect {
            host: String,
            session_id: String
        },
        Reset
    }

    phase_trait_empty!(Option<Command>);

}

#[allow(non_snake_case)]
pub mod Disconnecting {

    #[derive(Debug, Clone, PartialEq)]
    pub struct State;

    phase_trait_empty!(());

}

#[allow(non_snake_case)]
pub mod SimulatingOnline {

    use std::collections::HashSet;

    use super::SimulationState;
    use crate::{peer::PeerID, simulation::FrameID, traits::State as HandlerState};

    #[derive(Debug, Clone, PartialEq)]
    pub struct State<S: HandlerState> {
        pub frame: (FrameID, FrameID), // Committed / Simulated
        pub frame_deviation: f64,
        pub frame_advantage: f32,
        pub frame_speed: f64,
        pub state: S,
        pub peers_slow: HashSet<PeerID>
    }

    impl<S: HandlerState> SimulationState<S> for State<S> {
        fn create(
            frame: (FrameID, FrameID),
            frame_speed: f64,
            state: S

        ) -> super::ClientPhase<S> {
            super::ClientPhase::SimulatingOnline(State {
                frame,
                frame_advantage: 0.0,
                frame_deviation: 0.0,
                frame_speed,
                state,
                peers_slow: HashSet::new()
            })
        }
    }


    #[must_use]
    #[derive(Debug, Clone, PartialEq)]
    pub enum Command {
        Disconnect
    }

    phase_trait_state!(Option<Command>);

}

#[allow(non_snake_case)]
pub mod SimulatingOffline {

    use super::SimulationState;
    use crate::{simulation::FrameID, traits::State as HandlerState};

    #[derive(Debug, Clone, PartialEq)]
    pub struct State<S: HandlerState> {
        pub frame: (FrameID, FrameID), // Committed / Simulated
        pub state: S
    }

    impl<S: HandlerState> SimulationState<S> for State<S> {
        fn create(
            frame: (FrameID, FrameID),
            _: f64,
            state: S

        ) -> super::ClientPhase<S> {
            super::ClientPhase::SimulatingOffline(State {
                frame,
                state
            })
        }
    }

    #[must_use]
    #[derive(Debug, Clone, PartialEq)]
    pub enum Command {
        Pause,
        Stop
    }

    phase_trait_state!(Option<Command>);

}

#[allow(non_snake_case)]
pub mod PausedOffline {

    use crate::{simulation::FrameID, traits::State as HandlerState};

    #[derive(Debug, Clone, PartialEq)]
    pub struct State<S: HandlerState> {
        pub frame: (FrameID, FrameID), // Committed / Simulated
        pub state: S
    }

    #[must_use]
    #[derive(Debug, Clone, PartialEq)]
    pub enum Command {
        Resume,
        Stop
    }

    phase_trait_state!(Option<Command>);

}

#[allow(non_snake_case)]
pub mod Synchronizing {

    use std::collections::HashSet;
    use crate::peer::PeerID;
    use crate::traits::State as HandlerState;

    #[derive(Debug, Clone, PartialEq)]
    pub struct State {
        pub peers_ready: HashSet<PeerID>
    }

    pub(crate) fn new<S: HandlerState>() -> super::ClientPhase<S> {
        super::ClientPhase::Synchronizing(State {
            peers_ready: HashSet::new()
        })
    }

    phase_trait!(());

}

#[allow(non_snake_case)]
pub mod SynchronisationLost {

    use crate::{simulation::FrameID, phase::SyncError};
    use crate::traits::State as HandlerState;

    #[derive(Debug, Clone, PartialEq)]
    pub struct State {
        pub frame: FrameID,
        pub reason: SyncError
    }

    pub(crate) fn new<S: HandlerState>(frame: FrameID, reason: SyncError) -> super::ClientPhase<S> {
        super::ClientPhase::SynchronisationLost(State {
            frame,
            reason
        })
    }

    #[must_use]
    #[derive(Debug, Clone, PartialEq)]
    pub enum Command {
        Reset
    }

    phase_trait!(Option<Command>);

}

