// STD Dependencies -----------------------------------------------------------
use std::error::Error;
use std::marker::PhantomData;


// External Dependencies ------------------------------------------------------
use serde_derive::{Deserialize, Serialize};


// Internal Dependencies ------------------------------------------------------
use crate::simulation::SimulationFrame;
use crate::socket::SocketRequest;
use crate::phase::{
    Disconnected,
    Connecting,
    Hosting,
    Peering,
    Started,
    Starting,
    ConnectionLost,
    Disconnecting,
    SimulatingOnline,
    SimulatingOffline,
    PausedOffline,
    Synchronizing,
    SynchronisationLost
};

use crate::{
    Config,
    Event,
    Peer,
    Handler,
    Input,
    State,
    StateConfig,
    JSONSocket,
    PeerID,
    RTCConnection,
    phase,
    phase::ClientPhase
};


// Modules --------------------------------------------------------------------
pub mod server;


// Macros ---------------------------------------------------------------------
macro_rules! test_phase_impl_empty_command {
    ($handler:ty, $phase:ident, $command_prop:ident, $enum:expr, $command_type:ty) => {

        impl $phase::Phase for $handler {

            fn enter(&mut self, _: &[Peer<Self>], tick_time: f64) {
                self.transitions.push(StateTransition::Enter($enum, tick_time));
            }

            fn tick(&mut self, _: &[Peer<Self>], tick_time: f64) -> Option<$phase::Command<$command_type>> {
                self.ticks.push(tick_time);
                self.command.$command_prop.take()
            }

            fn draw(&mut self, _: &[Peer<Self>], _: f64, _: f32) {
            }

            fn leave(&mut self, _: &[Peer<Self>], tick_time: f64) {
                self.transitions.push(StateTransition::Leave($enum, tick_time));
            }

        }

    }
}

macro_rules! test_phase_impl_empty {
    ($handler:ty, $phase:ident, $command_prop:ident, $enum:expr) => {

        impl $phase::Phase for $handler {

            fn enter(&mut self, _: &[Peer<Self>], tick_time: f64) {
                self.transitions.push(StateTransition::Enter($enum, tick_time));
            }

            fn tick(&mut self, _: &[Peer<Self>], tick_time: f64) -> Option<$phase::Command> {
                self.ticks.push(tick_time);
                self.command.$command_prop.take()
            }

            fn draw(&mut self, _: &[Peer<Self>], _: f64, _: f32) {
            }

            fn leave(&mut self, _: &[Peer<Self>], tick_time: f64) {
                self.transitions.push(StateTransition::Leave($enum, tick_time));
            }

        }

    }
}

macro_rules! test_phase_impl_no_command_empty {
    ($handler:ty, $phase:ident, $command_prop:ident, $enum:expr) => {

        impl $phase::Phase for $handler {

            fn enter(&mut self, _: &[Peer<Self>], tick_time: f64) {
                self.transitions.push(StateTransition::Enter($enum, tick_time));
            }

            fn tick(&mut self, _: &[Peer<Self>], tick_time: f64) {
                self.ticks.push(tick_time);
            }

            fn draw(&mut self, _: &[Peer<Self>], _: f64, _: f32) {
            }

            fn leave(&mut self, _: &[Peer<Self>], tick_time: f64) {
                self.transitions.push(StateTransition::Leave($enum, tick_time));
            }

        }

    }
}

macro_rules! test_phase_impl {
    ($handler:ty, $phase:ident, $command_prop:ident, $enum:expr) => {

        impl $phase::Phase for $handler {

            fn enter(&mut self, phase: &$phase::State, _: &[Peer<Self>], tick_time: f64) {
                self.transitions.push(StateTransition::Enter($enum(phase.clone()), tick_time));
            }

            fn tick(&mut self, _: &$phase::State, _: &[Peer<Self>], tick_time: f64) -> Option<$phase::Command> {
                self.ticks.push(tick_time);
                self.command.$command_prop.take()
            }

            fn draw(&mut self, _: &$phase::State, _: &[Peer<Self>], _: f64, _: f32) {
            }

            fn leave(&mut self, phase: &$phase::State, _: &[Peer<Self>], tick_time: f64) {
                self.transitions.push(StateTransition::Leave($enum(phase.clone()), tick_time));
            }

        }

    }
}

macro_rules! test_phase_impl_state {
    ($handler:ty, $phase:ident, $state:ty, $command_prop:ident, $enum:expr) => {

        impl $phase::Phase<$state> for $handler {

            fn enter(&mut self, phase: &$phase::State<$state>, _: &[Peer<Self>], tick_time: f64) {
                self.transitions.push(StateTransition::Enter($enum(phase.clone()), tick_time));
            }

            fn tick(&mut self, _: &$phase::State<$state>, _: &[Peer<Self>], tick_time: f64) -> Option<$phase::Command> {
                self.ticks.push(tick_time);
                self.command.$command_prop.take()
            }

            fn draw(&mut self, _: &$phase::State<$state>, _: &[Peer<Self>], _: f64, _: f32) {
            }

            fn leave(&mut self, phase: &$phase::State<$state>, _: &[Peer<Self>], tick_time: f64) {
                self.transitions.push(StateTransition::Leave($enum(phase.clone()), tick_time));
            }

        }

    }
}

macro_rules! test_phase_impl_state_command {
    ($handler:ty, $phase:ident, $state:ty, $command_prop:ident, $enum:expr) => {

        impl $phase::Phase<$state> for $handler {

            fn enter(&mut self, phase: &$phase::State<$state>, _: &[Peer<Self>], tick_time: f64) {
                self.transitions.push(StateTransition::Enter($enum(phase.clone()), tick_time));
            }

            fn tick(&mut self, _: &$phase::State<$state>, _: &[Peer<Self>], tick_time: f64) -> Option<$phase::Command<$state>> {
                self.ticks.push(tick_time);
                self.command.$command_prop.take()
            }

            fn draw(&mut self, _: &$phase::State<$state>, _: &[Peer<Self>], _: f64, _: f32) {
            }

            fn leave(&mut self, phase: &$phase::State<$state>, _: &[Peer<Self>], tick_time: f64) {
                self.transitions.push(StateTransition::Leave($enum(phase.clone()), tick_time));
            }

        }

    }
}

macro_rules! test_phase_impl_no_command {
    ($handler:ty, $phase:ident, $command_prop:ident, $enum:expr) => {

        impl $phase::Phase for $handler {

            fn enter(&mut self, phase: &$phase::State, _: &[Peer<Self>], tick_time: f64) {
                self.transitions.push(StateTransition::Enter($enum(phase.clone()), tick_time));
            }

            fn tick(&mut self, _: &$phase::State, _: &[Peer<Self>], tick_time: f64) {
                self.ticks.push(tick_time);
            }

            fn draw(&mut self, _: &$phase::State, _: &[Peer<Self>], _: f64, _: f32) {
            }

            fn leave(&mut self, phase: &$phase::State, _: &[Peer<Self>], tick_time: f64) {
                self.transitions.push(StateTransition::Leave($enum(phase.clone()), tick_time));
            }

        }

    }
}

#[macro_export]
macro_rules! test_phase_all_impl {
    ($handler:ty, $state:ty) => {
        test_phase_impl_empty_command!($handler, Disconnected, disconnected, ClientPhase::Disconnected, $state);
        test_phase_impl_state!($handler, Connecting, $state, connecting, ClientPhase::Connecting);
        test_phase_impl_state_command!($handler, Hosting, $state, hosting, ClientPhase::Hosting);
        test_phase_impl_state_command!($handler, Peering, $state, peering, ClientPhase::Peering);
        test_phase_impl_state!($handler, Started, $state, started, ClientPhase::Started);
        test_phase_impl_state!($handler, Starting, $state, starting, ClientPhase::Starting);
        test_phase_impl_empty!($handler, ConnectionLost, connection_lost, ClientPhase::ConnectionLost);
        test_phase_impl_no_command_empty!($handler, Disconnecting, disconnecting, ClientPhase::Disconnecting);

        test_phase_impl_state!($handler, SimulatingOnline, $state, simulating_online, ClientPhase::SimulatingOnline);
        test_phase_impl_state!($handler, SimulatingOffline, $state, simulating_offline, ClientPhase::SimulatingOffline);
        test_phase_impl_state!($handler, PausedOffline, $state, paused_offline, ClientPhase::PausedOffline);
        test_phase_impl_no_command!($handler, Synchronizing, synchronizing, ClientPhase::Synchronizing);
        test_phase_impl!($handler, SynchronisationLost, synchronisation_lost, ClientPhase::SynchronisationLost);
    }
}


// Mock Implementations used for Testing --------------------------------------
pub trait PacketConnection {
    fn set_received(&mut self, packets: Vec<Vec<u8>>);
    fn sent(&self) -> &[Vec<u8>];
    fn clear_sent(&mut self);
}

pub struct TestConnection {
    pub id: String,
    pub initiator: bool,
    pub offer: u32,
    pub connected: bool,
    pub received_signals: Vec<String>,
    pub packets_sent: Vec<Vec<u8>>,
    pub packets_received: Vec<Vec<u8>>
}

impl PacketConnection for TestConnection {

    fn set_received(&mut self, packets: Vec<Vec<u8>>) {
        self.packets_received = packets;
    }

    fn sent(&self) -> &[Vec<u8>] {
        &self.packets_sent
    }

    fn clear_sent(&mut self) {
        self.packets_sent.clear();
    }

}

impl RTCConnection for TestConnection {

    fn new(id: String, initiator: bool) -> Self {
        Self {
            id,
            initiator,
            offer: 0,
            connected: false,
            received_signals: Vec::new(),
            packets_sent: Vec::new(),
            packets_received: Vec::new()
        }
    }

    fn send(&mut self, packet: Vec<u8>) {
        self.packets_sent.push(packet);
    }

    fn signal(&mut self, signal: String) {
        self.received_signals.push(signal);
    }

    fn is_connected(&self) -> bool {
        self.connected
    }

    fn was_disconnected(&self) -> bool {
        false
    }

    fn signals(&mut self) -> Option<Vec<String>> {
        if self.connected {
            None

        } else if self.initiator {
            self.offer += 1;
            Some(vec![format!("peer-signal-offer-{}", self.offer)])

        } else {
            Some(self.received_signals.clone())
        }
    }

    fn receive(&mut self) -> Option<Vec<Vec<u8>>> {
        if self.packets_received.is_empty() {
            None

        } else {
            Some(self.packets_received.drain(0..).collect())
        }
    }

}

pub trait TestHandlerSocket {
    fn set_received(&mut self, received: Option<Vec<String>>);
}

pub struct TestSocket {
    pub url: String,
    pub connected: bool,
    pub(crate) requests: Vec<SocketRequest>,
    pub received: Option<Vec<String>>
}

impl TestHandlerSocket for TestSocket {
    fn set_received(&mut self, received: Option<Vec<String>>) {
        self.received = received;
    }
}

impl JSONSocket for TestSocket {
    fn try_new(url: &str) -> Result<Self, Box<Error>> where Self: Sized {
        Ok(TestSocket {
            url: url.to_string(),
            connected: true,
            requests: Vec::new(),
            received: None
        })
    }

    fn close(&mut self) {
        self.connected = false;
    }

    fn send(&mut self, text: String) {
        if let Ok(request) = serde_json::from_str::<SocketRequest>(&text) {
            self.requests.push(request);
        }
    }

    fn receive(&mut self) -> Vec<String> {
        if let Some(messages) = self.received.take() {
            messages

        } else {
            Vec::new()
        }
    }

    fn is_open(&self) -> bool {
        self.connected
    }

    fn is_closed(&self) -> bool {
        !self.connected
    }

}

#[derive(Debug, Default, Copy, Clone, PartialEq, Eq, Hash)]
pub struct TestInput(pub u8);
impl Input for TestInput {

    fn predict(previous: Option<TestInput>) -> TestInput {
        match previous {
            Some(i) => i,
            _ => TestInput(0)
        }
    }

    fn into_byte(self) -> u8 {
        self.0
    }

    fn from_byte(byte: u8) -> Self {
        TestInput(byte)
    }

}

#[derive(Debug, Default, Clone, Hash, Eq, PartialEq, Serialize, Deserialize)]
pub struct TestStateConfig {
    pub value: u32
}
impl StateConfig for TestStateConfig {

    fn merge(&self, other: Self, peer_id: PeerID) -> Self {
        Self {
            value: 5 * other.value + (peer_id.0 as u32) * 1000
        }
    }

}

pub trait StateFromHandler: State {
    fn from_handler(config: Self::Config, peers: Vec<PeerID>) -> Self;
}

#[derive(Debug, Clone, Hash, Eq, PartialEq)]
pub struct TestState {
    pub id: u32,
    pub v: u32
}

impl State for TestState {
    type Input = TestInput;
    type Config = TestStateConfig;

    fn next(&self, frame: &SimulationFrame<TestInput>) -> Self {
        let mut v = self.v;
        for (_, i) in frame.inputs() {
            if let Some(i) = i {
                v += i.into_byte() as u32;
            }
        }
        Self {
            id: self.id + 1,
            v
        }
    }

    fn hash_value(&self) -> u64 {
        0
    }

}

impl StateFromHandler for TestState {
    fn from_handler(_: Self::Config, _: Vec<PeerID>) -> Self {
        TestState {
            id: 0,
            v: 0
        }
    }
}

#[derive(Debug, Clone, Hash, Eq, PartialEq)]
pub struct TestInputState {
    pub frames: Vec<u32>,
    pub inputs: Vec<(u32, PeerID, Option<u32>)>
}

impl State for TestInputState {
    type Input = TestInput;
    type Config = TestStateConfig;

    fn next(&self, frame: &SimulationFrame<TestInput>) -> Self {
        let mut frames = self.frames.clone();
        let mut inputs = self.inputs.clone();
        for (id, i) in frame.inputs() {
            if let Some(i) = i {
                inputs.push((frame.id(), id.clone(), Some(i.into_byte() as u32)));

            } else {
                inputs.push((frame.id(), id.clone(), None));
            }
        }
        frames.push(frame.id());
        Self {
            frames,
            inputs
        }
    }

    fn hash_value(&self) -> u64 {
        0
    }

}

impl StateFromHandler for TestInputState {
    fn from_handler(_: Self::Config, _: Vec<PeerID>) -> Self {
        TestInputState {
            frames: Vec::new(),
            inputs: Vec::new()
        }
    }
}

#[derive(Debug, Clone, Hash, Eq, PartialEq)]
pub struct TestConfigState {
    pub config: TestStateConfig,
    pub peer_ids: Vec<PeerID>
}

impl State for TestConfigState {
    type Input = TestInput;
    type Config = TestStateConfig;

    fn next(&self, _: &SimulationFrame<TestInput>) -> Self {
        self.clone()
    }

    fn hash_value(&self) -> u64 {
        0
    }
}

impl StateFromHandler for TestConfigState {
    fn from_handler(config: Self::Config, peer_ids: Vec<PeerID>) -> Self {
        TestConfigState {
            config,
            peer_ids
        }
    }
}

#[derive(Debug, PartialEq)]
pub(crate) enum StateTransition<S: State> {
    Enter(ClientPhase<S>, f64),
    Leave(ClientPhase<S>, f64)
}

pub struct Command<S: State> {
    pub disconnected: Option<Disconnected::Command<S>>,
    pub connecting: Option<Connecting::Command>,
    pub hosting: Option<Hosting::Command<S>>,
    pub peering: Option<Peering::Command<S>>,
    pub started: Option<Started::Command>,
    pub starting: Option<Starting::Command>,
    pub connection_lost: Option<ConnectionLost::Command>,
    //disconnecting: Option<()>,
    pub simulating_online: Option<SimulatingOnline::Command>,
    pub simulating_offline: Option<SimulatingOffline::Command>,
    pub paused_offline: Option<PausedOffline::Command>,
    //synchronizing: Option<()>,
    pub synchronisation_lost: Option<SynchronisationLost::Command>
}

impl<S: State> Command<S> {
    fn default() -> Self {
        Self {
            disconnected: None,
            connecting: None,
            hosting: None,
            peering: None,
            started: None,
            starting: None,
            connection_lost: None,
            //disconnecting: None,
            simulating_online: None,
            simulating_offline: None,
            paused_offline: None,
            //synchronizing: None,
            synchronisation_lost: None,
        }
    }
}

pub trait WebSocketHandler {
    fn connect_web_socket(&mut self);
}

pub(crate) struct BaseTestHandler<S: State + StateFromHandler, O: JSONSocket> {
    socket: PhantomData<O>,
    config: Config,
    pub command: Command<S>,
    pub applied: Vec<(u32, S, f64)>,
    pub reverted: Vec<(u32, S, f64)>,
    pub committed: Vec<(u32, S, f64)>,
    pub transitions: Vec<StateTransition<S>>,
    pub ticks: Vec<f64>,
    pub syncs: Vec<(u32, S, f64)>,
    pub peer_connects: Vec<(PeerID, f64)>,
    pub peer_disconnects: Vec<(PeerID, f64)>
}

impl<S: State + StateFromHandler, O: JSONSocket> BaseTestHandler<S, O> {
    pub fn new(config: Config) -> Self {
        Self {
            socket: PhantomData,
            config,
            command: Command::default(),
            applied: Vec::new(),
            reverted: Vec::new(),
            committed: Vec::new(),
            transitions: Vec::new(),
            syncs: Vec::new(),
            ticks: Vec::new(),
            peer_connects: Vec::new(),
            peer_disconnects: Vec::new()
        }
    }
}

impl<S: State + StateFromHandler, O: JSONSocket> WebSocketHandler for BaseTestHandler<S, O> {
    fn connect_web_socket(&mut self) {
        self.command.disconnected = Some(phase::Disconnected::Command::Connect {
            host: "localhost".to_string(),
            session_id: "session-test".to_string()
        });
    }
}

impl<S: State + StateFromHandler, O: JSONSocket> Handler for BaseTestHandler<S, O> {

    type State = S;
    type Socket = O;
    type Connection = TestConnection;

    fn log<L: Into<String>>(_: L) {
    }

    fn config(&self) -> Config {
        self.config.clone()
    }

    fn version() -> u32 {
        42
    }

    fn now() -> f64 {
        0.0
    }

    fn state(&self, config: S::Config, peers: &[Peer<Self>]) -> S {
        let ids: Vec<PeerID> = peers.iter().map(|p| p.id()).collect();
        S::from_handler(config, ids)
    }

    fn input(&self) -> S::Input {
        <S::Input as Input>::predict(None)
    }

    fn event(&mut self, event: Event<Self>, now: f64) where Self: Sized {
        match event {
            Event::StateSync(frame, sync) => self.syncs.push((frame, sync.clone(), now)),
            Event::StateApply(frame, state) => self.applied.push((frame, state.clone(), now)),
            Event::StateRevert(frame, state) => self.reverted.push((frame, state, now)),
            Event::StateCommit(frame, state) => self.committed.push((frame, state, now)),
            Event::PeerConnect(id) => self.peer_connects.push((id, now)),
            Event::PeerDisconnect(id) => self.peer_disconnects.push((id, now))
        }
    }

}

pub(crate) type TestHandler = BaseTestHandler<TestState, TestSocket>;
test_phase_all_impl!(TestHandler, TestState);

pub(crate) type TestInputHandler = BaseTestHandler<TestInputState, TestSocket>;
test_phase_all_impl!(TestInputHandler, TestInputState);

pub(crate) type TestConfigHandler = BaseTestHandler<TestConfigState, TestSocket>;
test_phase_all_impl!(TestConfigHandler, TestConfigState);

pub(crate) type MockClientHandler = BaseTestHandler<TestState, self::server::MockSocketClient>;
test_phase_all_impl!(MockClientHandler, TestState);

