// STD Dependencies -----------------------------------------------------------
use std::error::Error;
use std::cell::RefCell;
use std::sync::mpsc::{Sender, Receiver, channel};
use std::collections::HashMap;


// Internal Dependencies ------------------------------------------------------
use crate::PeerID;
use crate::traits::JSONSocket;
use crate::socket::SocketResponse;
use crate::server::connection::{Connection, ConnectionSocket, ConnectionHandler};


// Statics --------------------------------------------------------------------
thread_local! {
    static SERVERS: RefCell<HashMap<String, MockServer>> = RefCell::new(HashMap::new());
}


// Socket Mocks ---------------------------------------------------------------
enum SocketMessage {
    Text(String),
    Close
}

pub struct MockSocketServer {
    url: String,
    outgoing: Sender<SocketMessage>,
    incoming: Option<Receiver<SocketMessage>>,
    logs: Sender<String>
}

impl ConnectionSocket for MockSocketServer {

    fn send_response(&mut self, response: SocketResponse) {
        self.outgoing.send(SocketMessage::Text(response.to_string())).ok();
    }

    fn log(&self, log: &str) {
        self.logs.send(log.to_string()).ok();
    }

}

pub struct MockSocketClient {
    connected: bool,
    outgoing: Sender<SocketMessage>,
    incoming: Receiver<SocketMessage>
}

impl JSONSocket for MockSocketClient {

    fn try_new(url: &str) -> Result<Self, Box<dyn Error>> where Self: Sized {

        let (client_out, server_in) = channel();
        let (server_out, client_in) = channel();
        let (logs_out, logs_in) = channel();

        SERVERS.with(|s| {
            let mut s = s.borrow_mut();
            let server = s.entry(url.to_string()).or_insert_with(|| {
                MockServer::new(logs_in)
            });
            server.connection_made(MockSocketServer {
                url: url.to_string(),
                outgoing: server_out,
                incoming: Some(server_in),
                logs: logs_out
            })
        });

        Ok(MockSocketClient {
            connected: true,
            outgoing: client_out,
            incoming: client_in
        })

    }

    fn close(&mut self) {
        self.connected = false;
        self.outgoing.send(SocketMessage::Close).ok();
    }

    fn send(&mut self, text: String) {
        self.outgoing.send(SocketMessage::Text(text)).ok();
    }

    fn receive(&mut self) -> Vec<String> {
        let mut received = Vec::new();
        while let Ok(msg) = self.incoming.try_recv() {
            match msg {
                SocketMessage::Text(text) => received.push(text),
                SocketMessage::Close => {
                    self.connected = false;
                    break;
                }
            }
        }
        received
    }

    fn is_open(&self) -> bool {
        self.connected
    }

    fn is_closed(&self) -> bool {
        self.connected
    }

}


// Server Mock ----------------------------------------------------------------
struct MockServerConnection {
    url: Option<String>,
    is_active: bool,
    incoming: Receiver<SocketMessage>,
    connection: Connection<MockSocketServer>
}

#[derive(Debug, PartialEq)]
pub struct MockServerInfo {
    pub logs: Vec<String>,
    pub connection_count: usize
}

pub struct MockServer {
    manager: ConnectionHandler<MockSocketServer>,
    connections: Vec<MockServerConnection>,
    incoming_logs: Receiver<String>,
    logs: Vec<String>
}

impl MockServer {

    pub fn tick_all() {
        SERVERS.with(|servers| {
            let mut servers = servers.borrow_mut();
            for s in servers.values_mut() {
                s.tick();
            }
        });
    }

    pub fn info(url: &str) -> Option<MockServerInfo> {
        SERVERS.with(|servers| {
            let servers = servers.borrow_mut();
            if let Some(s) = servers.get(url) {
                Some(MockServerInfo {
                    logs: s.logs.clone(),
                    connection_count: s.connections.len()
                })

            } else {
                None
            }
        })
    }

    pub fn urls() -> Vec<String> {
        SERVERS.with(|servers| {
            servers.borrow().keys().map(|s| s.clone()).collect()
        })
    }

    fn new(logs: Receiver<String>) -> Self {
        let ids = vec![PeerID(0), PeerID(1)];
        Self {
            manager: ConnectionHandler::with_id_pool(ids),
            connections: Vec::new(),
            incoming_logs: logs,
            logs: Vec::new()
        }
    }

    fn tick(&mut self) {
        for conn in &mut self.connections {

            if let Some(url) = conn.url.take() {
                if conn.connection.connect(&url).is_ok() {
                    conn.is_active = true;
                }
            }

            if conn.is_active {
                while let Ok(msg) = conn.incoming.try_recv() {
                    match msg {
                        SocketMessage::Text(text) => {
                            if conn.connection.message(&text).is_err() {
                                conn.is_active = false;
                            }
                        },
                        SocketMessage::Close => {
                            conn.connection.close();
                            conn.is_active = false;
                            break;
                        }
                    }
                }
            }

        }

        self.connections.retain(|c| c.is_active);

        while let Ok(log) = self.incoming_logs.try_recv() {
            self.logs.push(log);
        }

    }

    fn connection_made(&mut self, mut socket: MockSocketServer) {
        self.connections.push(MockServerConnection {
            url: Some(socket.url.to_string()),
            is_active: false,
            incoming: socket.incoming.take().unwrap(),
            connection: self.manager.add_socket(socket)
        });
    }

}

