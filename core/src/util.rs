// STD Dependencies -----------------------------------------------------------
use std::iter;
use std::ops::{Add, Sub, Div, Mul};


// CMA Average ----------------------------------------------------------------
pub struct Average<T: Copy + Clone + Add + Sub + Div + Mul + From<u32>> {
    slots: Vec<T>,
    index: usize,
    default: T,
    cma: T
}

impl<T: Copy + Clone + Add<Output=T> + Sub<Output=T> + Div<Output=T> + Mul<Output=T> + From<u32>> Average<T> {

    pub fn new(slots: u32, default: T) -> Self {
        Self {
            slots: iter::repeat(default).take(slots as usize).collect(),
            index: 0,
            default,
            cma: default
        }
    }

    pub fn reset(&mut self) {
        self.index = 0;
        self.cma = self.default;
        for v in &mut self.slots {
            *v = self.default;
        }
    }

    pub fn push(&mut self, value: T) {
        self.slots[self.index] = value;
        self.index = (self.index + 1) % self.slots.len();

        let mut sum = self.slots[0];
        for v in &self.slots[1..] {
            sum = sum + *v;
        }

        self.cma = sum / T::from(self.slots.len() as u32);

    }

    pub fn get(&self) -> T {
        self.cma
    }

}

pub struct Sum<T: Copy + Clone + Add + Sub + From<u32>> {
    slots: Vec<T>,
    index: usize,
    default: T,
    sum: T
}

impl<T: Copy + Clone + Add<Output=T> + Sub<Output=T> + From<u32>> Sum<T> {

    pub fn new(slots: u32, default: T) -> Self {
        Self {
            slots: iter::repeat(default).take(slots as usize).collect(),
            index: 0,
            default,
            sum: default
        }
    }

    pub fn reset(&mut self) {
        self.index = 0;
        self.sum = self.default;
        for v in &mut self.slots {
            *v = self.default;
        }
    }

    pub fn push(&mut self, value: T) {
        self.sum = self.sum - self.slots[self.index];
        self.slots[self.index] = value;
        self.index = (self.index + 1) % self.slots.len();
        self.sum = self.sum + value;
    }

    pub fn get(&self) -> T {
        self.sum
    }

}


// Timer ----------------------------------------------------------------------
#[derive(Clone)]
pub struct Timer {
    reference: f64
}

#[allow(clippy::new_without_default_derive)]
impl Timer {

    pub fn new() -> Self {
        Self {
            reference: 0.0
        }
    }

    pub fn reset(&mut self, reference: f64) {
        self.reference = reference;
    }

    pub fn has_elapsed(&mut self, reference: f64, duration: f64) -> bool {
        if self.reference == 0.0 && reference > 0.0 {
            self.reference = reference;
            reference >= duration

        } else {
            let delta = reference - self.reference;
            if delta >= duration {
                self.reference = reference + (delta.min(duration) - duration);
                true

            } else {
                false
            }
        }

    }

}

#[derive(Clone)]
pub struct FixedTimer {
    duration: f64,
    timer: Timer
}

impl FixedTimer {

    pub fn new(duration: f64) -> Self {
        Self {
            duration,
            timer: Timer::new()
        }
    }

    pub fn reset(&mut self, reference: f64) {
        self.timer.reset(reference);
    }

    pub fn has_elapsed(&mut self, reference: f64) -> bool {
        let d = self.duration;
        self.timer.has_elapsed(reference, d)
    }

}


// Tests ----------------------------------------------------------------------
#[cfg(test)]
mod test {

    use super::{Average, FixedTimer, Sum, Timer};

    #[test]
    fn test_timer() {
        let mut t = Timer::new();
        assert_eq!(t.has_elapsed(0.0, 1000.0), false);
        assert_eq!(t.has_elapsed(500.0, 1000.0), false);
        assert_eq!(t.has_elapsed(1000.0, 1000.0), false);
        assert_eq!(t.has_elapsed(1500.0, 1000.0), true);
        assert_eq!(t.has_elapsed(2500.0, 1000.0), true);
        t.reset(0.0);
        assert_eq!(t.has_elapsed(1000.0, 1000.0), true);
        assert_eq!(t.has_elapsed(2000.0, 1000.0), true);
    }

    #[test]
    fn test_fixed_timer() {
        let mut t = FixedTimer::new(1000.0);
        assert_eq!(t.has_elapsed(0.0), false);
        assert_eq!(t.has_elapsed(500.0), false);
        assert_eq!(t.has_elapsed(1000.0), false);
        assert_eq!(t.has_elapsed(1500.0), true);
        assert_eq!(t.has_elapsed(2500.0), true);
        t.reset(0.0);
        assert_eq!(t.has_elapsed(1000.0), true);
        assert_eq!(t.has_elapsed(2000.0), true);
    }

    #[test]
    fn test_average() {
        let mut a = Average::<f64>::new(4, 0.0);

        let mut values = Vec::new();
        for _ in 0..16 {
            a.push(10.0);
            values.push(format!("{:.3}", a.get()));
        }

        for _ in 0..8 {
            a.push(0.0);
            values.push(format!("{:.3}", a.get()));
        }
        assert_eq!(values, vec![
            "2.500",
            "5.000",
            "7.500",
            "10.000",
            "10.000",
            "10.000",
            "10.000",
            "10.000",
            "10.000",
            "10.000",
            "10.000",
            "10.000",
            "10.000",
            "10.000",
            "10.000",
            "10.000",
            "7.500",
            "5.000",
            "2.500",
            "0.000",
            "0.000",
            "0.000",
            "0.000",
            "0.000"
        ]);

        a.reset();
        assert_eq!(a.get(), 0.0);
    }

    #[test]
    fn test_sum() {
        let mut s = Sum::<f64>::new(4, 0.0);
        s.push(10.0);
        assert_eq!(s.get(), 10.0);
        s.push(10.0);
        assert_eq!(s.get(), 20.0);
        s.push(10.0);
        assert_eq!(s.get(), 30.0);
        s.push(10.0);
        assert_eq!(s.get(), 40.0);
        s.push(10.0);
        assert_eq!(s.get(), 40.0);
        s.push(5.0);
        assert_eq!(s.get(), 35.0);
        s.reset();
        assert_eq!(s.get(), 0.0);
    }

}

