// STD Dependencies -----------------------------------------------------------
use std::fmt::Debug;
use std::error::Error;
use std::hash::{Hash, Hasher};


// External Dependencies ------------------------------------------------------
use seahash::SeaHasher;
use serde::{Serialize, de::DeserializeOwned};


// Internal Dependencies ------------------------------------------------------
use crate::{Config, Event, Peer, PeerID, Frame};


// Traits ---------------------------------------------------------------------
pub trait JSONSocket {
    fn try_new(url: &str) -> Result<Self, Box<dyn Error>> where Self: Sized;
    fn close(&mut self);
    fn send(&mut self, text: String);
    fn receive(&mut self) -> Vec<String>;
    fn is_open(&self) -> bool;
    fn is_closed(&self) -> bool;
}

pub trait RTCConnection {
    fn new(id: String, initiator: bool) -> Self;
    fn send(&mut self, bytes: Vec<u8>);
    fn signal(&mut self, signal: String);
    fn is_connected(&self) -> bool;
    fn was_disconnected(&self) -> bool;
    fn signals(&mut self) -> Option<Vec<String>>;
    fn receive(&mut self) -> Option<Vec<Vec<u8>>>;
}

pub trait Input: Debug + Clone + Copy + PartialEq {
    fn predict(previous: Option<Self>) -> Self where Self: Sized;
    fn into_byte(self) -> u8;
    fn from_byte(byte: u8) -> Self;
}

pub trait StateConfig: Debug + Clone + Default + PartialEq + Hash + Serialize + DeserializeOwned {

    fn merge(&self, other: Self, peer_id: PeerID) -> Self;

    fn hash_value(&self) -> u64 {
        let mut hasher = SeaHasher::new();
        self.hash(&mut hasher);
        hasher.finish()
    }

}

pub trait State: Debug + Clone + Hash {

    type Input: Input;
    type Config: StateConfig;

    fn next(&self, frame: &Frame<Self::Input>) -> Self;
    fn hash_value(&self) -> u64;

}

pub trait Handler: Sized {

    type State: State;
    type Socket: JSONSocket;
    type Connection: RTCConnection;

    fn log<S: Into<String>>(msg: S);
    fn version() -> u32;

    fn now() -> f64;

    fn config(&self) -> Config;
    fn state(&self, state_config: <Self::State as State>::Config, peers: &[Peer<Self>]) -> Self::State;
    fn input(&self) -> <Self::State as State>::Input;

    // TODO use to ignore peers beyond the limit of peers in a session and
    // also make peers auto disconnect if they are the ones exceeding the limit
    // fn limit(&self, config: &GameConfig) -> Option<usize> {
    //     None
    // }

    fn event(&mut self, event: Event<Self>, tick_time: f64);

}

