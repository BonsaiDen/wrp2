// Internal -------------------------------------------------------------------
use super::{Client, Event};
use crate::phase::{self, ClientPhase};
use crate::traits::{Handler, JSONSocket, StateConfig};


// Client Ticking Implementation ----------------------------------------------
impl<H> Client<H>
where H:
    'static + Handler +
    phase::Disconnected::Phase +
    phase::Connecting::Phase<<H as Handler>::State> +
    phase::Hosting::Phase<<H as Handler>::State> +
    phase::Peering::Phase<<H as Handler>::State> +
    phase::Started::Phase<<H as Handler>::State> +
    phase::Starting::Phase<<H as Handler>::State> +
    phase::ConnectionLost::Phase +
    phase::Disconnecting::Phase +
    phase::SimulatingOnline::Phase<<H as Handler>::State> +
    phase::SimulatingOffline::Phase<<H as Handler>::State> +
    phase::PausedOffline::Phase<<H as Handler>::State> +
    phase::Synchronizing::Phase +
    phase::SynchronisationLost::Phase
{

    pub(in crate::client) fn ticking(&mut self, now: f64, elapsed: f64) {

        // Receive from peers and handle timeouts / disconnects
        let disconnect_occured = self.ticking_receive_from_peers(now);
        self.ticking_update(now, elapsed, disconnect_occured);

        // Detect WebSocket disconnectes
        if let Some(socket) = &self.socket {
            if socket.is_closed() {
                H::log("[Core] [WebSocket] Connection lost!");

                self.transition(ClientPhase::ConnectionLost, now);
                self.reset(now);
                self.socket = None;
            }
        }

        // Send to peers
        for p in &mut self.peers {
            p.tick_send(now);
        }

    }

    fn ticking_receive_from_peers(&mut self, now: f64) -> bool {

        // During a Syncing Phase we reduce the max delay by half so bad connections
        // get timed out faster and we can avoid extremely long re-sync times
        let max_peer_receive_delay = if let ClientPhase::Synchronizing { .. } = &self.phase {
            self.config.simulation_timeout * 0.5

        } else {
            self.config.simulation_timeout
        };

        let mut disconnect_occured = false;

        // Receive from peers
        let handler = &mut self.handler;
        for p in &mut self.peers {
            p.tick_receive(now);

            // Detect client disconnects
            if p.receive_delay(now) > max_peer_receive_delay && p.disconnect() {
                H::log(format!("[Core] [Ticking] Peer timed out: {}", p.id()));
                handler.event(Event::PeerDisconnect(p.id()), now);
                disconnect_occured = true;
            }
        }

        disconnect_occured

    }

    fn ticking_update(&mut self, now: f64, elapsed: f64, disconnect_occured: bool) {

        let tick_time = now - self.tick_init_time;

        match self.phase {

            ClientPhase::SimulatingOnline(ref p) => {
                let command = phase::SimulatingOnline::Phase::tick(&mut self.handler, p, &self.peers, tick_time);
                if let Some(phase::SimulatingOnline::Command::Disconnect) = command {
                    // When disconnecting programmatically during the simulation we
                    // need to give the other peers a chance to actually receive our
                    // Disconnect message.
                    self.timer_disconnect.reset(now);
                    self.transition(ClientPhase::Disconnecting, now);

                } else {
                    self.ticking_phase_simulation(now, elapsed, disconnect_occured);
                }
            },

            ClientPhase::SimulatingOffline(ref p) => {
                let command = phase::SimulatingOffline::Phase::tick(&mut self.handler, p, &self.peers, tick_time);
                if let Some(phase::SimulatingOffline::Command::Pause) = command {
                    self.transition(ClientPhase::PausedOffline(phase::PausedOffline::State {
                        frame: p.frame,
                        state: p.state.clone()

                    }), now);

                } else if let Some(phase::SimulatingOffline::Command::Stop) = command {
                    self.transition_disconnected(now);

                } else {
                    self.ticking_phase_simulation(now, elapsed, disconnect_occured);
                }
            },

            ClientPhase::PausedOffline(ref p) => {
                let command = phase::PausedOffline::Phase::tick(&mut self.handler, p, &self.peers, tick_time);
                if let Some(phase::PausedOffline::Command::Resume) = command {
                    self.transition(ClientPhase::SimulatingOffline(phase::SimulatingOffline::State {
                        frame: p.frame,
                        state: p.state.clone()

                    }), now);

                } else if let Some(phase::PausedOffline::Command::Stop) = command {
                    self.transition_disconnected(now);
                }
            },

            ClientPhase::Connecting(ref p) => {
                let command = phase::Connecting::Phase::tick(&mut self.handler, p, &self.peers, tick_time);
                self.peering(now);
                if let Some(phase::Connecting::Command::Disconnect) = command {
                    self.json_socket_disconnect(now);
                }
            },

            ClientPhase::Hosting(ref mut session) => {
                let command = phase::Hosting::Phase::tick(&mut self.handler, session, &self.peers, tick_time);
                if let Some(phase::Hosting::Command::SetConfig(config)) = command {
                    let hash = config.hash_value();
                    if session.state_hash != hash {
                        session.state_hash = hash;
                        session.state_config = config;
                        self.peering_advertise(0.0, now);
                    }
                    self.peering(now);

                } else if let Some(phase::Hosting::Command::Start) = command {
                    let session = session.clone();
                    self.peering(now);
                    return self.start(session, now);

                } else {
                    self.peering(now);
                    if let Some(phase::Hosting::Command::Disconnect) = command {
                        self.json_socket_disconnect(now);
                    }
                }
            },

            ClientPhase::Peering(ref session) => {
                let current_state_hash = session.state_hash;
                let command = phase::Peering::Phase::tick(&mut self.handler, session, &self.peers, tick_time);
                self.peering(now);
                if let Some(phase::Peering::Command::RequestConfig(config)) = command {
                    if current_state_hash != config.hash_value() {
                        self.json_socket_config_request(config);
                    }

                } else if let Some(phase::Peering::Command::Disconnect) = command {
                    self.json_socket_disconnect(now);
                }
            },

            ClientPhase::Started(ref config) => {
                let command = phase::Started::Phase::tick(&mut self.handler, config, &self.peers, tick_time);
                self.starting(now);
                if let Some(phase::Started::Command::Disconnect) = command {
                    self.json_socket_disconnect(now);
                }
            },

            ClientPhase::Starting(ref config) => {
                let command = phase::Starting::Phase::tick(&mut self.handler, config, &self.peers, tick_time);
                self.starting(now);
                if let Some(phase::Starting::Command::Disconnect) = command {
                    self.json_socket_disconnect(now);
                }
            },

            ClientPhase::ConnectionLost => {
                let command = phase::ConnectionLost::Phase::tick(&mut self.handler, &self.peers, tick_time);
                if let Some(phase::ConnectionLost::Command::Connect { host, session_id }) = command {
                    self.json_socket_reconnect(host, session_id, now);

                } else if let Some(phase::ConnectionLost::Command::Reset) = command {
                    self.transition_disconnected(now);
                }
            },

            ClientPhase::Disconnecting => {
                phase::Disconnecting::Phase::tick(&mut self.handler, &self.peers, tick_time);
                if self.peers.len() > 1 && !self.timer_disconnect.has_elapsed(now) {
                    self.disconnecting();

                } else {
                    self.transition_disconnected(now);
                }
            },

            ClientPhase::Disconnected => {
                let command = phase::Disconnected::Phase::tick(&mut self.handler, &self.peers, tick_time);
                if let Some(phase::Disconnected::Command::Start(state_config)) = command {
                    self.start_offline(state_config, now);

                } else if let Some(phase::Disconnected::Command::Connect { host, session_id }) = command {
                    self.json_socket_reconnect(host, session_id, now);
                }
            },

            ClientPhase::Synchronizing(ref p) => {
                phase::Synchronizing::Phase::tick(&mut self.handler, p, &self.peers, tick_time);
                self.syncing(now);
            },

            ClientPhase::SynchronisationLost(ref p) => {
                let command = phase::SynchronisationLost::Phase::tick(&mut self.handler, p, &self.peers, tick_time);
                if let Some(phase::SynchronisationLost::Command::Reset) = command {
                    self.transition_disconnected(now);
                }
            }

        }

    }

    fn ticking_phase_simulation(&mut self, now: f64, elapsed: f64, disconnect_occured: bool) {
        if disconnect_occured {
            self.resync(now);

        } else {
            self.tick_total += 1;
            self.simulate(now, elapsed).ok();
        }
    }

    fn transition_disconnected(&mut self, now: f64) {
        self.transition(ClientPhase::Disconnected, now);
        self.reset(now);
    }

    fn json_socket_disconnect(&mut self, now: f64) {
        if let Some(mut socket) = self.socket.take() {
            self.transition_disconnected(now);
            socket.close();
        }
    }

    fn json_socket_reconnect(&mut self, host: String, session_id: String, now: f64) {

        self.timer_advertise.reset(now);

        let url = format!("{}/{}", host, session_id);
        if let Ok(socket) = H::Socket::try_new(&url) {
            H::log(format!("[Core] [WebSocket] Connecting to: {} ...", url));
            self.socket = Some(socket);
        }

        self.transition(phase::Connecting::new(host, session_id), now);

    }

}
