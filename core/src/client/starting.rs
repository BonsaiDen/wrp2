// Internal Dependencies ------------------------------------------------------
use super::{Client, Event, Session, SyncState};
use crate::peer::{Peer, PeerMessage};
use crate::phase::{self, ClientPhase, SimulationState};
use crate::traits::{Handler, State, JSONSocket};


// Client Starting Phase ------------------------------------------------------
impl<H> Client<H>
where H:
    'static + Handler +
    phase::Disconnected::Phase +
    phase::Connecting::Phase<<H as Handler>::State> +
    phase::Hosting::Phase<<H as Handler>::State> +
    phase::Peering::Phase<<H as Handler>::State> +
    phase::Started::Phase<<H as Handler>::State> +
    phase::Starting::Phase<<H as Handler>::State> +
    phase::ConnectionLost::Phase +
    phase::Disconnecting::Phase +
    phase::SimulatingOnline::Phase<<H as Handler>::State> +
    phase::SimulatingOffline::Phase<<H as Handler>::State> +
    phase::PausedOffline::Phase<<H as Handler>::State> +
    phase::Synchronizing::Phase +
    phase::SynchronisationLost::Phase
{

    pub(in crate::client) fn start(&mut self, session: Session<H::State>, now: f64) {

        // Drop any non ready or non connected peers, but always keep the local one
        self.peers.retain(|p| {
            p.is_local() || (p.is_ready() && p.is_connected())
        });

        self.transition(phase::Started::from_session(session), now);

    }

    pub(in crate::client) fn start_offline(&mut self, state_config: <H::State as State>::Config, now: f64) {

        let peer = Peer::offline(self.config.tick_rate, now);
        H::log(format!("[Core] [Peering] Offline peer created: {}", peer.id()));
        self.handler.event(Event::PeerConnect(peer.id()), now);

        self.peers.clear();
        self.peers.push(peer);
        self.start_simulation::<phase::SimulatingOffline::State<H::State>>(state_config, now);

    }

    pub(in crate::client) fn starting(&mut self, now: f64) {

        // We still need to received advertisements and handle timeouts
        self.peering_update_peers(false, now);

        // Determine the list of peers to send along with our starting message
        let peer_ids = if let ClientPhase::Started(ref session) = self.phase {

            let required_config_hash = Some(session.state_hash);

            // We still need to advertise our config and wait for all peers to match it before
            // actually sending out starting messages
            self.peering_advertise(self.config.advertise_interval.0, now);

            // We wait for all other peers to advertise the matching session config
            // Since we're always the hosting + local peer here we can consider ourselves
            // configured in any case
            let all_configured = self.peers.iter().all(|p| p.is_local() || p.state_config_hash() == required_config_hash);
            if all_configured {
                // In case we're the host notify the other peers that we're
                // starting with the specified list of peers
                Some(self.peers.iter().map(|p| p.id()).collect())

            } else {
                None
            }

        } else {
            Some(Vec::new())
        };

        // Broadcast starting message to all connected peers
        if let Some(peer_ids) = peer_ids {
            for peer in &mut self.peers {

                // Always mark local peer as starting
                if peer.is_local() {
                    peer.start();

                } else {

                    // Start the peer as soon as it sends us a starting message
                    if peer.receive().into_iter().any(|m| m.is_starting()) {
                        peer.start();
                    }

                    // Let other peers know that we are starting
                    peer.send(PeerMessage::Starting(peer_ids.clone()));

                }

            }
        }

        // Check whether all connected peers are starting the game
        let all_started = self.peers.iter().all(|p| p.is_starting());
        if all_started {

            H::log(format!("[Core] [Starting] All {} peers are starting...", self.peers.len()));
            let config = match self.phase {
                ClientPhase::Starting(ref state) => state.state_config.clone(),
                ClientPhase::Started(ref state) => state.state_config.clone(),
                _ => unreachable!()
            };

            self.start_simulation::<phase::SimulatingOnline::State<H::State>>(config, now);

            // Close the WebSocket connection used for peering
            if let Some(mut socket) = self.socket.take() {
                socket.close();
            }

        }

    }

    fn start_simulation<T: SimulationState<H::State>>(&mut self, state_config: <H::State as State>::Config, now: f64) {
        // "Restore" the simulation using the base state provided by the handler
        let state = self.handler.state(state_config, &self.peers);
        self.restore_from_sync_state::<T>(SyncState {
            frame: 0,
            frame_elapsed_time: 0.0,
            frame_elapsed_time_real: 0.0,
            tick_total: 0,
            state

        }, now);
    }

}

