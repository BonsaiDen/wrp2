// STD Dependencies -----------------------------------------------------------
use std::cmp;


// Internal -------------------------------------------------------------------
use super::Client;
use crate::peer::PeerMessage;
use crate::phase::{self, ClientPhase, SyncError};
use crate::traits::Handler;


// Client Syncing Phase ------------------------------------------------------
impl<H> Client<H>
where H:
    'static + Handler +
    phase::Disconnected::Phase +
    phase::Connecting::Phase<<H as Handler>::State> +
    phase::Hosting::Phase<<H as Handler>::State> +
    phase::Peering::Phase<<H as Handler>::State> +
    phase::Started::Phase<<H as Handler>::State> +
    phase::Starting::Phase<<H as Handler>::State> +
    phase::ConnectionLost::Phase +
    phase::Disconnecting::Phase +
    phase::SimulatingOnline::Phase<<H as Handler>::State> +
    phase::SimulatingOffline::Phase<<H as Handler>::State> +
    phase::PausedOffline::Phase<<H as Handler>::State> +
    phase::Synchronizing::Phase +
    phase::SynchronisationLost::Phase
{

    pub(in crate::client) fn resync(&mut self, now: f64) {

        H::log("[Core] [Syncing] Re-Sync triggered...");

        // TODO timeout with a de-sync in case re-sync take too long (10 seconds+)

        // also trigger in case we receive an sync message during the simulation
        // but only then the sync counter contained within the message is higher than our local one
        // we then overwrite the local sync counter with the highest once received and
        // transmis that one to all other peers

        // in order to resolve single sided disconnects in the graph
        // all peers also transmit a list of the peer indexes they are still connected to
        // once all peers connected to the local one signal their readiness for re-sync
        // the local determines the highest number of active incoming connections any peer currently
        // holds and then disconnects from all peers hold less than that number of connections
        // this avoids a deadlock situation

        // connections must be disconnected before continuing from a restored state so that
        // all clients stop applying any inputs / logic for that peer

        // need to ensure that after connection drops we reset the sync state and wait for
        // further sync messages to complete the process with the remaining peers

        // TODO de-sync here instead
        assert!(!self.resync_states.is_empty(), "No states available for re-sync!");

        // Reset sync timeout which guards against deadlocks
        self.timer_syncing.reset(now);
        self.transition(phase::Synchronizing::new(), now);
    }

    pub(crate) fn syncing(&mut self, now: f64) {

        // Extract local sync point frame
        let latest_sync_frame = self.resync_states.back().unwrap().frame;

        // Guard against deadlock
        if self.timer_syncing.has_elapsed(now) {
            return self.transition(phase::SynchronisationLost::new(
                latest_sync_frame,
                SyncError::Timeout

            ), now);
        }

        // Negotiate state to sycn with other peers
        let mut lowest_sync_frame = latest_sync_frame;
        for peer in &mut self.peers {
            if peer.is_local() {
                peer.sync().latest_sync_frame = Some(latest_sync_frame);

            // Ignore disconnected peers
            } else if peer.is_connected() {

                // Receive sync frame information from other peers
                for m in peer.receive() {
                    // TODO upgrade local sync counter to the highest one received
                    if let PeerMessage::Sync { latest_sync_frame } = m {
                        peer.sync().latest_sync_frame = Some(latest_sync_frame);
                    }
                }

                // Send our latest sync frame to the peer
                peer.send(PeerMessage::Sync {
                    // TODO include sync_counter
                    latest_sync_frame
                });

                // Keep track of the lowest common sync frame between all peers
                if let Some(latest) = peer.sync().latest_sync_frame {
                    lowest_sync_frame = cmp::min(lowest_sync_frame, latest)
                }

            }
        }

        // Check if all connected peers are ready for re-sync
        let peers_needed_for_sync = self.peers.iter().filter(|p| p.is_connected()).count();
        let peers_ready_for_sync = self.peers.iter().filter(|p| {
            // TODO peers with sync counters != local sync counter are not considered ready
            p.sync_ref().latest_sync_frame.is_some()

        }).count();

        // Update phase information
        if let ClientPhase::Synchronizing(ref mut phase) = self.phase {
            for peer in &mut self.peers {
                if !phase.peers_ready.contains(&peer.id()) && peer.sync().latest_sync_frame.is_some() {
                    phase.peers_ready.insert(peer.id());
                }
            }
        }

        // Re-sync once all peers are ready
        if peers_ready_for_sync == peers_needed_for_sync {

            H::log(format!("[Core] [Syncing] All {} peers are restoring to frame {}...", peers_ready_for_sync, lowest_sync_frame));

            // Re-initalize with the lowest common sync state
            if let Some(sync) = self.resync_states.iter().find(|s| s.frame == lowest_sync_frame) {
                // TODO increase local sync counter to avoid responding to late sync requests
                // TODO trigger resync callback on handler with information on how many states were
                // lost
                self.restore_from_sync_state::<phase::SimulatingOnline::State<H::State>>(sync.clone(), now);

            // TODO If we don't find a matching sync point trigger a desync
            // test by having only a higher re-sync point locally but the remotes both agree on a
            // lower one
            } else {
                // self.transition(Phase::Desynced {
                //     frame: *frame,
                //     local_hash: *hash,
                //     peer_hash: p_hash,
                //     peer_token: id.clone()
                // })
            }

        }

    }

}

