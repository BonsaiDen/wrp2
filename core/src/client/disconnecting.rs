// Internal -------------------------------------------------------------------
use super::Client;
use crate::peer::PeerMessage;
use crate::traits::Handler;


// Client Starting Phase ------------------------------------------------------
impl<H: 'static + Handler> Client<H> {

    pub(in crate::client) fn disconnecting(&mut self) {
        for peer in &mut self.peers {
            if peer.is_remote() {
                peer.send(PeerMessage::Disconnecting);
            }
        }
    }

}

