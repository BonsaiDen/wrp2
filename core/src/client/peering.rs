// Internal Dependencies ------------------------------------------------------
use super::{Client, Event};
use crate::phase::{self, ClientPhase};
use crate::peer::{Peer, PeerID, PeerMessage};
use crate::traits::{Handler, State, StateConfig, JSONSocket};
use crate::socket::{SocketRequest, SocketRequestPayload, SocketRequestConfig, SocketResponse};


// Client Peering Phase -------------------------------------------------------
impl<H> Client<H>
where H:
    'static + Handler +
    phase::Disconnected::Phase +
    phase::Connecting::Phase<<H as Handler>::State> +
    phase::Hosting::Phase<<H as Handler>::State> +
    phase::Peering::Phase<<H as Handler>::State> +
    phase::Started::Phase<<H as Handler>::State> +
    phase::Starting::Phase<<H as Handler>::State> +
    phase::ConnectionLost::Phase +
    phase::Disconnecting::Phase +
    phase::SimulatingOnline::Phase<<H as Handler>::State> +
    phase::SimulatingOffline::Phase<<H as Handler>::State> +
    phase::PausedOffline::Phase<<H as Handler>::State> +
    phase::Synchronizing::Phase +
    phase::SynchronisationLost::Phase
{

    pub(in crate::client) fn peering(&mut self, now: f64) {

        self.peering_update_peers(true, now);

        if let Some(local_peer_seed) = self.peers.iter().find(|p| p.is_local()).map(|p| p.seed()) {

            // Establish rtc connections from local peer to others by
            // generating and forwarding peering requests over the json socket
            let requests: Vec<SocketRequest> = self.peers.iter_mut().filter(|p| p.is_remote()).filter_map(|p| {
                p.rtc_initialize(local_peer_seed, H::version(), now)

            }).collect();

            for r in requests {
                self.json_socket_send(r);
            }

            self.update_peering_phase(now);

        }

        let all_connected = self.peers.iter().all(|p| p.is_connected());
        let interval = if all_connected {
            self.config.advertise_interval.1

        } else {
            self.config.advertise_interval.0
        };

        // Continously advertise our presence to other peers on the server
        self.peering_advertise(interval, now);

        // Wait and Receive starting message from host
        if let ClientPhase::Peering(_) = self.phase {
            if let Some(local_id) = self.peers.iter().find(|p| p.is_local()).map(|p| p.id()) {
                self.receive_host_start(local_id, now);
            }
        }

    }

    pub(in crate::client) fn peering_update_peers(&mut self, accept_new_peers: bool, now: f64) {

        // Handle incoming socket messages
        if let Some(socket) = &mut self.socket {
            for msg in socket.receive() {
                if let Ok(response) = SocketResponse::from_str(&msg) {
                    self.handle_gateway_response(response, accept_new_peers, now);
                }
            }
        }

        // Filter inactive peers and order by server side index
        let handler = &mut self.handler;
        let timeout = self.config.peering_timeout;
        self.peers.retain(|peer| {
            if peer.is_present(now, timeout) {
                true

            } else {
                H::log(format!("[Core] [Peering] Lost peer: {}", peer.id()));
                handler.event(Event::PeerDisconnect(peer.id()), now);
                false
            }
        });

        self.peers.sort_by(|a, b| a.gateway_index().cmp(&b.gateway_index()));

    }

    pub(in crate::client) fn peering_advertise(&mut self, interval: f64, now: f64) {
        if self.timer_advertise.has_elapsed(now, interval) {
            if let ClientPhase::Connecting(ref p) | ClientPhase::Peering(ref p) | ClientPhase::Hosting(ref p) | ClientPhase::Started(ref p) | ClientPhase::Starting(ref p) = self.phase {
                let config = SocketRequestConfig::from_session(p);
                self.json_socket_send(SocketRequest {
                    to: None,
                    peers: Vec::new(),
                    version: H::version(),
                    payload: SocketRequestPayload::Advertise(config)
                });
            }
        }
    }


    // Internal ---------------------------------------------------------------
    fn handle_gateway_response(&mut self, response: SocketResponse, accept_new_peers: bool, now: f64) {

        // Ignore requests from different handler versions
        if response.request.version != H::version() {
            return;
        }

        // Figure out if the remote peer has connections to all peers that the local peer is also
        // connected to, if so then we consider it to be "ready"
        let is_ready = self.peers.iter().filter(|p| {
            // Local peer connections the remote must also be connected to
            p.is_connected() && p.id() != response.from

        }).filter(|p| {
            // Minus the connections the remote peer has established
            !response.request.peers.contains(&p.id())

        }).count() == 0;

        // Handle incoming config requests from other peers
        if let (SocketRequestPayload::ConfigRequest(config), ClientPhase::Hosting(session)) = (&response.request.payload, &mut self.phase) {
            if let Some((_, config)) = config.to_inner::<H::State>() {
                session.state_config = session.state_config.merge(config, response.from);
                let new_state_hash = session.state_config.hash_value();
                if new_state_hash != session.state_hash {
                    session.state_hash = new_state_hash;
                    self.peering_advertise(0.0, now);
                }
            }

        // Always copy the hosts session config if advertised
        } else if let (0, SocketRequestPayload::Advertise(config), ClientPhase::Peering(session)) = (response.index, &response.request.payload, &mut self.phase) {
            if let Some((hash, config)) = config.to_inner::<H::State>() {
                session.state_hash = hash;
                session.state_config = config;
            }
        }

        // Handle all other gateway responses
        if let Some(peer) = self.peers.iter_mut().find(|p| p.id() == response.from) {
            peer.gateway_receive(response, is_ready, now);

        } else if accept_new_peers {
            H::log(format!("[Core] [Peering] Discovered peer: {} (local: {})", response.from, response.local));

            let peer_id = response.from;
            let mut peer = Peer::new(
                peer_id,
                response.index,
                response.seed,
                response.local,
                self.config.tick_rate,
                now
            );

            // Ensure that the initial state config advertiesment is picked up
            peer.gateway_receive(response, false, now);

            self.peers.push(peer);
            self.handler.event(Event::PeerConnect(peer_id), now);
        }

    }

    fn update_peering_phase(&mut self, now: f64) {

        // Update phase in case client gains / looses host status
        // this also performs the initial transition from Connecting to Peering
        let next_id = if self.is_host() {
            ClientPhase::<H::State>::hosting_id()

        } else {
            ClientPhase::<H::State>::peering_id()
        };

        if next_id != self.phase.id() {

            if let Some(session) = match self.phase {
                ClientPhase::Connecting(ref p) |ClientPhase::Hosting(ref p) | ClientPhase::Peering(ref p) => {
                    Some(p.clone())
                },
                _ => None
            } {
                let next_phase = if self.is_host() {
                    phase::Hosting::from_session(session)

                } else {
                    phase::Peering::from_session(session)
                };
                self.transition(next_phase, now);
            }

        }

    }

    fn receive_host_start(&mut self, local_id: PeerID, now: f64) {

        let host_peers = if let Some(host) = self.peers.get_mut(0) {
            if let Some(PeerMessage::Starting(host_peers)) = host.receive().into_iter().find(|m| m.is_starting()) {
                H::log(format!("[Core] [Peering] [Message] [Start] Received from Host: {}", host.id()));
                host.start();
                Some(host_peers)

            } else {
                None
            }

        } else {
            None
        };

        if let Some(host_peers) = host_peers {

            assert!(!host_peers.is_empty(), "Host did not send any peers");

            // Check if local peer is actually in the hosts peer list
            if host_peers.contains(&local_id) {

                // Drop any peers which are not known / ready on the host
                let handler = &mut self.handler;
                self.peers.retain(|p| {
                    if host_peers.contains(&p.id())  {
                        true

                    } else {
                        handler.event(Event::PeerDisconnect(p.id()), now);
                        false
                    }
                });

                // Sort to ensure identical order with host before starting simulation
                self.peers.sort_by(|a, b| {
                    let index_a = host_peers.iter().position(|i| *i == a.id());
                    let index_b = host_peers.iter().position(|i| *i == b.id());
                    index_a.cmp(&index_b)
                });

                if let ClientPhase::Peering(ref session) = self.phase {
                    self.transition(phase::Starting::from_session(session.clone()), now);
                }

            }

        }

    }

    pub(in crate::client) fn json_socket_config_request(&mut self, config: <H::State as State>::Config) {
        self.json_socket_send(SocketRequest {
            to: None,
            peers: Vec::new(),
            version: H::version(),
            payload: SocketRequestPayload::ConfigRequest(SocketRequestConfig::from_config::<H::State>(config))
        });
    }

    fn json_socket_send(&mut self, mut req: SocketRequest) {
        if let Some(socket) = &mut self.socket {

            // Attach list of connected peers
            req.peers.append(&mut self.peers.iter().filter(|p| {
                p.is_remote() && p.is_connected()

            }).map(|p| p.id()).collect());

            socket.send(req.to_string());

        }
    }

}

