// STD Dependencies -----------------------------------------------------------
use std::collections::HashSet;
use std::collections::VecDeque;


// Internal Dependencies ------------------------------------------------------
use crate::{Handler, State};
use crate::peer::{Peer, PeerID};
use crate::phase::{self, ClientPhase, SimulationState};
use crate::simulation::{FrameID, Simulation};
use crate::util::{Average, FixedTimer, Timer};


// Client Phases --------------------------------------------------------------
mod disconnecting;
mod peering;
mod simulation;
mod starting;
mod syncing;
mod ticking;

#[cfg(test)]
mod test;


// Internal Types -------------------------------------------------------------
type StateHash = (FrameID, u64, f64, HashSet<PeerID>);

#[derive(Debug, Clone)]
struct SyncState<S: State> {
    frame: FrameID,
    frame_elapsed_time: f64,
    frame_elapsed_time_real: f64,
    tick_total: u64,
    state: S
}


// Macros ---------------------------------------------------------------------
macro_rules! phase_dispatch {
    ($this:ident, $name:ident, $($args:tt)* ) => {
        match $this.phase {
            ClientPhase::Disconnected => phase::Disconnected::Phase::$name(&mut $this.handler, $($args)*),
            ClientPhase::Connecting(ref p) => phase::Connecting::Phase::$name(&mut $this.handler, p, $($args)*),
            ClientPhase::Hosting(ref p) => phase::Hosting::Phase::$name(&mut $this.handler, p, $($args)*),
            ClientPhase::Peering(ref p) => phase::Peering::Phase::$name(&mut $this.handler, p, $($args)*),
            ClientPhase::Started(ref p) => phase::Started::Phase::$name(&mut $this.handler, p, $($args)*),
            ClientPhase::Starting(ref p) => phase::Starting::Phase::$name(&mut $this.handler, p, $($args)*),
            ClientPhase::ConnectionLost => phase::ConnectionLost::Phase::$name(&mut $this.handler, $($args)*),
            ClientPhase::Disconnecting => phase::Disconnecting::Phase::$name(&mut $this.handler, $($args)*),
            ClientPhase::SimulatingOnline(ref p) => phase::SimulatingOnline::Phase::$name(&mut $this.handler, p, $($args)*),
            ClientPhase::SimulatingOffline(ref p) => phase::SimulatingOffline::Phase::$name(&mut $this.handler, p, $($args)*),
            ClientPhase::PausedOffline(ref p) => phase::PausedOffline::Phase::$name(&mut $this.handler, p, $($args)*),
            ClientPhase::Synchronizing(ref p) => phase::Synchronizing::Phase::$name(&mut $this.handler, p, $($args)*),
            ClientPhase::SynchronisationLost(ref p) => phase::SynchronisationLost::Phase::$name(&mut $this.handler, p, $($args)*),
        }
    }
}


// Types ----------------------------------------------------------------------
pub enum Event<'a, H: Handler> {
    StateSync(FrameID, &'a H::State),
    StateApply(FrameID, &'a mut H::State),
    StateRevert(FrameID, H::State),
    StateCommit(FrameID, H::State),
    PeerConnect(PeerID),
    PeerDisconnect(PeerID)
}

#[derive(Debug, Clone)]
pub struct Config {
    pub tick_rate: u32,
    pub slow_threshold: f64,
    pub hash_interval: u32,
    pub hash_timeout: f64,
    pub sync_interval: u32,
    pub sync_timeout: f64,
    pub peering_timeout: f64,
    pub simulation_timeout: f64,
    pub disconnect_timeout: f64,
    pub advertise_interval: (f64, f64)
}

impl Default for Config {
    fn default() -> Self {
        Self {
            tick_rate: 30,
            hash_interval: 240,
            hash_timeout: 5000.0,
            slow_threshold: 1000.0,
            sync_interval: 30,
            sync_timeout: 10000.0,
            peering_timeout: 2000.0,
            simulation_timeout: 10000.0,
            disconnect_timeout: 2000.0,
            advertise_interval: (100.0, 500.0)
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Session<S: State> {
    pub host: String,
    pub id: String,
    pub(crate) state_hash: u64,
    pub state_config: S::Config
}


// Client ---------------------------------------------------------------------
pub struct Client<H: Handler> {

    // Tick
    tick_delay: Average<f64>,
    tick_speed: Average<f64>,
    tick_duration: f64,
    tick_total: u64,
    tick_last_time: f64,
    tick_init_time: f64,

    // Drawing
    frame_elapsed_time: f64,
    frame_elapsed_time_real: f64,
    frame_last_simulated: FrameID,
    frame_last_committed: FrameID,
    frame_delta: u32,
    frame_advantage: f32,
    frame_stall_queue: u32,

    // Internal timers
    timer_advertise: Timer,
    timer_disconnect: FixedTimer,
    timer_syncing: FixedTimer,

    // Client
    config: Config,
    pub(crate) handler: H,
    socket: Option<H::Socket>,
    peers: Vec<Peer<H>>,
    phase: ClientPhase<H::State>,
    simulation: Simulation<H::State>,
    local_inputs: Vec<(FrameID, <H::State as State>::Input)>,
    local_hashes: Vec<StateHash>,
    resync_states: VecDeque<SyncState<H::State>>

}

// Phase Dependent ------------------------------------------------------------
impl<H> Client<H>
where H:
    'static + Handler +
    phase::Disconnected::Phase +
    phase::Connecting::Phase<<H as Handler>::State> +
    phase::Hosting::Phase<<H as Handler>::State> +
    phase::Peering::Phase<<H as Handler>::State> +
    phase::Started::Phase<<H as Handler>::State> +
    phase::Starting::Phase<<H as Handler>::State> +
    phase::ConnectionLost::Phase +
    phase::Disconnecting::Phase +
    phase::SimulatingOnline::Phase<<H as Handler>::State> +
    phase::SimulatingOffline::Phase<<H as Handler>::State> +
    phase::PausedOffline::Phase<<H as Handler>::State> +
    phase::Synchronizing::Phase +
    phase::SynchronisationLost::Phase
{

    pub fn new(handler: H, now: f64) -> Self {

        let config = handler.config();
        let tick_duration = f64::from(1000 / config.tick_rate);

        let mut client = Self {

            // Ticking
            tick_delay: Average::new(config.tick_rate, tick_duration),
            tick_speed: Average::new(config.tick_rate, 1.0),
            tick_duration,
            tick_total: 0,
            tick_last_time: now,
            tick_init_time: now,

            // Drawing
            frame_elapsed_time: 0.0,
            frame_elapsed_time_real: 0.0,
            frame_last_simulated: 0,
            frame_last_committed: 0,
            frame_delta: 0,
            frame_advantage: 0.0,
            frame_stall_queue: 0,

            timer_advertise: Timer::new(),
            timer_disconnect: FixedTimer::new(config.disconnect_timeout),
            timer_syncing: FixedTimer::new(config.sync_timeout),

            // Client
            handler,
            socket: None,
            peers: Vec::new(),
            phase: ClientPhase::Disconnected,
            simulation: Simulation::new(config.tick_rate as usize),
            local_inputs: Vec::new(),
            local_hashes: Vec::new(),
            resync_states: VecDeque::new(),
            config

        };

        client.transition_enter(0.0);
        client

    }

    pub fn tick(&mut self, now: f64) -> u32 {
        let elapsed = (now - self.tick_last_time).max(0.0);
        let actual_delay = elapsed.min(self.tick_duration * 1.5);
        self.tick_delay.push(actual_delay);
        self.ticking(now, actual_delay);
        self.tick_last_time = now;

        let diff = self.tick_duration - self.tick_delay.get();
        (self.tick_duration + diff * 1.25) as u32
    }

    pub fn draw(&mut self, now: f64) {
        let diff = now - self.tick_last_time;
        let t = (1.0 / self.tick_duration * diff).min(1.5) as f32;
        phase_dispatch!(self, draw, &self.peers, self.frame_elapsed_time, t);
    }

    fn transition(&mut self, phase: ClientPhase<H::State>, now: f64) {

        let tick_time = now - self.tick_init_time;
        phase_dispatch!(self, leave, &self.peers, tick_time);

        H::log(format!("[Core] [Phase] Transition from {:?} to: {:?}", self.phase, phase));
        self.phase = phase;
        self.transition_enter(tick_time);

    }

    fn transition_enter(&mut self, tick_time: f64) {
        phase_dispatch!(self, enter, &self.peers, tick_time);
    }

    fn restore_from_sync_state<T: SimulationState<H::State>>(&mut self, sync: SyncState<H::State>, now: f64) {

        H::log(format!("[Core] [State] Restore from: {:?}", sync));

        // Reset tick, frame and draw offsets
        self.tick_total = sync.tick_total;
        self.frame_elapsed_time = sync.frame_elapsed_time;
        self.frame_elapsed_time_real = sync.frame_elapsed_time_real;
        self.frame_last_simulated = sync.frame;
        self.frame_last_committed = sync.frame;
        self.frame_delta = 0;
        self.frame_advantage = 0.0;
        self.frame_stall_queue = 0;
        self.tick_speed.reset();

        // Reset local lists
        self.local_inputs.clear();
        self.local_hashes.clear();
        self.resync_states.clear();

        // Reset all peers to their initial simulation state
        for peer in &mut self.peers {
            peer.reset(sync.frame);
        }

        // Transition to the simulation phase
        let ids = self.peers.iter().map(|p| p.id()).collect();
        self.simulation.init(sync.frame, sync.state.clone(), ids);
        self.handler.event(Event::StateSync(sync.frame, &sync.state), now);

        // Start with the initial sync point as a fallback
        self.resync_states.push_back(sync);

        self.transition(T::create(
            (self.frame_last_committed, self.frame_last_simulated),
            self.tick_speed.get(),
            self.simulation.state()

        ), now);

    }

    fn desync(&mut self, phase: ClientPhase<H::State>, now: f64) {
        for peer in &mut self.peers {
            peer.disconnect();
        }
        self.transition(phase, now);
    }

}


// Phase Independent ----------------------------------------------------------
impl<H> Client<H> where H: 'static + Handler {

    fn reset(&mut self, now: f64) {
        let ids: Vec<PeerID> = self.peers.iter().map(|p| p.id()).collect();
        for i in ids {
            self.handler.event(Event::PeerDisconnect(i), now);
        }
        self.peers.clear();
        self.local_inputs.clear();
        self.local_hashes.clear();
        self.resync_states.clear();
    }

    fn is_host(&self) -> bool {
        // The lowest peer as indicated by the server-side `index` is considered the host
        self.peers.iter().position(|p| p.is_local()).unwrap_or(1) == 0
    }

}

