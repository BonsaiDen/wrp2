// STD Dependencies -----------------------------------------------------------
use std::cmp;
use std::collections::HashSet;


// Internal -------------------------------------------------------------------
use super::{Client, Event, SyncState};
use crate::peer::PeerMessage;
use crate::phase::{self, ClientPhase, SyncError};
use crate::simulation::{FrameID, SimulationInput};
use crate::traits::{State, Handler};


// Client Simulation Phase ----------------------------------------------------
impl<H> Client<H>
where H:
    'static + Handler +
    phase::Disconnected::Phase +
    phase::Connecting::Phase<<H as Handler>::State> +
    phase::Hosting::Phase<<H as Handler>::State> +
    phase::Peering::Phase<<H as Handler>::State> +
    phase::Started::Phase<<H as Handler>::State> +
    phase::Starting::Phase<<H as Handler>::State> +
    phase::ConnectionLost::Phase +
    phase::Disconnecting::Phase +
    phase::SimulatingOnline::Phase<<H as Handler>::State> +
    phase::SimulatingOffline::Phase<<H as Handler>::State> +
    phase::PausedOffline::Phase<<H as Handler>::State> +
    phase::Synchronizing::Phase +
    phase::SynchronisationLost::Phase
{

    pub(in crate::client) fn simulate(&mut self, now: f64, elapsed: f64) -> Result<(), ()> {

        // If any other peers are too far behind our local simulation we want to complete freeze
        let mut tick_speed = 1.0;
        if self.frame_delta < self.config.tick_rate {

            // Otherwise we want to reduce the local input advantage over other peers very slowly
            // by applying a slowdown to the tick rate
            let target_rate = (f64::from(self.config.tick_rate) - f64::from(self.frame_advantage) * 0.5).max(0.0);
            self.tick_speed.push(target_rate / f64::from(self.config.tick_rate));

            // When very close to 1.0 speed we want to snap to it. This improves
            // the overall feeling and limits the speed correction to situations
            // where it will actually have a meaningful impact.
            tick_speed = (self.tick_speed.get() + 0.0075).min(1.0);
            if 1.0 - tick_speed < 0.01 {
                tick_speed = 1.0;
            }
            self.frame_elapsed_time += elapsed * tick_speed;

            // Keep track of the full elapsed time in order to calculate the deviation due to tick
            // slow down
            self.frame_elapsed_time_real += elapsed;

        }

        // Calculate frame number directly from elapsed frame time to avoid issues
        // with timer precision
        let next_simulation_frame = (self.frame_elapsed_time / self.tick_duration).floor() as FrameID + 1;

        self.simulate_receive(next_simulation_frame);
        self.simulate_state(next_simulation_frame, now);
        self.simulate_hash(now)?;
        self.simulate_send(next_simulation_frame);

        // Update phase data for handler calls
        if let ClientPhase::SimulatingOnline(ref mut phase) = &mut self.phase {

            // Calculate the number of frame we're deviating from the target frame rate due to tick
            // slow artifical down
            // TODO speed up simulation if deviation becomes to big
            let frame_deviation = (self.frame_elapsed_time_real - self.frame_elapsed_time) / self.tick_duration;

            phase.frame = (self.frame_last_committed, self.frame_last_simulated);
            phase.frame_deviation = frame_deviation;
            phase.frame_advantage = self.frame_advantage;
            phase.frame_speed = tick_speed;
            phase.state = self.simulation.state();

            // Update list of slow peers
            for p in &self.peers {
                if p.receive_delay(now) >= self.config.slow_threshold {
                    phase.peers_slow.insert(p.id());

                } else {
                    phase.peers_slow.remove(&p.id());
                }
            }

        } else if let ClientPhase::SimulatingOffline(ref mut phase) = &mut self.phase {
            phase.frame = (self.frame_last_committed, self.frame_last_simulated);
            phase.state = self.simulation.state();
        }

        Ok(())

    }

    fn simulate_receive(&mut self, next_simulation_frame: FrameID) {

        // Sample and receive inputs from all peers
        let mut best_input_advantage: f32 = 0.0;
        let mut lowest_input_requested_by_remote = next_simulation_frame;
        let mut lowest_hash_requested_by_remote = next_simulation_frame;
        for peer in &mut self.peers {
            if peer.is_local() {
                let local_input = self.handler.input();
                for i in self.frame_last_simulated..next_simulation_frame {
                    peer.input().received.push((i, Some(local_input)));
                    self.local_inputs.push((i, local_input));
                }

            } else if peer.is_connected() {

                // Receive messages from connected peers
                for m in peer.receive() {
                    if let PeerMessage::Input { requested_frame, inputs } = m {
                        // Remember the oldest frame this peer requests input for
                        peer.input().requested_frame_by_remote = cmp::max(peer.input().requested_frame_by_remote, requested_frame);

                        // Push into received inputs
                        for (frame, input) in inputs {
                            peer.input().received.push((frame, Some(input)));
                        }

                    } else if let PeerMessage::Hash { requested_frame, mut hashes } = m {
                        // Remember the oldest frame this peer requests hashes for
                        peer.hash().requested_frame_by_remote = cmp::max(peer.hash().requested_frame_by_remote, requested_frame);

                        // Push into received inputs
                        peer.hash().received.append(&mut hashes);

                    } else if let PeerMessage::Disconnecting = m {
                        peer.timeout();
                    }

                    // TODO react to sync messages

                }

                // Remember the lowest input frame requested by any of the remote peers
                lowest_input_requested_by_remote = cmp::min(
                    lowest_input_requested_by_remote,
                    peer.input().requested_frame_by_remote
                );

                // Remember the lowest hash frame requested by any of the remote peers
                lowest_hash_requested_by_remote = cmp::min(
                    lowest_hash_requested_by_remote,
                    peer.hash().requested_frame_by_remote
                );

                // Compute input advantage this peer has over us
                let remote_frame_lag = peer.input().requested_frame_by_local as f32 - peer.input().requested_frame_by_remote as f32;
                let local_frame_lag = next_simulation_frame as f32 - peer.input().requested_frame_by_local as f32;
                let input_frame_advantage = local_frame_lag - remote_frame_lag;
                best_input_advantage = best_input_advantage.max(input_frame_advantage);

            } else {
                // For disconnected peers inser empty inputs locally
                for frame in peer.input().requested_frame_by_local..next_simulation_frame {
                    peer.input().received.push((frame, None));
                }
            }

        }

        // Drop local inputs which are no longer requested by any other peer
        // We don't need to buffer them for any longer
        self.local_inputs.retain(|(frame, _)| {
            // The offset ensures that there's always at least one local state in the buffer
            *frame + 1 >= lowest_input_requested_by_remote
        });

        // Drop local hashes which are no longer requested by any other peer
        // We don't need to buffer them for any longer
        self.local_hashes.retain(|(frame, _, _, ids)| {
            // The offset ensures that there's always at least one local hash in the buffer
            *frame + 1 >= lowest_hash_requested_by_remote
            // Don't drop local hashes which have outstanding confirms by remote peers
            || !ids.is_empty()
        });

        // Determine how many frames we are ahead of the lowest requested frame by any other peer
        // `lowest_input_requested_by_remote` can never be `> next_simulation_frame` since we cmp::min() the two
        self.frame_delta = next_simulation_frame - lowest_input_requested_by_remote;

        // Compute the advantage in simualtion frames. Adjusting the simulation by one frame
        // adjusts the input advantage by 2 frames (one on each side).
        self.frame_advantage = 0.5 * best_input_advantage;

        // We can theoretically reduce simulation advantages above 0.5 frames, but we only
        // test against 0.75 for stability given the inaccuracy of our measurements.
        if self.frame_advantage >= 0.75 {
            self.frame_stall_queue = cmp::max((self.frame_advantage + 0.5) as u32, 1);

        } else {
            self.frame_stall_queue = 0;
        }

    }

    fn simulate_state(&mut self, next_simulation_frame: FrameID, now: f64) {

        // Convert received inputs and feed into simulation
        let mut inputs = Vec::new();
        for peer in &mut self.peers {
            let is_local = peer.is_local();
            let id = peer.id();
            for (frame, input) in peer.input().received.drain(0..) {
                if is_local {
                    inputs.push(SimulationInput::Local(frame, id, input.unwrap()));

                } else if let Some(input) = input {
                    inputs.push(SimulationInput::Remote(frame, id, input));

                } else {
                    inputs.push(SimulationInput::Disconnected(frame, id));
                }
            }
        }
        self.simulation.inputs(inputs);

        // Step once for each elapsed frame in order to keep the simulation deterministic
        for _ in self.frame_last_simulated..next_simulation_frame {

            let (committed, reverted) = self.simulation.step(&mut self.handler, now);

            // Use the returned fully completed states / frame for verification purposes
            for (frame, state) in committed {

                // Compare simulation state hashes every N frames with other peers
                if frame % self.config.hash_interval == 0 {
                    // Push the hashed state into the list of pending states
                    let mut set = HashSet::new();
                    for p in &self.peers {
                        // Only insert for peers which are still connected
                        if p.is_remote() && p.is_connected() {
                            set.insert(p.id());
                        }
                    }
                    self.local_hashes.push((frame, state.hash_value(), now, set));
                }

                // Every second we store a state copy for re-sync purposes in case
                // a peer disconnects
                if frame % self.config.sync_interval == 0 {

                    self.resync_states.push_back(SyncState {
                        frame,
                        frame_elapsed_time: self.frame_elapsed_time,
                        frame_elapsed_time_real: self.frame_elapsed_time_real,
                        tick_total: self.tick_total,
                        state: state.clone()
                    });

                    // Limit number of maximum sync states to 1.5 times the peer timeout window
                    let sync_interval = f64::from(self.config.sync_interval) * self.tick_duration;
                    let max_resync_states = (self.config.simulation_timeout / sync_interval * 1.5).ceil() as usize;
                    if self.resync_states.len() > max_resync_states {
                        self.resync_states.pop_front();
                    }

                }

                // Forward committed states to handler
                self.handler.event(Event::StateCommit(frame, state), now);

                // Update latest comitted frame
                self.frame_last_committed = frame;

            }

            // Forward reverted states to handler in reverse order (i.e. newest first)
            for (frame, state) in reverted.into_iter().rev() {
                self.handler.event(Event::StateRevert(frame, state), now);
            }

        }

        // Find the lowest missing input frame for each peer
        let missing_inputs = self.simulation.missing_inputs();
        for peer in &mut self.peers {
            let mut lowest_missing_input_frame = next_simulation_frame + 1;
            for (frame, t) in &missing_inputs {
                if *t == peer.id() {
                    lowest_missing_input_frame = cmp::min(*frame, lowest_missing_input_frame);
                }
            }
            peer.input().requested_frame_by_local = lowest_missing_input_frame;
        }

    }

    fn simulate_hash(&mut self, now: f64) -> Result<(), ()> {

        let mut desync: Option<ClientPhase<H::State>> = None;

        // Go through all peers...
        'peers: for peer in &mut self.peers {
            let id = peer.id();

            // ...and the hashes they send us
            for (p_frame, p_hash) in peer.hash().received.drain(0..) {
                'local: for (frame, hash, created, ids) in &mut self.local_hashes {

                    // Timeout hashes in case the remote peer never sends us their corresponding hash value
                    // We ignore empty local hashes which are still pending confirmation by other peers
                    if !ids.is_empty() && now - *created >= self.config.hash_timeout {
                        H::log("[Core] [Simulating] Hash timeout!");
                        desync = Some(phase::SynchronisationLost::new(*frame, SyncError::HashTimeout(*hash)));
                        break 'peers;

                    // Match the peer hash up with the local hash, comparing them to verify simulation integrity
                    } else if p_frame == *frame && ids.contains(&id) {
                        if p_hash == *hash {
                            ids.remove(&id);
                            break 'local;

                        } else {
                            H::log("[Core] [Simulating] Mismatched hash from peer!");
                            desync = Some(phase::SynchronisationLost::new(*frame, SyncError::HashMismatch(p_hash, id)));
                            break 'peers;
                        }
                    }
                }
            }

        }

        // Handle de-syncs
        if let Some(phase) = desync {
            self.desync(phase, now);
            Err(())

        // For each peer find the lowest hash frame we need for state verification
        } else {
            if !self.local_hashes.is_empty() {
                let highest_hash_frame = self.local_hashes.last().unwrap().0;
                for peer in &mut self.peers {
                    let id = peer.id();
                    let mut lowest_missing_hash_frame = highest_hash_frame;
                    for (frame, _, _, ids) in &mut self.local_hashes {
                        if ids.contains(&id) {
                            lowest_missing_hash_frame = cmp::min(*frame, lowest_missing_hash_frame);
                        }
                    }
                    peer.hash().requested_frame_by_local = lowest_missing_hash_frame;
                }
            }
            Ok(())
        }

    }

    fn simulate_send(&mut self, next_simulation_frame: FrameID) {

        // Send requests for further inputs along with local inputs
        for peer in &mut self.peers {
            if peer.is_remote() {

                // Request all inputs starting from this frame from the other peer
                let requested_input_frame = peer.input().requested_frame_by_local;

                // Send at most `tick_rate` inputs that this peer has requested from us
                let inputs: Vec<(FrameID, <H::State as State>::Input)> = self.local_inputs.iter().take(self.config.tick_rate as usize).filter(|(f, _)| {
                    *f >= peer.input().requested_frame_by_remote

                }).cloned().collect();

                peer.send(PeerMessage::Input {
                    requested_frame: requested_input_frame,
                    inputs
                });

                // Send hash data every 4th tick
                if self.tick_total % 4 == 0 {

                    // Request all hashes starting from this frame from the other peer
                    let requested_hash_frame = peer.hash().requested_frame_by_local;

                    // Send at most 3 hashes that this peer has requests from us
                    let hashes: Vec<(FrameID, u64)> = self.local_hashes.iter().take(3).filter(|(f, _, _, _)| {
                        *f >= peer.hash().requested_frame_by_remote

                    }).map(|(frame, hash, _, _)| (*frame, *hash)).collect();

                    peer.send(PeerMessage::Hash {
                        requested_frame: requested_hash_frame,
                        hashes
                    });

                }

            }
        }

        self.frame_last_simulated = next_simulation_frame;

    }

}

