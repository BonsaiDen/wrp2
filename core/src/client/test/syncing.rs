// Internal Dependencies ------------------------------------------------------
use super::{client_simulation, phase_simulation, phase_syncing, phase_connecting, phase_hosting, phase_peering, phase_starting, sent, clear_sent, ClientPhase};
use crate::Client;
use crate::phase::{self, SyncError};
use crate::test::{TestHandler, StateTransition};
use crate::peer::PeerMessage;


// Tests ----------------------------------------------------------------------
#[test]
fn test_syncing() {

    let mut client = client_syncing();
    assert_eq!(sent(&client.peers[2]), vec![
        (2, 2, vec![PeerMessage::Sync { latest_sync_frame: 0 }]),
        (3, 2, vec![PeerMessage::Sync { latest_sync_frame: 0 }]),
        (4, 2, vec![PeerMessage::Sync { latest_sync_frame: 0 }])

    ], "Should send sync packets to remaining peer");

    // Receive sync package with higher remote sync frame
    client.peers[2].rtc.as_mut().unwrap().packets_received = vec![
        vec![3, 4,   4, 0, 0, 8]
    ];

    client.tick(10006.0);

    // Also timeout peer 2
    client.tick(20007.0);

    // Should immediately return to simulation with only one peer left
    client.tick(20008.0);

    assert_eq!(client.handler.transitions, vec![
        StateTransition::Leave(ClientPhase::Disconnected, 0.0),
        StateTransition::Enter(phase_connecting(), 0.0),
        StateTransition::Leave(phase_connecting(), 0.0),
        StateTransition::Enter(phase_hosting(), 0.0),
        StateTransition::Leave(phase_hosting(), 0.0),
        StateTransition::Enter(phase_peering(), 0.0),
        StateTransition::Leave(phase_peering(), 2.0),
        StateTransition::Enter(phase_starting(), 2.0),
        StateTransition::Leave(phase_starting(), 3.0),
        StateTransition::Enter(phase_simulation((0, 0), 0.0), 3.0),
        StateTransition::Leave(phase_simulation((0, 0), 0.0), 10003.0),
        StateTransition::Enter(phase_syncing(vec![]), 10003.0),
        StateTransition::Leave(phase_syncing(vec![1, 2]), 10006.0),
        StateTransition::Enter(phase_simulation((0, 0), 0.0), 10006.0),
        StateTransition::Leave(phase_simulation((0, 0), 0.0), 20007.0),
        StateTransition::Enter(phase_syncing(vec![]), 20007.0),
        StateTransition::Leave(phase_syncing(vec![1]), 20008.0),
        StateTransition::Enter(phase_simulation((0, 0), 0.0), 20008.0)

    ], "Should sync back to the lowest common sync frame (0)");

}

#[test]
fn test_syncing_timeout() {

    let mut client = client_syncing();

    // Receive another packet from peer 2 to keep alive
    client.peers[2].rtc.as_mut().unwrap().packets_received = vec![
        vec![3, 0]
    ];
    client.tick(20002.0);
    assert_eq!(client.phase, phase_syncing(vec![1]), "Should not timeout syncing phase before 10000ms have passed");

    // Trigger eventual syncing phase timeout
    client.tick(20003.0);

    assert_eq!(client.phase, phase::SynchronisationLost::new(0, SyncError::Timeout), "Should timeout syncing phase after 10000ms have passed");

}

fn client_syncing() -> Client<TestHandler> {

    let mut client = client_simulation();
    client.tick(10003.0);

    // Receive another packet from peer 2 to keep alive
    client.peers[2].rtc.as_mut().unwrap().packets_received = vec![
        vec![2, 0]
    ];
    clear_sent(&mut client.peers[2]);

    // Timeout peer 1
    client.tick(10003.0);
    client.tick(10004.0);
    client.tick(10005.0);
    client

}

