// Internal Dependencies ------------------------------------------------------
use super::{client_create, client_add_peer, client_connect_ws, ClientPhase, phase_connecting, phase_hosting};
use crate::phase;
use crate::traits::JSONSocket;
use crate::test::StateTransition;


// Tests ----------------------------------------------------------------------
#[test]
fn test_socket_connection() {

    let mut client = client_create();

    // Connect ocket
    assert_eq!(client.phase, ClientPhase::Disconnected);
    client.handler.command.disconnected = Some(phase::Disconnected::Command::Connect {
        host: "localhost".to_string(),
        session_id: "session-test".to_string()
    });

    client.tick(0.0);
    assert_eq!(client.phase, phase_connecting());
    assert_eq!(client.handler.transitions, vec![
        StateTransition::Leave(ClientPhase::Disconnected, 0.0),
        StateTransition::Enter(phase_connecting(), 0.0)
    ]);
    assert!(client.socket.is_some(), "Should establish socket connection after handler::tick() retuend Command::Connect");
    assert_eq!(client.socket.as_ref().unwrap().url, "localhost/session-test");
    assert_eq!(client.socket.as_ref().unwrap().is_open(), true);
    assert_eq!(client.socket.as_ref().unwrap().is_closed(), false);

}

#[test]
fn test_socket_connection_lost() {
    let mut client = client_create();
    client_connect_ws(&mut client);;
    assert_eq!(client.phase, phase_connecting());

    client.socket.as_mut().unwrap().connected = false;
    assert_eq!(client.socket.as_ref().unwrap().is_closed(), true);

    client.tick(4.0);
    assert_eq!(client.phase, ClientPhase::ConnectionLost);
    assert_eq!(client.handler.transitions, vec![
        StateTransition::Leave(ClientPhase::Disconnected, 0.0),
        StateTransition::Enter(phase_connecting(), 0.0),
        StateTransition::Leave(phase_connecting(), 4.0),
        StateTransition::Enter(ClientPhase::ConnectionLost, 4.0)
    ]);

}

#[test]
fn test_socket_connection_lost_reset() {
    let mut client = client_create();
    client_connect_ws(&mut client);;
    assert_eq!(client.phase, phase_connecting());

    client.socket.as_mut().unwrap().connected = false;
    assert_eq!(client.socket.as_ref().unwrap().is_closed(), true);

    client.tick(4.0);
    assert_eq!(client.phase, ClientPhase::ConnectionLost);

    client.handler.command.connection_lost = Some(phase::ConnectionLost::Command::Reset);
    client.tick(4.0);
    assert_eq!(client.phase, ClientPhase::Disconnected);

}

#[test]
fn test_socket_connection_lost_reconnect() {
    let mut client = client_create();
    client_connect_ws(&mut client);;
    assert_eq!(client.phase, phase_connecting());

    client.socket.as_mut().unwrap().connected = false;
    assert_eq!(client.socket.as_ref().unwrap().is_closed(), true);

    client.tick(4.0);
    assert_eq!(client.phase, ClientPhase::ConnectionLost);

    client.handler.command.connection_lost = Some(phase::ConnectionLost::Command::Connect {
        host: "localhost".to_string(),
        session_id: "session-test".to_string()
    });
    client.tick(4.0);
    assert_eq!(client.phase, phase_connecting());

}

#[test]
fn test_socket_disconnect() {
    let mut client = client_create();
    client_connect_ws(&mut client);;
    assert_eq!(client.phase, phase_connecting());
    client.tick(3.0);

    client_add_peer(&mut client, false, 1, 1, 48, 0.0);
    client_add_peer(&mut client, true, 0, 0, 47, 0.0);

    client.tick(3.0);
    assert_eq!(client.peers.len(), 2);
    assert_eq!(client.phase, phase_hosting());

    client.handler.command.hosting = Some(phase::Hosting::Command::Disconnect);
    client.tick(4.0);
    assert_eq!(client.phase, ClientPhase::Disconnected);
    assert_eq!(client.handler.transitions, vec![
        StateTransition::Leave(ClientPhase::Disconnected, 0.0),
        StateTransition::Enter(phase_connecting(), 0.0),
        StateTransition::Leave(phase_connecting(), 0.0),
        StateTransition::Enter(phase_hosting(), 0.0),
        StateTransition::Leave(phase_hosting(), 4.0),
        StateTransition::Enter(ClientPhase::Disconnected, 4.0)
    ]);
    assert_eq!(client.peers.len(), 0, "Should remove all local peers after disconnect from socket.");
}


#[test]
fn test_socket_connecting_disconnect() {

    let mut client = client_create();

    // Connect socket
    assert_eq!(client.phase, ClientPhase::Disconnected);
    client.handler.command.disconnected = Some(phase::Disconnected::Command::Connect {
        host: "localhost".to_string(),
        session_id: "session-test".to_string()
    });

    client.tick(0.0);
    assert_eq!(client.phase, phase_connecting());
    client.handler.command.connecting = Some(phase::Connecting::Command::Disconnect);
    client.tick(0.0);
    assert_eq!(client.phase, ClientPhase::Disconnected);

}

