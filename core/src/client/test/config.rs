// Internal Dependencies ------------------------------------------------------
use super::{client_config_create, client_connect_ws, client_add_peer, ws_request, phase_peering_config, phase_hosting_config, phase_started_config, sent, ws_peers};
use std::collections::HashSet;
use crate::PeerID;
use crate::phase::{self, ClientPhase};
use crate::peer::PeerMessage;
use crate::test::{TestStateConfig, TestConfigState};
use crate::socket::{SocketRequest, SocketRequestConfig, SocketRequestPayload};

#[test]
fn test_host_set_config_command() {

    let mut client = client_config_create();
    client_connect_ws(&mut client);
    client_add_peer(&mut client, false, 1, 1, 48, 0.0);
    client_add_peer(&mut client, true, 0, 0, 47, 0.0);
    assert_eq!(client.phase, phase_hosting_config(308180007927971785, TestStateConfig {
        value: 0

    }), "Should have the base config");

    client.handler.command.hosting = Some(phase::Hosting::Command::SetConfig(TestStateConfig {
        value: 7
    }));
    client.tick(2.0);
    assert_eq!(client.socket.as_ref().unwrap().requests, vec![
        SocketRequest {
            to: None,
            peers: vec![],
            version: 42,
            payload: SocketRequestPayload::Advertise(SocketRequestConfig("{\"value\":7}".to_string()))
        }

    ], "Should directly advertise the new config to other peers");

    client.socket.as_mut().unwrap().requests.clear();
    client.tick(500.0);

    client.handler.command.hosting = Some(phase::Hosting::Command::SetConfig(TestStateConfig {
        value: 14
    }));
    client.tick(1000.0);

    assert_eq!(client.socket.as_ref().unwrap().requests, vec![
        SocketRequest {
            to: None,
            peers: vec![],
            version: 42,
            payload: SocketRequestPayload::Advertise(SocketRequestConfig("{\"value\":7}".to_string()))
        },
        SocketRequest {
            to: None,
            peers: vec![],
            version: 42,
            payload: SocketRequestPayload::Advertise(SocketRequestConfig("{\"value\":14}".to_string()))
        }

    ], "Should advertise the updated configuration to other peers.");

    client.tick(10002.0);
    assert_eq!(client.phase, phase_hosting_config(2299446367718509715, TestStateConfig {
        value: 14

    }), "Should have timed out other peers and become host");
    client.handler.command.hosting = Some(phase::Hosting::Command::Start);

    // Transition to started
    client.tick(10002.0);

    // Transition to simulation
    client.tick(10002.0);
    assert_eq!(client.phase, ClientPhase::SimulatingOnline(phase::SimulatingOnline::State {
        frame: (0, 0),
        frame_advantage: 0.0,
        frame_deviation: 0.0,
        frame_speed: 1.0,
        state: TestConfigState {
            config: TestStateConfig {
                value: 14
            },
            peer_ids: vec![PeerID(0)]
        },
        peers_slow: HashSet::new()

    }), "Should create the initial state with the correct configuration");

}

#[test]
fn test_peer_set_config_command() {

    let mut client = client_config_create();
    client_connect_ws(&mut client);
    client_add_peer(&mut client, true, 1, 1, 48, 0.0);
    client_add_peer(&mut client, false, 0, 0, 47, 0.0);
    assert_eq!(client.phase, phase_peering_config(308180007927971785, TestStateConfig {
        value: 0

    }), "Should have the base config");

    client.handler.command.peering = Some(phase::Peering::Command::RequestConfig(TestStateConfig {
        value: 7
    }));

    client.tick(2.0);
    assert_eq!(client.phase, phase_peering_config(308180007927971785, TestStateConfig {
        value: 0

    }), "Should not modify the local config");

    client.tick(500.0);
    assert_eq!(client.socket.as_ref().unwrap().requests, vec![
        SocketRequest {
            to: None,
            peers: vec![],
            version: 42,
            payload: SocketRequestPayload::ConfigRequest(SocketRequestConfig("{\"value\":7}".to_string()))
        },
        SocketRequest {
            to: Some(PeerID(0)),
            peers: vec![],
            version: 42,
            payload: SocketRequestPayload::PeeringOffer(vec!["peer-signal-offer-1".to_string()])
        },
        SocketRequest {
            to: None,
            peers: vec![],
            version: 42,
            payload: SocketRequestPayload::Advertise(SocketRequestConfig("{\"value\":0}".to_string()))
        }

    ], "Should send the config request and continue to advertise the unmodified base config to other peers.");

    client.socket.as_mut().unwrap().requests.clear();
    client.handler.command.peering = Some(phase::Peering::Command::RequestConfig(TestStateConfig {
        value: 0
    }));
    client.tick(2.0);
    assert_eq!(client.socket.as_ref().unwrap().requests, vec![], "Should not send ConfigRequest in case config matches the local one");

}

#[test]
fn test_host_merge_config_request() {

    let mut client = client_config_create();
    client_connect_ws(&mut client);
    client_add_peer(&mut client, false, 1, 1, 48, 0.0);
    client_add_peer(&mut client, true, 0, 0, 47, 0.0);
    assert_eq!(client.phase, phase_hosting_config(308180007927971785, TestStateConfig {
        value: 0

    }), "Should have the base config");

    // Receive config request from peer
    client.socket.as_mut().unwrap().received = ws_request(
        false,
        1,
        1,
        48,
        SocketRequest {
            to: None,
            peers: vec![],
            version: 42,
            payload: SocketRequestPayload::ConfigRequest(SocketRequestConfig("{\"value\":7}".to_string()))
        }
    );
    client.tick(2.0);

    assert_eq!(client.socket.as_ref().unwrap().requests, vec![
        SocketRequest {
            to: None,
            peers: vec![],
            version: 42,
            payload: SocketRequestPayload::Advertise(SocketRequestConfig("{\"value\":1035}".to_string()))
        }
    ], "Should directly advertise the merged configuration");

    assert_eq!(client.phase, phase_hosting_config(18148212416216331885, TestStateConfig {
        value: 5 * 7 + 1000

    }), "Should have merged the received config into the local one");

    client.socket.as_mut().unwrap().requests.clear();
    client.socket.as_mut().unwrap().received = ws_request(
        false,
        1,
        1,
        48,
        SocketRequest {
            to: None,
            peers: vec![],
            version: 42,
            payload: SocketRequestPayload::ConfigRequest(SocketRequestConfig("{\"value\":7}".to_string()))
        }
    );
    client.tick(2.0);
    assert_eq!(client.socket.as_ref().unwrap().requests, vec![], "Should not advertise config merge resulted in no change");

    client.socket.as_mut().unwrap().received = ws_request(
        false,
        1,
        1,
        48,
        SocketRequest {
            to: None,
            peers: vec![],
            version: 42,
            payload: SocketRequestPayload::ConfigRequest(SocketRequestConfig("{\"vlue\":9}".to_string()))
        }
    );
    client.tick(2.0);
    assert_eq!(client.phase, phase_hosting_config(18148212416216331885, TestStateConfig {
        value: 5 * 7 + 1000

    }), "Should ignore malformed configs from peers");

}

#[test]
fn test_peer_ignore_config_request() {

    let mut client = client_config_create();
    client_connect_ws(&mut client);
    client_add_peer(&mut client, true, 1, 1, 48, 0.0);
    client_add_peer(&mut client, false, 0, 0, 47, 0.0);
    assert_eq!(client.phase, phase_peering_config(308180007927971785, TestStateConfig {
        value: 0

    }), "Should have the base config");

    // Receive config request from peer
    client.socket.as_mut().unwrap().received = ws_request(
        false,
        0,
        0,
        48,
        SocketRequest {
            to: None,
            peers: vec![],
            version: 42,
            payload: SocketRequestPayload::ConfigRequest(SocketRequestConfig("{\"value\":7}".to_string()))
        }
    );
    client.tick(2.0);

    assert_eq!(client.phase, phase_peering_config(308180007927971785, TestStateConfig {
        value: 0

    }), "Should still have the base config");

}

#[test]
fn test_peer_use_host_config() {

    let mut client = client_config_create();
    client_connect_ws(&mut client);
    client_add_peer(&mut client, true, 1, 1, 48, 0.0);
    client_add_peer(&mut client, false, 0, 0, 47, 0.0);
    client_add_peer(&mut client, false, 2, 0, 45, 0.0);

    assert_eq!(client.phase, phase_peering_config(308180007927971785, TestStateConfig {
        value: 0

    }), "Should have the base config");

    client.socket.as_mut().unwrap().received = ws_request(
        false,
        0,
        0,
        47,
        SocketRequest {
            to: Some(PeerID(1)),
            peers: vec![],
            version: 42,
            payload: SocketRequestPayload::Advertise(SocketRequestConfig("{\"value\":7}".to_string()))
        }
    );
    client.tick(2.0);
    assert_eq!(client.phase, phase_peering_config(10469944127758045432, TestStateConfig {
        value: 7

    }), "Should copy the config from the host");

    client.socket.as_mut().unwrap().received = ws_request(
        false,
        0,
        0,
        47,
        SocketRequest {
            to: Some(PeerID(1)),
            peers: vec![],
            version: 42,
            payload: SocketRequestPayload::Advertise(SocketRequestConfig("{\"value\":14}".to_string()))
        }
    );
    client.tick(2.0);
    assert_eq!(client.phase, phase_peering_config(2299446367718509715, TestStateConfig {
        value: 14

    }), "Should always copy the config from the host");

    client.socket.as_mut().unwrap().received = ws_request(
        false,
        2,
        2,
        45,
        SocketRequest {
            to: Some(PeerID(1)),
            peers: vec![],
            version: 42,
            payload: SocketRequestPayload::Advertise(SocketRequestConfig("{\"value\":28}".to_string()))
        }
    );
    client.tick(2.0);
    assert_eq!(client.phase, phase_peering_config(2299446367718509715, TestStateConfig {
        value: 14

    }), "Should not copy the config from peers other than the host");

    client.socket.as_mut().unwrap().received = ws_request(
        false,
        0,
        0,
        47,
        SocketRequest {
            to: Some(PeerID(1)),
            peers: vec![],
            version: 42,
            payload: SocketRequestPayload::Advertise(SocketRequestConfig("{\"alue\":28}".to_string()))
        }
    );
    client.tick(2.0);
    assert_eq!(client.phase, phase_peering_config(2299446367718509715, TestStateConfig {
        value: 14

    }), "Should ignore malformed configs from the host");

    // Timeout other peers
    client.tick(10002.0);
    assert_eq!(client.phase, phase_hosting_config(2299446367718509715, TestStateConfig {
        value: 14

    }), "Should have timed out other peers and become host");
    client.handler.command.hosting = Some(phase::Hosting::Command::Start);

    // Transition to started
    client.tick(10002.0);

    // Transition to simulation
    client.tick(10002.0);
    assert_eq!(client.phase, ClientPhase::SimulatingOnline(phase::SimulatingOnline::State {
        frame: (0, 0),
        frame_advantage: 0.0,
        frame_deviation: 0.0,
        frame_speed: 1.0,
        state: TestConfigState {
            config: TestStateConfig {
                value: 14
            },
            peer_ids: vec![PeerID(1)]
        },
        peers_slow: HashSet::new()

    }), "Should create the initial state with the correct configuration");

}

#[test]
fn test_host_wait_for_peer_config_match() {

    let mut client = client_config_create();
    client_connect_ws(&mut client);
    client_add_peer(&mut client, true, 0, 0, 48, 0.0);
    client_add_peer(&mut client, false, 1, 1, 47, 0.0);
    client_add_peer(&mut client, false, 2, 2, 46, 0.0);
    client.peers[1].rtc.as_mut().unwrap().connected = true;
    client.peers[2].rtc.as_mut().unwrap().connected = true;

    assert_eq!(client.phase, phase_hosting_config(308180007927971785, TestStateConfig {
        value: 0

    }), "Should have the base config");

    client.handler.command.hosting = Some(phase::Hosting::Command::SetConfig(TestStateConfig {
        value: 7
    }));
    client.tick(1.0);

    // Mark peers as ready
    client.socket.as_mut().unwrap().received = ws_peers(1, 1, vec![PeerID(0), PeerID(2)]);
    client.tick(1.0);
    client.socket.as_mut().unwrap().received = ws_peers(2, 2, vec![PeerID(0), PeerID(1)]);

    client.handler.command.hosting = Some(phase::Hosting::Command::Start);
    client.tick(2.0);

    // Should not yet start before other peers have advertised the same config hash
    assert_eq!(client.phase, phase_started_config(10469944127758045432, TestStateConfig {
        value: 7

    }), "Should not start the simulation before the all peers have advertised the same configuration");

    assert_eq!(sent(&client.peers[1]), vec![
        (0, 0, vec![]),
        (1, 0, vec![]),
        (2, 0, vec![])

    ], "Should not yet send starting packets before all peers have advertised the same configuration");

    // Receive updated config from peers
    client.socket.as_mut().unwrap().received = ws_request(
        false,
        1,
        1,
        47,
        SocketRequest {
            to: None,
            peers: vec![],
            version: 42,
            payload: SocketRequestPayload::Advertise(SocketRequestConfig("{\"value\":7}".to_string()))
        }
    );
    client.tick(2.0);

    client.socket.as_mut().unwrap().received = ws_request(
        false,
        2,
        2,
        46,
        SocketRequest {
            to: None,
            peers: vec![],
            version: 42,
            payload: SocketRequestPayload::Advertise(SocketRequestConfig("{\"value\":7}".to_string()))
        }
    );
    client.tick(2.0);
    assert_eq!(client.phase, phase_started_config(10469944127758045432, TestStateConfig {
        value: 7

    }), "Should start the simulation after all peers have advertised a config that matches the host");

    assert_eq!(sent(&client.peers[1]), vec![
        (0, 0, vec![]),
        (1, 0, vec![]),
        (2, 0, vec![]),
        (3, 0, vec![]),
        (4, 0, vec![PeerMessage::Starting(vec![PeerID(0), PeerID(1), PeerID(2)])])

    ], "Should send starting packets once all peers have advertised the same configuration");

}

