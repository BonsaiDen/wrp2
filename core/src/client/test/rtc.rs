// Internal Dependencies ------------------------------------------------------
use super::{client_create, client_connect_ws, client_add_peer, ws_request, ws_peers, sent};
use crate::PeerID;
use crate::socket::{SocketRequest, SocketRequestConfig, SocketRequestPayload};


// Tests ----------------------------------------------------------------------
#[test]
fn test_rtc_accept() {
    let mut client = client_create();
    client_connect_ws(&mut client);;
    client_add_peer(&mut client, true, 0, 0, 47, 0.0);
    client_add_peer(&mut client, false, 1, 1, 48, 0.0);
    client_add_peer(&mut client, false, 2, 2, 49, 0.0);
    client.tick(0.0);
    assert_eq!(client.socket.as_ref().unwrap().requests, vec![], "Should not offer rtc connections if local seed is < other peer seeds");

    assert_eq!(client.peers[0].rtc.is_some(), false, "Should not create a rtc connection for the local peer");
    assert_eq!(client.peers[1].rtc.is_some(), true, "Should still create a rtc connection to non-local peer for accepting incoming connections");
    assert_eq!(client.peers[2].rtc.is_some(), true, "Should still create a rtc connection to non-local peer for accepting incoming connections");

    assert_eq!(client.peers[1].rtc.as_ref().unwrap().initiator, false);
    assert_eq!(client.peers[2].rtc.as_ref().unwrap().initiator, false);
}

#[test]
fn test_rtc_offer() {

    let mut client = client_create();
    client_connect_ws(&mut client);
    client_add_peer(&mut client, true, 0, 0, 49, 0.0);
    client_add_peer(&mut client, false, 1, 1, 48, 0.0);

    assert_eq!(client.peers[1].rtc.is_some(), true, "Should create a rtc connection to the remote peer");
    assert_eq!(client.peers[1].rtc.as_ref().unwrap().initiator, true);

    assert_eq!(client.socket.as_ref().unwrap().requests, vec![], "Should only generate peering signals every 500ms");

    client.tick(500.0);
    assert_eq!(client.socket.as_ref().unwrap().requests, vec![
        SocketRequest {
            to: Some(PeerID(1)),
            peers: vec![],
            version: 42,
            payload: SocketRequestPayload::PeeringOffer(vec!["peer-signal-offer-1".to_string()])

        },
        SocketRequest {
            to: None,
            peers: vec![],
            version: 42,
            payload: SocketRequestPayload::Advertise(SocketRequestConfig("{\"value\":0}".to_string()))
        }

    ], "Should offer rtc connections if local seed is >= other peer seed");
    client.socket.as_mut().unwrap().requests.clear();

    client_add_peer(&mut client, false, 2, 2, 50, 0.0);

    client.tick(1000.0);
    assert_eq!(client.socket.as_ref().unwrap().requests, vec![
        SocketRequest {
            to: Some(PeerID(1)),
            peers: vec![],
            version: 42,
            payload: SocketRequestPayload::PeeringOffer(vec![
                "peer-signal-offer-1".to_string(),
                "peer-signal-offer-2".to_string()
            ])
        },
        SocketRequest {
            to: None,
            peers: vec![],
            version: 42,
            payload: SocketRequestPayload::Advertise(SocketRequestConfig("{\"value\":0}".to_string()))
        }

    ], "Should continue to offer rtc connections if local seed is >= other peer seed");
    client.socket.as_mut().unwrap().requests.clear();

    client_add_peer(&mut client, false, 3, 3, 47, 0.0);

    client.tick(1500.0);
    assert_eq!(client.socket.as_ref().unwrap().requests, vec![
        SocketRequest {
            to: Some(PeerID(1)),
            peers: vec![],
            version: 42,
            payload: SocketRequestPayload::PeeringOffer(vec![
                "peer-signal-offer-1".to_string(),
                "peer-signal-offer-2".to_string(),
                "peer-signal-offer-3".to_string()
            ])
        },
        SocketRequest {
            to: Some(PeerID(3)),
            peers: vec![],
            version: 42,
            payload: SocketRequestPayload::PeeringOffer(vec![
                "peer-signal-offer-1".to_string()
            ])
        },
        SocketRequest {
            to: None,
            peers: vec![],
            version: 42,
            payload: SocketRequestPayload::Advertise(SocketRequestConfig("{\"value\":0}".to_string()))
        }

    ], "Should offer rtc connections to multiple peers if local seed is >= other peer seed");

}

#[test]
fn test_rtc_answer() {
    let mut client = client_create();
    client_connect_ws(&mut client);;
    client_add_peer(&mut client, true, 0, 0, 47, 0.0);
    client_add_peer(&mut client, false, 1, 1, 48, 0.0);

    assert_eq!(client.peers[1].rtc.is_some(), true, "Should create a rtc connection to the remote peer");
    assert_eq!(client.peers[1].rtc.as_ref().unwrap().initiator, false);

    client.socket.as_mut().unwrap().received = ws_request(
        false,
        1,
        1,
        48,
        SocketRequest {
            to: Some(PeerID(0)),
            peers: vec![],
            version: 42,
            payload: SocketRequestPayload::PeeringOffer(vec!["peer-signal-offer-1".to_string()])
        }
    );
    client.tick(0.0);

    assert_eq!(client.peers[1].rtc.as_ref().unwrap().received_signals, vec![
        "peer-signal-offer-1".to_string()
    ]);

    client.tick(500.0);
    assert_eq!(client.socket.as_ref().unwrap().requests, vec![
        SocketRequest {
            to: Some(PeerID(1)),
            peers: vec![],
            version: 42,
            payload: SocketRequestPayload::PeeringAnswer(vec!["peer-signal-offer-1".to_string()])
        },
        SocketRequest {
            to: None,
            peers: vec![],
            version: 42,
            payload: SocketRequestPayload::Advertise(SocketRequestConfig("{\"value\":0}".to_string()))
        }

    ], "Should respond with answer signals");
    client.socket.as_mut().unwrap().requests.clear();

    client.tick(1000.0);
    assert_eq!(client.socket.as_ref().unwrap().requests, vec![
        SocketRequest {
            to: Some(PeerID(1)),
            peers: vec![],
            version: 42,
            payload: SocketRequestPayload::PeeringAnswer(vec![
                "peer-signal-offer-1".to_string(),
                "peer-signal-offer-1".to_string()
            ])
        },
        SocketRequest {
            to: None,
            peers: vec![],
            version: 42,
            payload: SocketRequestPayload::Advertise(SocketRequestConfig("{\"value\":0}".to_string()))
        }

    ], "Should respond with answer signals");

}

#[test]
fn test_rtc_advertise_peers() {
    let mut client = client_create();
    client_connect_ws(&mut client);;
    client_add_peer(&mut client, true, 0, 0, 47, 0.0);
    client_add_peer(&mut client, false, 1, 1, 48, 0.0);
    client_add_peer(&mut client, false, 2, 2, 49, 0.0);
    client.tick(0.0);

    // Mark rtc connections as connected
    client.peers[1].rtc.as_mut().unwrap().connected = true;
    client.tick(500.0);
    client.peers[2].rtc.as_mut().unwrap().connected = true;
    client.tick(1000.0);

    assert_eq!(client.socket.as_ref().unwrap().requests, vec![
        SocketRequest {
            to: None,
            peers: vec![PeerID(1)],
            version: 42,
            payload: SocketRequestPayload::Advertise(SocketRequestConfig("{\"value\":0}".to_string()))
        },
        SocketRequest {
            to: None,
            peers: vec![PeerID(1), PeerID(2)],
            version: 42,
            payload: SocketRequestPayload::Advertise(SocketRequestConfig("{\"value\":0}".to_string()))
        }

    ], "Should advertise the list of connected peers");

}

#[test]
fn test_rtc_is_ready() {
    let mut client = client_create();
    client_connect_ws(&mut client);;
    client_add_peer(&mut client, true, 0, 0, 47, 0.0);
    client_add_peer(&mut client, false, 1, 1, 48, 0.0);
    client_add_peer(&mut client, false, 2, 2, 49, 0.0);
    client_add_peer(&mut client, false, 3, 3, 50, 0.0);
    client.tick(0.0);

    assert_eq!(client.peers[0].is_ready(), false, "Local peer should not be ready by default");

    // Mark local rtc connections as connected
    client.peers[1].rtc.as_mut().unwrap().connected = true;
    client.peers[2].rtc.as_mut().unwrap().connected = true;

    // Peer 3 is not connected locally in this setup
    client.peers[3].rtc.as_mut().unwrap().connected = false;

    // Remote peers should only be considered ready when they have established a
    // rtc connection to ALL of the locally established connections too including
    // the local peer itself
    //
    // We're connected to vec![PeerID(1), PeerID(2)]

    // Ready Cases

    client.socket.as_mut().unwrap().received = ws_peers(1, 1, vec![PeerID(0), PeerID(2)]);
    client.tick(0.0);
    assert_eq!(client.peers[1].is_ready(), true);

    client.socket.as_mut().unwrap().received = ws_peers(2, 2, vec![PeerID(0), PeerID(1), PeerID(3)]);
    client.tick(0.0);
    assert_eq!(client.peers[2].is_ready(), true);

    // Not-Ready Cases

    client.socket.as_mut().unwrap().received = ws_peers(1, 1, vec![PeerID(0), PeerID(3)]);
    client.tick(0.0);
    assert_eq!(client.peers[1].is_ready(), false);

    client.socket.as_mut().unwrap().received = ws_peers(2, 2, vec![PeerID(0)]);
    client.tick(0.0);
    assert_eq!(client.peers[2].is_ready(), false);


    // Now connect peer 3 locally and test some more
    client.peers[3].rtc.as_mut().unwrap().connected = true;

    client.socket.as_mut().unwrap().received = ws_peers(1, 1, vec![PeerID(0), PeerID(2)]);
    client.tick(0.0);
    assert_eq!(client.peers[1].is_ready(), false);

    client.socket.as_mut().unwrap().received = ws_peers(2, 2, vec![PeerID(0), PeerID(1), PeerID(3)]);
    client.tick(0.0);
    assert_eq!(client.peers[2].is_ready(), true);

}

#[test]
fn test_rtc_timeout() {
    let mut client = client_create();
    client_connect_ws(&mut client);;
    client_add_peer(&mut client, true, 0, 0, 47, 0.0);
    client_add_peer(&mut client, false, 1, 1, 48, 0.0);
    client.tick(2500.0);
    assert_eq!(client.socket.as_ref().unwrap().requests, vec![
        SocketRequest {
            to: None,
            peers: vec![],
            version: 42,
            payload: SocketRequestPayload::Advertise(SocketRequestConfig("{\"value\":0}".to_string()))
        }
    ], "Should timeout rtc connection signaling after 2500ms");
}

#[test]
fn test_rtc_connect() {

    let mut client = client_create();
    client_connect_ws(&mut client);
    client_add_peer(&mut client, true, 0, 0, 48, 0.0);
    client_add_peer(&mut client, false, 1, 1, 47, 0.0);

    // Before rtc connection is established
    client.tick(500.0);
    assert_eq!(client.socket.as_ref().unwrap().requests, vec![
        SocketRequest {
            to: Some(PeerID(1)),
            peers: vec![],
            version: 42,
            payload: SocketRequestPayload::PeeringOffer(vec!["peer-signal-offer-1".to_string()])
        },
        SocketRequest {
            to: None,
            peers: vec![],
            version: 42,
            payload: SocketRequestPayload::Advertise(SocketRequestConfig("{\"value\":0}".to_string()))
        }
    ]);
    client.socket.as_mut().unwrap().requests.clear();

    // After rtc connection is established
    client.peers[1].rtc.as_mut().unwrap().connected = true;
    client.tick(0.0);
    assert_eq!(client.socket.as_ref().unwrap().requests, vec![], "Should no longer send peering signals once the the rtc connection is established.");

}

#[test]
fn test_rtc_packet_send() {
    let mut client = client_create();
    client_connect_ws(&mut client);
    client_add_peer(&mut client, true, 0, 0, 48, 0.0);
    client_add_peer(&mut client, false, 1, 1, 47, 0.0);
    client.peers[1].rtc.as_mut().unwrap().connected = true;

    client.tick(0.0);
    client.tick(0.0);
    client.tick(0.0);
    client.tick(0.0);
    client.tick(0.0);
    client.tick(0.0);
    client.tick(0.0);
    client.tick(0.0);

    // Packets
    assert_eq!(sent(&client.peers[1]), vec![
        (0, 0, vec![]),
        (1, 0, vec![]),
        (2, 0, vec![]),
        (3, 0, vec![]),
        (4, 0, vec![]),
        (5, 0, vec![]),
        (6, 0, vec![]),
        (7, 0, vec![])

    ], "Should send keep a live packets over the connection");
}

