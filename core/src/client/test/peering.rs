// Internal Dependencies ------------------------------------------------------
use super::{client_create, client_peers, client_connect_ws, client_add_peer, ws_advertise, ClientPhase, phase_connecting, phase_hosting, phase_peering};
use crate::PeerID;
use crate::phase;
use crate::socket::{SocketRequest, SocketRequestConfig, SocketRequestPayload};
use crate::test::StateTransition;


// Tests ----------------------------------------------------------------------
#[test]
fn test_local_peer() {

    let mut client = client_create();

    // Connect WebSocket
    client_connect_ws(&mut client);

    // Create Peer 1
    client.socket.as_mut().unwrap().received = ws_advertise(true, 42, 0, 128, 42);
    client.tick(1.0);
    assert_eq!(client.peers.len(), 1, "Should add a newly discovered peer");
    assert_eq!(client.peers[0].is_local(), true, "Should connect the local peer");
    assert_eq!(client.phase, phase_hosting());

    client.tick(2.0);
    assert_eq!(client.peers.len(), 1, "Should add a newly discovered peer");

    // Update Peer 1
    client.socket.as_mut().unwrap().received = ws_advertise(true, 42, 0, 128, 42);
    client.tick(2.0);
    assert_eq!(client.peers.len(), 1, "Should not add duplicated peers");

    // Advertise to other peers
    client.tick(500.0);
    assert_eq!(client.socket.as_ref().unwrap().requests, vec![], "Should not before advertise 500ms if local peer is the only connected peer.");
    client.tick(501.0);
    assert_eq!(client.socket.as_ref().unwrap().requests, vec![
        SocketRequest {
            to: None,
            peers: vec![],
            version: 42,
            payload: SocketRequestPayload::Advertise(SocketRequestConfig("{\"value\":0}".to_string()))
        }

    ], "Should advertise every 500ms if local peer is the only connected peer.");

    // Local Peer should not timeout
    client.tick(2000.0);
    assert_eq!(client.peers.len(), 1, "Should not timeout local peers");
    client.tick(4000.0);
    assert_eq!(client.peers.len(), 1, "Should not timeout local peers");

    assert_eq!(client.handler.transitions, vec![
        StateTransition::Leave(ClientPhase::Disconnected, 0.0),
        StateTransition::Enter(phase_connecting(), 0.0),
        StateTransition::Leave(phase_connecting(), 1.0),
        StateTransition::Enter(phase_hosting(), 1.0)
    ]);
    assert_eq!(client.handler.peer_connects, vec![
        (PeerID(42), 1.0),
    ]);
    assert_eq!(client.handler.peer_disconnects, vec![]);

}

#[test]
fn test_remote_peer() {

    let mut client = client_create();
    client_connect_ws(&mut client);;
    assert_eq!(client.phase, phase_connecting());

    // Remote Peer
    client.socket.as_mut().unwrap().received = ws_advertise(false, 1, 1, 127, 42);
    client.tick(0.0);
    assert_eq!(client.peers.len(), 1, "Should add additional discovered peer");
    assert_eq!(client.peers[0].is_remote(), true, "Should connect the remote peer");

    // Remote peer should timeout
    client.tick(1999.0);
    assert_eq!(client.peers.len(), 1, "Should not timeout remote peers until no messages are received for the specified timemout");
    client.tick(2000.0);
    assert_eq!(client.peers.len(), 0, "Should timeout remote peers when no messages are received");

    // Re-add peer
    client.socket.as_mut().unwrap().received = ws_advertise(false, 1, 1, 127, 42);
    client.tick(4000.0);
    assert_eq!(client.peers.len(), 1, "Should add additional discovered peer");
    assert_eq!(client.peers[0].is_remote(), true, "Should connect the remote peer");

    // Check client advertising
    assert_eq!(client.socket.as_ref().unwrap().requests, vec![
        SocketRequest {
            to: None,
            peers: vec![],
            version: 42,
            payload: SocketRequestPayload::Advertise(SocketRequestConfig("{\"value\":0}".to_string()))
        },
        SocketRequest {
            to: None,
            peers: vec![],
            version: 42,
            payload: SocketRequestPayload::Advertise(SocketRequestConfig("{\"value\":0}".to_string()))
        }

    ], "Client should advertise in regular intervals");

    assert_eq!(client.phase, phase_connecting(), "Peering Phase should not be entered unless a local peer exists.");

    assert_eq!(client.handler.peer_connects, vec![
        (PeerID(1), 0.0),
        (PeerID(1), 4000.0)

    ]);
    assert_eq!(client.handler.peer_disconnects, vec![
        (PeerID(1), 2000.0)
    ]);

}

#[test]
fn test_remote_ignore_other_versions() {

    let mut client = client_create();
    client_connect_ws(&mut client);;
    assert_eq!(client.phase, phase_connecting());

    // Remote Peer
    client.socket.as_mut().unwrap().received = ws_advertise(false, 1, 1, 127, 128);
    client.tick(0.0);
    assert_eq!(client.peers.len(), 0, "Should ignore advertisements from peers with other versions");

}

#[test]
fn test_fast_advertise() {
    let mut client = client_create();
    client_connect_ws(&mut client);;
    client_add_peer(&mut client, true, 0, 0, 48, 1.0);
    client_add_peer(&mut client, false, 1, 1, 96, 2.0);
    client.tick(101.0);
    client.tick(201.0);
    assert_eq!(client.socket.as_ref().unwrap().requests, vec![
        SocketRequest {
            to: None,
            peers: vec![],
            version: 42,
            payload: SocketRequestPayload::Advertise(SocketRequestConfig("{\"value\":0}".to_string()))
        },
        SocketRequest {
            to: None,
            peers: vec![],
            version: 42,
            payload: SocketRequestPayload::Advertise(SocketRequestConfig("{\"value\":0}".to_string()))
        }
    ], "Should advertise every 100ms as long as there are peers without a p2p connection established between them");

    assert_eq!(client.handler.peer_connects, vec![
        (PeerID(0), 1.0),
        (PeerID(1), 2.0)
    ]);
    assert_eq!(client.handler.peer_disconnects, vec![]);

}

#[test]
fn test_peer_sorting() {

    let mut client = client_create();
    client_connect_ws(&mut client);;

    // Sort peers by server index
    client_add_peer(&mut client, true, 2, 2, 48, 0.0);
    client_add_peer(&mut client, false, 0, 0, 96, 0.5);
    client_add_peer(&mut client, false, 1, 1, 128, 1.0);

    assert_eq!(client.peers.len(), 3);
    assert_eq!(client_peers(&mut client), vec![
        PeerID(0),
        PeerID(1),
        PeerID(2)

    ], "Should sort peers by their server index");

    assert_eq!(
        client.phase,
        phase_peering(),
        "Should be Peer if local client is not the first one in the list after sorting by server index"
    );

    // Re-order peers by server index
    client_add_peer(&mut client, true, 2, 0, 48, 2.0);
    client_add_peer(&mut client, false, 0, 2, 96, 2.0);
    client_add_peer(&mut client, false, 1, 1, 128, 2.0);

    assert_eq!(client.peers.len(), 3);
    assert_eq!(client_peers(&mut client), vec![
       PeerID(2),
       PeerID(1),
       PeerID(0)

    ], "Should sort peers by their server index");

    assert_eq!(
        client.phase,
        phase_hosting(),
        "Should be Host if local client is the first one in the list after sorting by server index"
    );

    assert_eq!(client.handler.transitions, vec![
        StateTransition::Leave(ClientPhase::Disconnected, 0.0),
        StateTransition::Enter(phase_connecting(), 0.0),
        StateTransition::Leave(phase_connecting(), 0.0),
        StateTransition::Enter(phase_hosting(), 0.0),
        StateTransition::Leave(phase_hosting(), 0.5),
        StateTransition::Enter(phase_peering(), 0.5),
        StateTransition::Leave(phase_peering(), 2.0),
        StateTransition::Enter(phase_hosting(), 2.0)
    ]);

    assert_eq!(client.handler.peer_connects, vec![
        (PeerID(2), 0.0),
        (PeerID(0), 0.5),
        (PeerID(1), 1.0)
    ]);
    assert_eq!(client.handler.peer_disconnects, vec![]);

}

#[test]
fn test_peering_disconnect() {
    let mut client = client_create();
    client_connect_ws(&mut client);;
    client_add_peer(&mut client, true, 1, 1, 48, 1.0);
    client_add_peer(&mut client, false, 0, 0, 96, 2.0);
    assert_eq!(client.phase, phase_peering());
    client.handler.command.peering = Some(phase::Peering::Command::Disconnect);
    client.tick(1.0);
    assert_eq!(client.phase, ClientPhase::Disconnected);
}

