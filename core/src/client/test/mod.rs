// STD Dependencies -----------------------------------------------------------
use std::collections::HashSet;


// Internal Dependencies ------------------------------------------------------
use super::{Config, Client, Handler, PeerID};
use crate::peer::{Peer, PeerMessage};
use crate::phase::{self, ClientPhase};
use crate::test::{TestHandler, TestHandlerSocket, TestConfigHandler, TestInputHandler, TestState, TestConfigState, TestInput, PacketConnection, StateTransition, TestStateConfig, WebSocketHandler};
use crate::socket::{SocketRequest, SocketRequestPayload, SocketRequestConfig, SocketResponse};


// Modules --------------------------------------------------------------------
mod config;
mod hashing;
mod offline;
mod rtc;
mod peering;
mod simulation;
mod starting;
mod socket;
mod syncing;


// Tests ----------------------------------------------------------------------
#[test]
fn test_new() {
    let handler = TestHandler::new(Config::default());
    let mut client = Client::new(handler, 0.0);

    assert_eq!(client.tick_duration, 33.0);
    assert_eq!(client.tick_total, 0);
    assert_eq!(client.tick_last_time, 0.0);
    assert_eq!(client.tick_init_time, 0.0);

    assert_eq!(client.frame_elapsed_time, 0.0);
    assert_eq!(client.frame_last_simulated, 0);
    assert_eq!(client.frame_last_committed, 0);

    client.tick(1.0);
    assert_eq!(client.tick_total, 0);
    assert_eq!(client.tick_last_time, 1.0);
    assert_eq!(client.tick_init_time, 0.0);

    client.draw(1.0);

    assert!(client.socket.is_none());
    assert!(client.local_hashes.is_empty());
    assert!(client.local_inputs.is_empty());
    assert!(client.resync_states.is_empty());
    assert!(client.peers.is_empty());

    assert_eq!(client.phase, ClientPhase::Disconnected);
    assert_eq!(client.handler.transitions, vec![
        StateTransition::Enter(ClientPhase::Disconnected, 0.0)
    ]);

}

// Test Helpers -----------------------------------------------------------
fn client_create() -> Client<TestHandler> {
    let handler = TestHandler::new(Config::default());
    let mut client = Client::new(handler, 0.0);
    client.handler.transitions.clear();
    client
}

fn client_config_create() -> Client<TestConfigHandler> {
    let handler = TestConfigHandler::new(Config::default());
    let mut client = Client::new(handler, 0.0);
    client.handler.transitions.clear();
    client
}

fn client_connect_ws<H>(client: &mut Client<H>)
where H:
    'static + Handler + WebSocketHandler +
    phase::Disconnected::Phase +
    phase::Connecting::Phase<<H as Handler>::State> +
    phase::Hosting::Phase<<H as Handler>::State> +
    phase::Peering::Phase<<H as Handler>::State> +
    phase::Started::Phase<<H as Handler>::State> +
    phase::Starting::Phase<<H as Handler>::State> +
    phase::ConnectionLost::Phase +
    phase::Disconnecting::Phase +
    phase::SimulatingOnline::Phase<<H as Handler>::State> +
    phase::SimulatingOffline::Phase<<H as Handler>::State> +
    phase::PausedOffline::Phase<<H as Handler>::State> +
    phase::Synchronizing::Phase +
    phase::SynchronisationLost::Phase
{
    client.handler.connect_web_socket();
    client.tick(0.0);
    client.tick(0.0);
}

fn client_add_peer<H>(client: &mut Client<H>, local: bool, id: u16, index: u32, seed: u32, now: f64)
where H:
    'static + Handler +
    phase::Disconnected::Phase +
    phase::Connecting::Phase<<H as Handler>::State> +
    phase::Hosting::Phase<<H as Handler>::State> +
    phase::Peering::Phase<<H as Handler>::State> +
    phase::Started::Phase<<H as Handler>::State> +
    phase::Starting::Phase<<H as Handler>::State> +
    phase::ConnectionLost::Phase +
    phase::Disconnecting::Phase +
    phase::SimulatingOnline::Phase<<H as Handler>::State> +
    phase::SimulatingOffline::Phase<<H as Handler>::State> +
    phase::PausedOffline::Phase<<H as Handler>::State> +
    phase::Synchronizing::Phase +
    phase::SynchronisationLost::Phase, H::Socket: TestHandlerSocket
{
    client.socket.as_mut().unwrap().set_received(ws_advertise(local, id, index, seed, 42));
    client.tick(now);
}

fn client_peers(client: &mut Client<TestHandler>) -> Vec<PeerID> {
    client.peers.iter().map(|c| c.id().clone()).collect()
}

fn phase_connecting() -> ClientPhase<TestState> {
    ClientPhase::Connecting(phase::Connecting::State {
        host: "localhost".to_string(),
        id: "session-test".to_string(),
        state_hash: 308180007927971785,
        state_config: TestStateConfig::default()
    })
}

fn phase_hosting() -> ClientPhase<TestState> {
    ClientPhase::Hosting(phase::Hosting::State {
        host: "localhost".to_string(),
        id: "session-test".to_string(),
        state_hash: 308180007927971785,
        state_config: TestStateConfig::default()
    })
}

fn phase_hosting_config(state_hash: u64, state_config: TestStateConfig) -> ClientPhase<TestConfigState> {
    ClientPhase::Hosting(phase::Hosting::State {
        host: "localhost".to_string(),
        id: "session-test".to_string(),
        state_hash,
        state_config
    })
}

fn phase_peering() -> ClientPhase<TestState> {
    ClientPhase::Peering(phase::Peering::State {
        host: "localhost".to_string(),
        id: "session-test".to_string(),
        state_hash: 308180007927971785,
        state_config: TestStateConfig::default()
    })
}

fn phase_peering_config(state_hash: u64, state_config: TestStateConfig) -> ClientPhase<TestConfigState> {
    ClientPhase::Peering(phase::Peering::State {
        host: "localhost".to_string(),
        id: "session-test".to_string(),
        state_hash,
        state_config
    })
}

fn phase_started() -> ClientPhase<TestState> {
    ClientPhase::Started(phase::Started::State {
        host: "localhost".to_string(),
        id: "session-test".to_string(),
        state_hash: 308180007927971785,
        state_config: TestStateConfig::default()
    })
}

fn phase_started_config(state_hash: u64, state_config: TestStateConfig) -> ClientPhase<TestConfigState> {
    ClientPhase::Started(phase::Started::State {
        host: "localhost".to_string(),
        id: "session-test".to_string(),
        state_hash,
        state_config
    })
}

fn phase_starting() -> ClientPhase<TestState> {
    ClientPhase::Starting(phase::Starting::State {
        host: "localhost".to_string(),
        id: "session-test".to_string(),
        state_hash: 308180007927971785,
        state_config: TestStateConfig::default()
    })
}

fn phase_simulation(frame: (u32, u32), frame_advantage: f32) -> ClientPhase<TestState> {
    ClientPhase::SimulatingOnline(phase::SimulatingOnline::State {
        frame,
        frame_advantage,
        frame_deviation: 0.0,
        frame_speed: 1.0,
        state: TestState {
            id: frame.1,
            v: 0
        },
        peers_slow: HashSet::new()
    })
}

fn phase_simulation_offline(frame: (u32, u32)) -> ClientPhase<TestState> {
    ClientPhase::SimulatingOffline(phase::SimulatingOffline::State {
        frame,
        state: TestState {
            id: frame.1,
            v: 0
        }
    })
}

fn phase_paused_offline(frame: (u32, u32)) -> ClientPhase<TestState> {
    ClientPhase::PausedOffline(phase::PausedOffline::State {
        frame,
        state: TestState {
            id: frame.1,
            v: 0
        }
    })
}

fn phase_syncing(ready: Vec<u16>) -> ClientPhase<TestState> {
    ClientPhase::Synchronizing(phase::Synchronizing::State {
        peers_ready: {
            let mut s = HashSet::new();
            for t in ready {
                s.insert(PeerID(t));
            }
            s
        }
    })
}

fn ws_request(local: bool, from: u16, index: u32, seed: u32, request: SocketRequest) -> Option<Vec<String>> {
    Some(vec![SocketResponse {
        local,
        from: PeerID(from),
        index,
        seed,
        request

    }.to_string()])
}

fn ws_advertise(local: bool, from: u16, index: u32, seed: u32, version: u32) -> Option<Vec<String>> {
    ws_request(local, from, index, seed, SocketRequest {
        to: None,
        peers: Vec::new(),
        version,
        payload: SocketRequestPayload::Advertise(SocketRequestConfig("{\"value\":0}".to_string()))
    })
}

fn ws_peers(from: u16, index: u32, peers: Vec<PeerID>) -> Option<Vec<String>> {
    ws_request(
        false,
        from,
        index,
        0,
        SocketRequest {
            to: Some(PeerID(0)),
            peers,
            version: 42,
            payload: SocketRequestPayload::PeeringOffer(vec![])
        }
    )
}

fn client_simulation() -> Client<TestHandler> {

    let mut client = client_create();
    client.handler.transitions.clear();
    client_connect_ws(&mut client);
    client_add_peer(&mut client, true, 1, 1, 48, 0.0);
    client_add_peer(&mut client, false, 0, 0, 47, 0.0);
    client_add_peer(&mut client, false, 2, 2, 50, 0.0);

    // Only host is connected yet
    client.peers[0].rtc.as_mut().unwrap().connected = true;

    // Connect the second peer
    client.peers[2].rtc.as_mut().unwrap().connected = true;

    // Receive starting from host again
    receive(&mut client.peers[0], vec![
        (2, 0, vec![PeerMessage::Starting(vec![PeerID(0), PeerID(1), PeerID(2)])])
    ]);
    client.tick(2.0);

    // Receive starting from second peer
    receive(&mut client.peers[2], vec![
        (1, 0, vec![PeerMessage::Starting(vec![])])
    ]);
    client.tick(3.0);

    assert_eq!(client.phase, phase_simulation((0, 0), 0.0), "Should start simulation once all peers are starting");

    client

}

fn client_input_simulation() -> Client<TestInputHandler> {

    let handler = TestInputHandler::new(Config {
        sync_interval: 2,
        hash_interval: 2,
        .. Config::default()
    });
    let mut client = Client::new(handler, 0.0);
    client.handler.transitions.clear();

    client.handler.command.disconnected = Some(phase::Disconnected::Command::Connect {
        host: "localhost".to_string(),
        session_id: "session-test".to_string()
    });
    client.tick(0.0);
    client.tick(0.0);

    client_add_input_peer(&mut client, true, 1, 1, 48, 0.0);
    client_add_input_peer(&mut client, false, 0, 0, 47, 0.0);
    client_add_input_peer(&mut client, false, 2, 2, 50, 0.0);

    // Only host is connected yet
    client.peers[0].rtc.as_mut().unwrap().connected = true;

    // Connect the second peer
    client.peers[2].rtc.as_mut().unwrap().connected = true;

    // Receive starting from host again
    receive(&mut client.peers[0], vec![
        (2, 0, vec![PeerMessage::Starting(vec![PeerID(0), PeerID(1), PeerID(2)])])
    ]);
    client.tick(2.0);

    // Receive starting from second peer
    receive(&mut client.peers[2], vec![
        (1, 0, vec![PeerMessage::Starting(vec![])])
    ]);
    client.tick(3.0);

    client

}

fn client_add_input_peer(client: &mut Client<TestInputHandler>, local: bool, id: u16, index: u32, seed: u32, now: f64) {
    client.socket.as_mut().unwrap().received = ws_advertise(local, id, index, seed, 42);
    client.tick(now);
}

fn receive<H: Handler>(peer: &mut Peer<H>, messages: Vec<(u8, u8, Vec<PeerMessage<TestInput>>)>) where H::Connection: PacketConnection {
    peer.rtc.as_mut().unwrap().set_received(messages.into_iter().map(|msg| {
        let mut bytes = vec![msg.0, msg.1];
        for m in msg.2 {
            bytes.append(&mut m.into_bytes());
        }
        bytes

    }).collect());
}

fn sent<H: Handler>(peer: &Peer<H>) -> Vec<(u8, u8, Vec<PeerMessage<TestInput>>)> where H::Connection: PacketConnection {
    peer.rtc.as_ref().unwrap().sent().iter().map(|packet| {
        (packet[0], packet[1], PeerMessage::all_from_bytes(&packet[2..]))

    }).collect()
}

fn clear_sent<H: Handler>(peer: &mut Peer<H>) where H::Connection: PacketConnection {
    peer.rtc.as_mut().unwrap().clear_sent();
}

