// Internal Dependencies ------------------------------------------------------
use super::{client_create, client_connect_ws, client_add_peer, phase_simulation, phase_hosting, phase_peering, ws_peers, sent, clear_sent, ClientPhase, phase_connecting, phase_starting, phase_started};
use crate::PeerID;
use crate::phase;
use crate::peer::PeerMessage;
use crate::socket::{SocketRequest, SocketRequestConfig, SocketRequestPayload};
use crate::test::StateTransition;


// Tests ----------------------------------------------------------------------
#[test]
fn test_handler_start_host() {
    let mut client = client_create();
    client_connect_ws(&mut client);
    client_add_peer(&mut client, true, 0, 0, 48, 0.0);
    client_add_peer(&mut client, false, 1, 1, 47, 0.0);
    client_add_peer(&mut client, false, 2, 2, 46, 0.0);
    client.peers[1].rtc.as_mut().unwrap().connected = true;
    client.peers[2].rtc.as_mut().unwrap().connected = true;

    // Receive advertise from peer 1 and 2
    client.socket.as_mut().unwrap().received = ws_peers(1, 1, vec![PeerID(0), PeerID(2)]);
    client.tick(1.0);
    client.socket.as_mut().unwrap().received = ws_peers(2, 2, vec![PeerID(0), PeerID(1)]);

    // Issue starting command
    client.handler.command.hosting = Some(phase::Hosting::Command::Start);
    client.tick(1.0);

    assert_eq!(client.phase, phase_started());
    assert_eq!(client.handler.transitions, vec![
        StateTransition::Leave(ClientPhase::Disconnected, 0.0),
        StateTransition::Enter(phase_connecting(), 0.0),
        StateTransition::Leave(phase_connecting(), 0.0),
        StateTransition::Enter(phase_hosting(), 0.0),
        StateTransition::Leave(phase_hosting(), 1.0),
        StateTransition::Enter(phase_started(), 1.0)
    ]);
    assert_eq!(client.peers.len(), 3, "Should not drop any peers that are ready and connected");
    client.tick(2.0);

    // Packets
    assert_eq!(sent(&client.peers[1]), vec![
        (0, 0, vec![]),
        (1, 0, vec![]),
        (2, 0, vec![PeerMessage::Starting(vec![PeerID(0), PeerID(1), PeerID(2)])])

    ], "Should send starting message along with the peers connected, in case local client is the host");

    // Advertise
    client.tick(102.0);
    client.tick(202.0);
    assert_eq!(client.socket.as_ref().unwrap().requests, vec![
        SocketRequest {
            to: None,
            peers: vec![PeerID(1), PeerID(2)],
            version: 42,
            payload: SocketRequestPayload::Advertise(SocketRequestConfig("{\"value\":0}".to_string()))
        },
        SocketRequest {
            to: None,
            peers: vec![PeerID(1), PeerID(2)],
            version: 42,
            payload: SocketRequestPayload::Advertise(SocketRequestConfig("{\"value\":0}".to_string()))
        }

    ], "Should advertise every 100ms when starting.");

    // Test disconnect
    client.handler.command.started = Some(phase::Started::Command::Disconnect);
    client.tick(102.0);
    assert_eq!(client.phase, ClientPhase::Disconnected);

}

#[test]
fn test_handler_start_host_drop_non_connected_non_ready() {
    let mut client = client_create();
    client_connect_ws(&mut client);
    client_add_peer(&mut client, true, 0, 0, 48, 0.0);
    client_add_peer(&mut client, false, 1, 1, 47, 0.0);
    client_add_peer(&mut client, false, 2, 2, 46, 0.0);
    client_add_peer(&mut client, false, 3, 3, 45, 0.0);
    client.peers[1].rtc.as_mut().unwrap().connected = false;
    client.peers[2].rtc.as_mut().unwrap().connected = true;
    client.peers[3].rtc.as_mut().unwrap().connected = true;

    // Receive advertise from peer 1 and 2
    client.socket.as_mut().unwrap().received = ws_peers(1, 1, vec![PeerID(0), PeerID(2)]);
    client.tick(1.0);
    client.socket.as_mut().unwrap().received = ws_peers(3, 3, vec![PeerID(0), PeerID(2)]);

    // Issue starting command
    client.handler.command.hosting = Some(phase::Hosting::Command::Start);
    client.tick(1.0);

    assert_eq!(client.phase, phase_started());
    assert_eq!(client.handler.transitions, vec![
        StateTransition::Leave(ClientPhase::Disconnected, 0.0),
        StateTransition::Enter(phase_connecting(), 0.0),
        StateTransition::Leave(phase_connecting(), 0.0),
        StateTransition::Enter(phase_hosting(), 0.0),
        StateTransition::Leave(phase_hosting(), 1.0),
        StateTransition::Enter(phase_started(), 1.0)
    ]);
    assert_eq!(client.peers.len(), 2, "Should drop any peers that not ready or connected");
    client.tick(2.0);

    // Packets
    assert_eq!(sent(&client.peers[1]), vec![
        (0, 0, vec![]),
        (1, 0, vec![]),
        (2, 0, vec![PeerMessage::Starting(vec![PeerID(0), PeerID(3)])])

    ], "Should send starting message along with the peers connected, in case local client is the host");
}


#[test]
fn test_handler_starting_socket_lost() {
    let mut client = client_create();
    client_connect_ws(&mut client);
    client_add_peer(&mut client, true, 0, 0, 48, 0.0);
    client_add_peer(&mut client, false, 1, 1, 47, 0.0);

    client.peers[1].rtc.as_mut().unwrap().connected = true;
    client.socket.as_mut().unwrap().received = ws_peers(1, 1, vec![PeerID(0)]);

    // Issue starting command
    client.handler.command.hosting = Some(phase::Hosting::Command::Start);
    client.tick(1.0);

    assert_eq!(client.phase, phase_started());
    client.socket.as_mut().unwrap().connected = false;
    client.tick(2.0);
    assert_eq!(client.phase, ClientPhase::ConnectionLost);
    assert_eq!(client.peers.len(), 0, "Should remove all peers when web socket connection is lost");

}

#[test]
fn test_starting_phase_peer_timeout() {
    let mut client = client_create();
    client_connect_ws(&mut client);
    client_add_peer(&mut client, true, 0, 0, 48, 0.0);
    client_add_peer(&mut client, false, 1, 1, 47, 0.0);
    client_add_peer(&mut client, false, 2, 2, 44, 0.0);
    client.peers[1].rtc.as_mut().unwrap().connected = true;
    client.peers[2].rtc.as_mut().unwrap().connected = true;

    client.socket.as_mut().unwrap().received = ws_peers(1, 1, vec![PeerID(0), PeerID(2)]);
    client.tick(1.0);
    client.socket.as_mut().unwrap().received = ws_peers(2, 2, vec![PeerID(0), PeerID(1)]);

    // Issue starting command
    client.handler.command.hosting = Some(phase::Hosting::Command::Start);
    client.tick(1.0);
    assert_eq!(client.phase, phase_started());

    // Receive packet to keep peer-2 alive
    client.peers[2].rtc.as_mut().unwrap().packets_received = vec![
        vec![2, 0]
    ];
    client.tick(1001.0);

    assert_eq!(client.peers.len(), 3);
    client.tick(2001.0);
    assert_eq!(client.peers.len(), 2, "Should handle peer timeouts during starting phase");
    assert_eq!(client.phase, phase_started());

}

#[test]
fn test_handler_start_peer_ignore() {
    let mut client = client_create();
    client_connect_ws(&mut client);
    client_add_peer(&mut client, true, 0, 1, 48, 0.0);
    client_add_peer(&mut client, false, 1, 0, 47, 0.0);
    client.peers[0].rtc.as_mut().unwrap().connected = true;

    // Issue starting command
    client.handler.command.hosting = Some(phase::Hosting::Command::Start);
    client.tick(0.0);

    assert_eq!(client.phase, phase_peering(), "Should ignore starting command from handler if not host");
}

#[test]
fn test_receive_start_peer_ignore() {
    let mut client = client_create();
    client_connect_ws(&mut client);
    client_add_peer(&mut client, true, 1, 1, 48, 0.0);
    client_add_peer(&mut client, false, 0, 0, 47, 1.0);
    client_add_peer(&mut client, false, 2, 2, 50, 1.5);
    client.peers[0].rtc.as_mut().unwrap().connected = true;
    // Peer 1 is local
    client.peers[2].rtc.as_mut().unwrap().connected = true;

    // Receive a starting command from the host
    client.peers[2].rtc.as_mut().unwrap().packets_received = vec![
        vec![1, 0, 0, 0]
    ];
    client.tick(1.8);
    assert_eq!(client.phase, phase_peering(), "Should transition into the starting phase once the host sends a starting message");
    assert_eq!(client.handler.transitions, vec![
        StateTransition::Leave(ClientPhase::Disconnected, 0.0),
        StateTransition::Enter(phase_connecting(), 0.0),
        StateTransition::Leave(phase_connecting(), 0.0),
        StateTransition::Enter(phase_hosting(), 0.0),
        StateTransition::Leave(phase_hosting(), 1.0),
        StateTransition::Enter(phase_peering(), 1.0)
    ]);
}

#[test]
fn test_receive_start_host() {
    let mut client = client_create();
    client_connect_ws(&mut client);
    client_add_peer(&mut client, true, 1, 1, 48, 0.0);
    client_add_peer(&mut client, false, 0, 0, 47, 0.0);
    client_add_peer(&mut client, false, 2, 2, 50, 0.0);

    // Only host is connected yet
    client.peers[0].rtc.as_mut().unwrap().connected = true;

    // Connect the second peer
    client.peers[2].rtc.as_mut().unwrap().connected = true;

    // Receive starting from host again
    client.peers[0].rtc.as_mut().unwrap().packets_received = vec![
        vec![
            2, 0,
            0, 3,  // Starting with 3 peers
            0, 0,
            0, 1,
            0, 2
        ]
    ];
    client.tick(2.0);
    assert_eq!(client.phase, phase_starting(), "Should transition into the starting phase once the host sends a starting message");
    assert_eq!(client.peers.len(), 3, "Should not have dropped any peers, since host has identical peer list");
    assert_eq!(client.peers[0].is_starting(), true, "Host should have been set to starting");
    assert_eq!(client.peers[0].id(), PeerID(0), "Should not have re-ordered any peers, since host has identical peer list");
    assert_eq!(client.peers[1].id(), PeerID(1), "Should not have re-ordered any peers, since host has identical peer list");
    assert_eq!(client.peers[2].id(), PeerID(2), "Should not have re-ordered any peers, since host has identical peer list");

    // Clear send packets
    clear_sent(&mut client.peers[0]);
    clear_sent(&mut client.peers[2]);

    // Check for start responses
    client.tick(0.0);
    assert_eq!(sent(&client.peers[0]), vec![
        (1, 2, vec![PeerMessage::Starting(vec![])])

    ], "Should send starting message once in starting phase");

    assert_eq!(sent(&client.peers[2]), vec![
        (1, 0, vec![PeerMessage::Starting(vec![])])

    ], "Should send starting message once in starting phase");

    // Receive starting from second peer
    client.peers[2].rtc.as_mut().unwrap().packets_received = vec![
        vec![1, 0, 0, 0]
    ];
    client.tick(3.0);

    assert_eq!(client.phase, phase_simulation((0, 0), 0.0), "Should start simulation once all peers are starting");

    assert_eq!(client.handler.transitions, vec![
        StateTransition::Leave(ClientPhase::Disconnected, 0.0),
        StateTransition::Enter(phase_connecting(), 0.0),
        StateTransition::Leave(phase_connecting(), 0.0),
        StateTransition::Enter(phase_hosting(), 0.0),
        StateTransition::Leave(phase_hosting(), 0.0),
        StateTransition::Enter(phase_peering(), 0.0),
        StateTransition::Leave(phase_peering(), 2.0),
        StateTransition::Enter(phase_starting(), 2.0),
        StateTransition::Leave(phase_starting(), 3.0),
        StateTransition::Enter(phase_simulation((0, 0), 0.0), 3.0)
    ]);

    assert!(client.socket.is_none(), "Should close the web socket connection once the simulation has started");
    assert_eq!(client.handler.peer_disconnects, vec![], "Should disconnect no peers");

}

#[test]
fn test_receive_start_host_sort() {
    let mut client = client_create();
    client_connect_ws(&mut client);
    client_add_peer(&mut client, true, 1, 1, 48, 0.0);
    client_add_peer(&mut client, false, 0, 0, 47, 0.0);
    client_add_peer(&mut client, false, 2, 2, 50, 0.0);

    // Only host is connected yet
    client.peers[0].rtc.as_mut().unwrap().connected = true;

    // Receive starting from host again
    client.peers[0].rtc.as_mut().unwrap().packets_received = vec![
        vec![
            2, 0,
            0, 3, // Starting with 3 peers
            0, 2,
            0, 0,
            0, 1
        ]
    ];

    client.tick(2.0);
    assert_eq!(client.phase, phase_starting(), "Should transition into the starting phase once the host sends a starting message");
    assert_eq!(client.peers.len(), 3, "Should not have dropped any peers, since host has the same peers");
    assert_eq!(client.peers[0].id(), PeerID(2), "Should have re-ordered peers, since host has different order");
    assert_eq!(client.peers[1].id(), PeerID(0), "Should have re-ordered peers, since host has different order");
    assert_eq!(client.peers[1].is_starting(), true, "Host should have been set to starting");
    assert_eq!(client.peers[2].id(), PeerID(1), "Should have re-ordered peers, since host has different order");
    assert_eq!(client.handler.peer_disconnects, vec![], "Should disconnect no peers");

    // Test disconnect
    client.handler.command.starting = Some(phase::Starting::Command::Disconnect);
    client.tick(2.0);
    assert_eq!(client.phase, ClientPhase::Disconnected);

}

#[test]
fn test_receive_start_host_drop() {
    let mut client = client_create();
    client_connect_ws(&mut client);
    client_add_peer(&mut client, true, 1, 1, 48, 0.0);
    client_add_peer(&mut client, false, 0, 0, 47, 0.0);
    client_add_peer(&mut client, false, 2, 2, 50, 0.0);
    client_add_peer(&mut client, false, 3, 3, 51, 0.0);
    client_add_peer(&mut client, false, 4, 4, 51, 0.0);

    // Only host is connected yet
    client.peers[0].rtc.as_mut().unwrap().connected = true;

    // Receive starting from host again
    client.peers[0].rtc.as_mut().unwrap().packets_received = vec![
        vec![
            2, 0,
            0, 3, // Starting with 3 peers
            0, 0, // Host
            0, 4, // Other
            0, 1  // Local
        ]
    ];

    client.tick(2.0);
    assert_eq!(client.phase, phase_starting(), "Should transition into the starting phase once the host sends a starting message");
    assert_eq!(client.peers.len(), 3, "Should have dropped peers, since host has different peers");
    assert_eq!(client.peers[0].id(), PeerID(0), "Should have re-ordered peers, since host has different order");
    assert_eq!(client.peers[0].is_starting(), true, "Host should have been set to starting");
    assert_eq!(client.peers[1].id(), PeerID(4), "Should have re-ordered peers, since host has different order");
    assert_eq!(client.peers[2].id(), PeerID(1), "Should have re-ordered peers, since host has different order");
    assert_eq!(client.handler.peer_disconnects, vec![(PeerID(2), 2.0), (PeerID(3), 2.0)], "Should disconnect peers");

}

#[test]
fn test_receive_start_host_drop_local() {
    let mut client = client_create();
    client_connect_ws(&mut client);
    client_add_peer(&mut client, true, 1, 1, 48, 0.0);
    client_add_peer(&mut client, false, 0, 0, 47, 0.0);
    client_add_peer(&mut client, false, 2, 2, 50, 0.0);
    client_add_peer(&mut client, false, 3, 3, 51, 0.0);
    client_add_peer(&mut client, false, 4, 4, 51, 0.0);

    // Only host is connected yet
    client.peers[0].rtc.as_mut().unwrap().connected = true;

    // Receive starting from host again
    client.peers[0].rtc.as_mut().unwrap().packets_received = vec![
        vec![
            2, 0,
            0, 2, // Starting with 2 peers
            0, 0, // Host
            0, 4  // Othe
        ]
    ];

    client.tick(2.0);
    assert_eq!(client.phase, phase_peering(), "Should not transition into the starting phase when host does not start with local peer");
    assert_eq!(client.peers.len(), 5, "Should not drop any peers");
    assert_eq!(client.handler.peer_disconnects, vec![], "Should not disconnect any peers");

}

