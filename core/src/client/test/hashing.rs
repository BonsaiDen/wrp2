// Internal Dependencies ------------------------------------------------------
use super::{client_simulation, client_input_simulation, phase_simulation, sent, clear_sent};
use crate::PeerID;
use crate::phase::{self, SyncError};
use crate::test::{TestInput, StateTransition};
use crate::peer::PeerMessage;


// Tests ----------------------------------------------------------------------
#[test]
fn test_hashing() {

    let mut client = client_input_simulation();

    // Receive inputs from other peers
    client.peers[0].rtc.as_mut().unwrap().packets_received = vec![vec![
        3, 0,
        1, 0, 0, 0,  0, 0,   1,  128,
        1, 0, 0, 1,  0, 0,   1,  128,
        1, 0, 0, 2,  0, 0,   1,  128,
        1, 0, 0, 3,  0, 0,   1,  128,
        1, 0, 0, 4,  0, 0,   1,  128,
        1, 0, 0, 5,  0, 0,   1,  128,
        1, 0, 0, 6,  0, 0,   1,  128
    ]];
    client.peers[2].rtc.as_mut().unwrap().packets_received = vec![vec![
        3, 0,
        1, 0, 0, 0,  0, 0,   1,  128,
        1, 0, 0, 1,  0, 0,   1,  128,
        1, 0, 0, 2,  0, 0,   1,  128,
        1, 0, 0, 3,  0, 0,   1,  128,
        1, 0, 0, 4,  0, 0,   1,  128,
        1, 0, 0, 5,  0, 0,   1,  128,
        1, 0, 0, 6,  0, 0,   1,  128
    ]];

    client.tick(36.0);
    assert_eq!(client.local_hashes.len(), 1, "Should have an initial hash");
    client.tick(69.0);
    assert_eq!(client.local_hashes.len(), 1);
    client.tick(102.0);
    assert_eq!(client.local_hashes.len(), 2, "Should create a new hash every 2 frames");
    client.tick(135.0);
    assert_eq!(client.local_hashes.len(), 2);
    client.tick(168.0);
    assert_eq!(client.local_hashes.len(), 3, "Should create a new hash every 2 frames");

    assert_eq!(sent(&client.peers[0]), vec![
        (0, 2, vec![]),
        (1, 2, vec![PeerMessage::Starting(vec![])]),
        (2, 3, vec![PeerMessage::Input { requested_frame: 2, inputs: vec![] }]),
        (3, 3, vec![PeerMessage::Input { requested_frame: 3, inputs: vec![] }]),
        (4, 3, vec![PeerMessage::Input { requested_frame: 4, inputs: vec![] }]),
        (5, 3, vec![PeerMessage::Input { requested_frame: 5, inputs: vec![] }, PeerMessage::Hash { requested_frame: 0, hashes: vec![(0, 0), (2, 0)] }]),
        (6, 3, vec![PeerMessage::Input { requested_frame: 6, inputs: vec![] }])

    ], "Should send hash requests to other peers every 4 frames");

    // Receive hashes for frame 0 from peer 0
    client.peers[0].rtc.as_mut().unwrap().packets_received = vec![vec![
        4, 0,
        // Hash
        3,
        0, 0, 0, // Requested frame
        1,  // 1 hash in payload
        0, 0, 0,  // For Frame 0
        0, 0, 0, 0, 0, 0, 0, 0  // Hash Value
    ]];
    client.tick(168.0);
    assert_eq!(client.local_hashes.len(), 3, "Should not yet drop the hash for frame 0 if only one peer has acknowledge it");

    // Receive hashes for frame 2 from peer 2
    client.peers[2].rtc.as_mut().unwrap().packets_received = vec![vec![
        4, 0,
        // Hash
        3,
        0, 0, 0, // Requested frame
        1,  // 1 hash in payload
        0, 0, 2,  // For Frame 9
        0, 0, 0, 0, 0, 0, 0, 0  // Hash Value
    ]];
    client.tick(168.0);
    assert_eq!(client.local_hashes.len(), 3, "Should not yet drop hash for frame 2 if only one peer has acknowledge it");

    // Receive hash for frame 0 from peer 2
    client.peers[2].rtc.as_mut().unwrap().packets_received = vec![vec![
        5, 0,
        // Hash
        3,
        0, 0, 0, // Requested frame
        1,  // 1 hash in payload
        0, 0, 0,  // For Frame 0
        0, 0, 0, 0, 0, 0, 0, 0  // Hash Value
    ]];
    client.tick(168.0);

    // Bump both peers minimum hash requested to 2 in order to drop local hash for frame #0
    client.peers[0].rtc.as_mut().unwrap().packets_received = vec![vec![
        6, 0,
        // Hash
        3,
        0, 0, 2, // Requested frame
        0,  // 1 hash in payload
    ]];
    client.peers[2].rtc.as_mut().unwrap().packets_received = vec![vec![
        6, 0,
        // Hash
        3,
        0, 0, 2, // Requested frame
        0,  // 1 hash in payload
    ]];
    client.tick(168.0);
    assert_eq!(client.local_hashes.len(), 2, "Should drop confirmed local hashes once all peers have acknowledged them.");

    // Bump both peers minimum hash requested to 4
    client.peers[0].rtc.as_mut().unwrap().packets_received = vec![vec![
        7, 0,
        // Hash
        3,
        0, 0, 4, // Requested frame
        0,  // 1 hash in payload
    ]];
    client.peers[2].rtc.as_mut().unwrap().packets_received = vec![vec![
        7, 0,
        // Hash
        3,
        0, 0, 4, // Requested frame
        0,  // 1 hash in payload
    ]];
    client.tick(168.0);
    assert_eq!(client.local_hashes.len(), 2, "Should not drop local hashes which are missing confirmations.");

    // Clear sent messages
    client.tick(201.0);
    clear_sent(&mut client.peers[0]);
    client.tick(234.0);

    assert_eq!(sent(&client.peers[0]), vec![
        (13, 7, vec![
            PeerMessage::Input { requested_frame: 7, inputs: vec![(6, TestInput(0)), (7, TestInput(0))] },
            PeerMessage::Hash { requested_frame: 2, hashes: vec![(4, 0), (6, 0)] }
        ])

    ], "Should send updated hash requests to other peers");

}

#[test]
fn test_hash_mismatch() {

    let mut client = client_input_simulation();

    // Receive inputs from other peers
    client.peers[0].rtc.as_mut().unwrap().packets_received = vec![vec![
        3, 0,
        1, 0, 0, 0,  0, 0,   1,  128,
        1, 0, 0, 1,  0, 0,   1,  128,
        1, 0, 0, 2,  0, 0,   1,  128,
        1, 0, 0, 3,  0, 0,   1,  128,
        1, 0, 0, 4,  0, 0,   1,  128,
        1, 0, 0, 5,  0, 0,   1,  128,
        1, 0, 0, 6,  0, 0,   1,  128
    ]];
    client.peers[2].rtc.as_mut().unwrap().packets_received = vec![vec![
        3, 0,
        1, 0, 0, 0,  0, 0,   1,  128,
        1, 0, 0, 1,  0, 0,   1,  128,
        1, 0, 0, 2,  0, 0,   1,  128,
        1, 0, 0, 3,  0, 0,   1,  128,
        1, 0, 0, 4,  0, 0,   1,  128,
        1, 0, 0, 5,  0, 0,   1,  128,
        1, 0, 0, 6,  0, 0,   1,  128
    ]];

    client.tick(36.0);
    client.tick(69.0);
    client.tick(102.0);

    // Receive hashes for frame 0 from peer 0
    client.peers[0].rtc.as_mut().unwrap().packets_received = vec![vec![
        4, 0,
        // Hash
        3,
        0, 0, 0, // Requested frame
        1,  // 1 hash in payload
        0, 0, 0,  // For Frame 0
        1, 1, 1, 1, 1, 1, 1, 1  // Hash Value
    ]];
    client.handler.transitions.clear();
    client.tick(135.0);

    let expected = vec![
        StateTransition::Enter(phase::SynchronisationLost::new(
            0,
            SyncError::HashMismatch(72340172838076673, PeerID(0))

        ), 135.0)
    ];
    assert_eq!(&client.handler.transitions[1..], &expected[..], "Should trigger de-sync after receiving non-matching hash for frame #0 from other peer");

    assert_eq!(client.phase, phase::SynchronisationLost::new(
        0,
        SyncError::HashMismatch(72340172838076673, PeerID(0))
    ));

    assert_eq!(client.local_hashes.len(), 2, "Should not clear local state upon de-sync");
    assert_eq!(client.peers.len(), 3, "Should not clear local state upon de-sync");

    assert_eq!(client.peers[0].is_connected(), false, "Should disconnect remote peers after de-sync");
    assert_eq!(client.peers[1].is_connected(), true, "Should not disconnect the local peer");
    assert_eq!(client.peers[2].is_connected(), false, "Should not disconnect remote peers after de-sync");

    // Reset command to get out of de-sync
    client.handler.command.synchronisation_lost = Some(phase::SynchronisationLost::Command::Reset);
    client.tick(135.0);

    assert_eq!(client.local_inputs.len(), 0);
    assert_eq!(client.resync_states.len(), 0);
    assert_eq!(client.local_hashes.len(), 0);
    assert_eq!(client.peers.len(), 0);

}

#[test]
fn test_hash_timeout() {

    let mut client = client_input_simulation();

    // Receive inputs from other peers
    client.peers[0].rtc.as_mut().unwrap().packets_received = vec![vec![
        3, 0,
        1, 0, 0, 0,  0, 0,   1,  128,
        1, 0, 0, 1,  0, 0,   1,  128,
        1, 0, 0, 2,  0, 0,   1,  128,
        1, 0, 0, 3,  0, 0,   1,  128,
        1, 0, 0, 4,  0, 0,   1,  128,
        1, 0, 0, 5,  0, 0,   1,  128,
        1, 0, 0, 6,  0, 0,   1,  128
    ]];
    client.peers[2].rtc.as_mut().unwrap().packets_received = vec![vec![
        3, 0,
        1, 0, 0, 0,  0, 0,   1,  128,
        1, 0, 0, 1,  0, 0,   1,  128,
        1, 0, 0, 2,  0, 0,   1,  128,
        1, 0, 0, 3,  0, 0,   1,  128,
        1, 0, 0, 4,  0, 0,   1,  128,
        1, 0, 0, 5,  0, 0,   1,  128,
        1, 0, 0, 6,  0, 0,   1,  128
    ]];

    client.tick(36.0);
    assert_eq!(client.local_hashes.len(), 1, "Should have an initial hash");
    client.tick(36.0);
    client.handler.transitions.clear();

    // Receive hash to trigger timeout check
    client.peers[0].rtc.as_mut().unwrap().packets_received = vec![vec![
        4, 0,
        // Hash
        3,
        0, 0, 0, // Requested frame
        1,  // 1 hash in payload
        0, 0, 0,  // For Frame 0
        0, 0, 0, 0, 0, 0, 0, 0  // Hash Value
    ]];

    client.tick(36.0 + 5000.0);

    let expected = vec![
        StateTransition::Enter(phase::SynchronisationLost::new(0, SyncError::HashTimeout(0)), 36.0 + 5000.0)
    ];
    assert_eq!(&client.handler.transitions[1..], &expected[..], "Should trigger de-sync after receiving non-matching hash for frame #0 from other peer");
    assert_eq!(client.phase, phase::SynchronisationLost::new(0, SyncError::HashTimeout(0)));

}


#[test]
fn test_hash_timeout_empty() {

    let mut client = client_simulation();

    // Receive inputs from other peers to create initial local hash
    client.peers[0].rtc.as_mut().unwrap().packets_received = vec![vec![
        3, 0,
        1, 0, 0, 0,  0, 0,   1,  0
    ]];
    client.peers[2].rtc.as_mut().unwrap().packets_received = vec![vec![
        3, 0,
        1, 0, 0, 0,  0, 0,   1,  0
    ]];

    client.tick(36.0);
    assert_eq!(client.local_hashes.len(), 1, "Should have an initial hash");
    client.handler.transitions.clear();

    // Receive hashes so the peers get removed from the hash states id set
    client.peers[0].rtc.as_mut().unwrap().packets_received = vec![vec![
        4, 0, 3, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    ]];
    client.tick(36.0);
    assert_eq!(client.local_hashes[0].3.len(), 1, "Should have removed the first id from the hash set");

    // Should ignore the receival of an identical peer hash
    client.peers[0].rtc.as_mut().unwrap().packets_received = vec![vec![
        5, 0, 3, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    ]];
    client.tick(36.0);
    assert_eq!(client.local_hashes[0].3.len(), 1, "Should already have remove the first id from the hash set");

    // Remove the second hash
    client.peers[2].rtc.as_mut().unwrap().packets_received = vec![vec![
        4, 0, 3, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    ]];
    client.tick(36.0);
    assert_eq!(client.local_hashes[0].3.len(), 0, "Should have removed the first id from the hash set");

    // Try to cause timeout for hash with empty id set
    client.peers[0].rtc.as_mut().unwrap().packets_received = vec![vec![
        6, 0, 3, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    ]];
    client.peers[2].rtc.as_mut().unwrap().packets_received = vec![vec![
        5, 0, 3, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    ]];
    client.tick(36.0 + 5000.0);

    assert_eq!(client.handler.transitions, vec![], "Should not trigger timeouts for empty local hahses");

    assert_eq!(client.phase, phase_simulation((0, 3), 0.5));

}

