// STD Dependencies -----------------------------------------------------------
use std::collections::HashSet;


// Internal Dependencies ------------------------------------------------------
use super::{phase_simulation, phase_syncing, client_simulation, client_input_simulation, sent, clear_sent, ClientPhase, phase_connecting, phase_hosting, phase_peering, phase_starting};
use crate::phase;
use crate::{Handler, PeerID};
use crate::test::{TestState, TestInputState, TestStateConfig, TestInput, StateTransition};
use crate::peer::PeerMessage;


// Tests ----------------------------------------------------------------------
#[test]
fn test_simulation_disconnect() {

    let mut client = client_simulation();

    // Issue disconnect
    client.handler.command.simulating_online = Some(phase::SimulatingOnline::Command::Disconnect);
    client.tick(4.0);

    assert_eq!(client.phase, ClientPhase::Disconnecting, "Should enter disconnecting phase after handler command");
    clear_sent(&mut client.peers[0]);
    clear_sent(&mut client.peers[2]);

    client.tick(1000.0);
    assert_eq!(client.phase, ClientPhase::Disconnecting, "Should stay in disconnecting phase for 2000ms");

    client.tick(2003.0);
    assert_eq!(client.phase, ClientPhase::Disconnecting, "Should stay in disconnecting phase for 2000ms");

    // Check for sending of Disconnecting messages
    assert_eq!(sent(&client.peers[0]), vec![
        (3, 2, vec![PeerMessage::Disconnecting]),
        (4, 2, vec![PeerMessage::Disconnecting])

    ], "Should send disconnecting message once in disconnecting phase");

    assert_eq!(sent(&client.peers[2]), vec![
        (2, 1, vec![PeerMessage::Disconnecting]),
        (3, 1, vec![PeerMessage::Disconnecting])

    ], "Should send disconnecting message once in disconnecting phase");

    client.tick(2004.0);
    assert_eq!(client.phase, ClientPhase::Disconnected, "Should leave the disconnecting phase after 2000ms");

    assert_eq!(client.peers.len(), 0);
    assert_eq!(client.handler.peer_disconnects, vec![
        (PeerID(0), 2004.0),
        (PeerID(1), 2004.0),
        (PeerID(2), 2004.0)

    ], "Should disconnect all peers");

}

#[test]
fn test_simulation_slow_peers() {
    let mut client = client_simulation();
    assert_eq!(client.phase, phase_simulation((0, 0), 0.0), "Should not list slow peers before 1000ms of receival delay");
    client.tick(999.0);
    assert_eq!(client.phase, phase_simulation((0, 2), 1.0), "Should not list slow peers before 1000ms of receival delay");
    client.tick(1002.0);

    let mut slow = HashSet::new();
    slow.insert(PeerID(0));

    let s = ClientPhase::SimulatingOnline(phase::SimulatingOnline::State {
        frame: (0, 2),
        frame_advantage: 1.0,
        frame_deviation: 0.0,
        frame_speed: 1.0,
        state: TestState {
            id: 2,
            v: 0
        },
        peers_slow: slow.clone()
    });
    assert_eq!(client.phase, s, "Should list peers as slow after 1000ms of no packet receival during simulation");

    client.tick(1003.0);
    slow.insert(PeerID(2));

    let s = ClientPhase::SimulatingOnline(phase::SimulatingOnline::State {
        frame: (0, 2),
        frame_advantage: 1.0,
        frame_deviation: 0.0,
        frame_speed: 1.0,
        state: TestState {
            id: 2,
            v: 0
        },
        peers_slow: slow
    });
    assert_eq!(client.phase, s, "Should list peers as slow after 1000ms of no packet receival during simulation");

}

#[test]
fn test_simulation_peer_timeout() {

    let mut client = client_simulation();
    client.tick(10003.0);
    assert_eq!(client.peers[0].is_connected(), false, "Should timeout host after 10000ms of no packet receival");
    assert_eq!(client.peers[1].is_connected(), true, "Should never timeout local peer");
    assert_eq!(client.peers[2].is_connected(), true, "Should not timeout other peer after 9000ms of no packet receival");
    assert_eq!(client.peers.len(), 3, "Should not remove disconnected peers in simulation phase");

    assert_eq!(client.phase, phase_syncing(vec![]), "Should transition to syncing phase");

    // Receive another packet from peer 2 to keep alive
    client.peers[2].rtc.as_mut().unwrap().packets_received = vec![
        vec![2, 0]
    ];
    client.tick(10004.0);
    assert_eq!(client.peers[2].is_connected(), true, "Should reset the peer timeout after receival of a packet");

    client.tick(15004.0);
    assert_eq!(client.peers[2].is_connected(), true, "Should not timeout other peer before 5000ms have passed during syncing phase");

    client.tick(15005.0);
    assert_eq!(client.resync_states.len(), 1);
    assert_eq!(client.peers[2].is_connected(), false, "Should timeout host after 5000ms of no packet receival during syncing phase");

    assert_eq!(client.phase, phase_simulation((0, 0), 0.0), "Should immediately resume to simulation phase when all other peers timeout during syncing phase");

    assert_eq!(client.handler.syncs, vec![
        (0, TestState { id: 0, v: 0 }, 3.0),
        (0, TestState { id: 0, v: 0 }, 15005.0)
    ]);

    assert_eq!(client.handler.transitions, vec![
        StateTransition::Leave(ClientPhase::Disconnected, 0.0),
        StateTransition::Enter(phase_connecting(), 0.0),
        StateTransition::Leave(phase_connecting(), 0.0),
        StateTransition::Enter(phase_hosting(), 0.0),
        StateTransition::Leave(phase_hosting(), 0.0),
        StateTransition::Enter(phase_peering(), 0.0),
        StateTransition::Leave(phase_peering(), 2.0),
        StateTransition::Enter(phase_starting(), 2.0),
        StateTransition::Leave(phase_starting(), 3.0),
        StateTransition::Enter(phase_simulation((0, 0), 0.0), 3.0),
        StateTransition::Leave(phase_simulation((0, 0), 0.0), 10003.0),
        StateTransition::Enter(phase_syncing(vec![]), 10003.0),
        StateTransition::Leave(phase_syncing(vec![1]), 15005.0),
        StateTransition::Enter(phase_simulation((0, 0), 0.0), 15005.0)
    ]);

    assert_eq!(client.handler.peer_disconnects, vec![
        (PeerID(0), 10003.0),
        (PeerID(2), 15005.0)
    ]);

}

#[test]
fn test_simulation_peer_disconnecting() {

    let mut client = client_simulation();

    // Receive disconnecting packet
    client.peers[0].rtc.as_mut().unwrap().packets_received = vec![
        vec![3, 0, 5]
    ];

    // Receive and timeout peer on this tick
    client.tick(1.0);

    // Actually disconnect peer on this tick due to timed out delay
    client.tick(1.0);

    assert_eq!(client.peers[0].is_connected(), false, "Should disconnect host after receiving Disconnecting message from them");
    assert_eq!(client.peers[1].is_connected(), true, "Should never disconnect local peer");
    assert_eq!(client.peers[2].is_connected(), true, "Should not disconnect other peer");
    assert_eq!(client.peers.len(), 3, "Should not remove disconnected peers in simulation phase");

    assert_eq!(client.phase, phase_syncing(vec![]), "Should transition to syncing phase");
    client.tick(1.0);
    assert_eq!(client.phase, phase_syncing(vec![1]), "Local peer should always directly be ready to sync");

}

#[test]
fn test_simulation_tick_negative() {

    let mut client = client_simulation();

    // Previous tick was at 3.0
    client.tick(0.0);
    assert_eq!(client.phase, phase_simulation((0, 1), 0.5), "Should guard against negative tick durations");

}

#[test]
fn test_simulation_ticking() {

    let mut client = client_simulation();

    assert_eq!(client.resync_states.len(), 1);
    assert_eq!(client.handler.syncs, vec![
        (0, TestState { id: 0, v: 0 }, 3.0)
    ]);

    clear_sent(&mut client.peers[0]);
    clear_sent(&mut client.peers[2]);

    // We start at 3 seconds and 1 frame
    for i in 0..30 {
        client.peers[0].rtc.as_mut().unwrap().packets_received = vec![vec![i + 5, 0,  1,  0, 0, i,  0, 0,  3, 0, 0, 0]];
        client.peers[2].rtc.as_mut().unwrap().packets_received = vec![vec![i + 5, 0,  1,  0, 0, i,  0, 0,  3, 0, 0, 0]];
        client.tick(3.0 + i as f64 * 33.333333); // Milliseconds
    }

    // Due to tick duration averaging this should come out to 37 handler tick calls - even though
    // the state was only advanced 30 times
    assert_eq!(client.handler.ticks.len(), 37, "Should have called handler.tick() on iteration regardless of the elapsed time between ticks");

    if let ClientPhase::SimulatingOnline(phase) = client.phase {
        assert_eq!(phase.frame.0, 28, "Should have committed 28 frames");
        assert_eq!(phase.frame.1, 30, "Should have simulated 30 frames");
        assert_eq!(phase.state.id, 30, "Should have simulated 30 states");
        assert_eq!(client.local_inputs.len(), 2, "Should have accumulated 2 local pending inputs not confirmed by the other peers");
        assert_eq!(client.local_hashes.len(), 1, "Should have created one local state hash for frame #0");
        assert_eq!(client.resync_states.len(), 2, "Should have two resync states");
        assert_eq!(client.resync_states[0].state, client.handler.state(TestStateConfig::default(), &client.peers), "Should have a copy of the base state as the first resync state");
        assert_eq!(client.resync_states[0].frame, 0);
        assert_eq!(client.resync_states[0].frame_elapsed_time, 0.0);
        assert_eq!(client.resync_states[0].tick_total, 0);

        assert_eq!(client.resync_states[1].state, client.handler.state(TestStateConfig::default(), &client.peers), "Should have a copy of the first committed state as the second resync state");
        assert_eq!(client.resync_states[1].frame, 0);
        assert_eq!(client.resync_states[1].frame_elapsed_time, 33.333333);
        assert_eq!(client.resync_states[1].tick_total, 2);

        assert_eq!(sent(&client.peers[0]), vec![
            ( 2,  5, vec![PeerMessage::Input { requested_frame:  1, inputs: vec![( 0, TestInput(0))] }]),
            ( 3,  6, vec![PeerMessage::Input { requested_frame:  2, inputs: vec![( 1, TestInput(0))] }]),
            ( 4,  7, vec![PeerMessage::Input { requested_frame:  3, inputs: vec![( 2, TestInput(0))] }]),
            ( 5,  8, vec![PeerMessage::Input { requested_frame:  4, inputs: vec![( 3, TestInput(0))] }, PeerMessage::Hash { requested_frame: 0, hashes: vec![(0, 0)] }]),
            ( 6,  9, vec![PeerMessage::Input { requested_frame:  5, inputs: vec![( 4, TestInput(0))] }]),
            ( 7, 10, vec![PeerMessage::Input { requested_frame:  6, inputs: vec![( 5, TestInput(0))] }]),
            ( 8, 11, vec![PeerMessage::Input { requested_frame:  7, inputs: vec![( 6, TestInput(0))] }]),
            ( 9, 12, vec![PeerMessage::Input { requested_frame:  8, inputs: vec![( 7, TestInput(0))] }, PeerMessage::Hash { requested_frame: 0, hashes: vec![(0, 0)] }]),
            (10, 13, vec![PeerMessage::Input { requested_frame:  9, inputs: vec![( 8, TestInput(0))] }]),
            (11, 14, vec![PeerMessage::Input { requested_frame: 10, inputs: vec![( 9, TestInput(0))] }]),
            (12, 15, vec![PeerMessage::Input { requested_frame: 11, inputs: vec![(10, TestInput(0))] }]),
            (13, 16, vec![PeerMessage::Input { requested_frame: 12, inputs: vec![(11, TestInput(0))] }, PeerMessage::Hash { requested_frame: 0, hashes: vec![(0, 0)] }]),
            (14, 17, vec![PeerMessage::Input { requested_frame: 13, inputs: vec![(12, TestInput(0))] }]),
            (15, 18, vec![PeerMessage::Input { requested_frame: 14, inputs: vec![(13, TestInput(0))] }]),
            (16, 19, vec![PeerMessage::Input { requested_frame: 15, inputs: vec![(14, TestInput(0))] }]),
            (17, 20, vec![PeerMessage::Input { requested_frame: 16, inputs: vec![(15, TestInput(0))] }, PeerMessage::Hash { requested_frame: 0, hashes: vec![(0, 0)] }]),
            (18, 21, vec![PeerMessage::Input { requested_frame: 17, inputs: vec![(16, TestInput(0))] }]),
            (19, 22, vec![PeerMessage::Input { requested_frame: 18, inputs: vec![(17, TestInput(0))] }]),
            (20, 23, vec![PeerMessage::Input { requested_frame: 19, inputs: vec![(18, TestInput(0))] }]),
            (21, 24, vec![PeerMessage::Input { requested_frame: 20, inputs: vec![(19, TestInput(0))] }, PeerMessage::Hash { requested_frame: 0, hashes: vec![(0, 0)] }]),
            (22, 25, vec![PeerMessage::Input { requested_frame: 21, inputs: vec![(20, TestInput(0))] }]),
            (23, 26, vec![PeerMessage::Input { requested_frame: 22, inputs: vec![(21, TestInput(0))] }]),
            (24, 27, vec![PeerMessage::Input { requested_frame: 23, inputs: vec![(22, TestInput(0))] }]),
            (25, 28, vec![PeerMessage::Input { requested_frame: 24, inputs: vec![(23, TestInput(0))] }, PeerMessage::Hash { requested_frame: 0, hashes: vec![(0, 0)] }]),
            (26, 29, vec![PeerMessage::Input { requested_frame: 25, inputs: vec![(24, TestInput(0))] }]),
            (27, 30, vec![PeerMessage::Input { requested_frame: 26, inputs: vec![(25, TestInput(0))] }]),
            (28, 31, vec![PeerMessage::Input { requested_frame: 27, inputs: vec![(26, TestInput(0))] }]),
            (29, 32, vec![PeerMessage::Input { requested_frame: 28, inputs: vec![(27, TestInput(0))] }, PeerMessage::Hash { requested_frame: 0, hashes: vec![(0, 0)] }]),
            (30, 33, vec![PeerMessage::Input { requested_frame: 29, inputs: vec![(28, TestInput(0))] }]),
            (31, 34, vec![PeerMessage::Input { requested_frame: 30, inputs: vec![(29, TestInput(0))] }])

        ], "Should send input (requests & payload) / hash requests (every 4 tick) to other peers");

    } else {
        panic!("Should not leave simulation phase");
    }

}

#[test]
fn test_simulation_stalling() {

    let mut client = client_simulation();

    assert_eq!(client.resync_states.len(), 1);
    assert_eq!(client.handler.syncs, vec![
        (0, TestState { id: 0, v: 0 }, 3.0)
    ]);

    clear_sent(&mut client.peers[0]);
    clear_sent(&mut client.peers[2]);

    // We start at 3 seconds and 1 frame
    let mut frames: Vec<(u8, u32, f32)> = Vec::new();
    let initial_frames = 60;
    for i in 0..initial_frames {
        client.peers[0].rtc.as_mut().unwrap().packets_received = vec![vec![i + 5, 0]];
        client.peers[2].rtc.as_mut().unwrap().packets_received = vec![vec![i + 5, 0]];
        client.tick(3.0 + i as f64 * 33.333333); // Milliseconds
        if let ClientPhase::SimulatingOnline(phase) = &client.phase {
            frames.push((i, phase.frame.1, phase.frame_advantage));

        } else {
            panic!("Should not leave simulation phase");
        }
    }

    assert_eq!(frames[0..32].to_vec(), vec![
        ( 0,  1,  0.5),
        ( 1,  2,  1.0),
        ( 2,  3,  1.5),
        ( 3,  4,  2.0),
        ( 4,  5,  2.5),
        ( 5,  6,  3.0),
        ( 6,  7,  3.5),
        ( 7,  8,  4.0),
        ( 8,  9,  4.5),
        ( 9, 10,  5.0),
        (10, 11,  5.5),
        (11, 12,  6.0),
        (12, 13,  6.5),
        (13, 14,  7.0),
        (14, 15,  7.5),
        (15, 16,  8.0),
        (16, 17,  8.5),
        (17, 18,  9.0),
        (18, 18,  9.0),
        (19, 19,  9.5),
        (20, 20, 10.0),
        (21, 21, 10.5),
        (22, 22, 11.0),
        (23, 23, 11.5),
        (24, 24, 12.0),
        (25, 25, 12.5),
        (26, 26, 13.0),
        (27, 27, 13.5),
        (28, 28, 14.0),
        (29, 29, 14.5),
        (30, 30, 15.0),
        (31, 30, 15.0)

    ], "Does apply stalling once input advantage rises too much.");

    if let ClientPhase::SimulatingOnline(phase) = &client.phase {
        assert_eq!(phase.frame.0, 0, "Should have committed 0 frames");
        assert_eq!(phase.frame.1, 30, "Should have stopped after simulating 30 frames in advance");
        assert_eq!(phase.state.id, 29, "Should have stalled after simulating 29 states");
        assert_eq!(client.local_inputs.len(), 30, "Should have accumulated 30 local pending inputs not confirmed by the other peers");
        assert_eq!(client.local_hashes.len(), 0, "Should have no local state hashes");
        assert_eq!(client.resync_states.len(), 1, "Should have one resync states");
        assert_eq!(client.resync_states[0].state, client.handler.state(TestStateConfig::default(), &client.peers), "Should have a copy of the base state as the only resync state");
        assert_eq!(client.resync_states[0].frame, 0);
        assert_eq!(client.resync_states[0].frame_elapsed_time, 0.0);
        assert_eq!(client.resync_states[0].tick_total, 0);
        assert_eq!(client.peers[0].rtc.as_mut().unwrap().packets_sent.len(), initial_frames as usize, "Should send exactly 30 packets, one for each tick during the simulation phase");

    } else {
        panic!("Should not leave simulation phase");
    }

    // Resume
    frames.clear();
    for i in 0..170 {
        client.peers[0].rtc.as_mut().unwrap().packets_received = vec![vec![i + 5 + initial_frames, 0,  1,  0, 0, i,  0, 0,  3, 0, 0, 0]];
        client.peers[2].rtc.as_mut().unwrap().packets_received = vec![vec![i + 5 + initial_frames, 0,  1,  0, 0, i,  0, 0,  3, 0, 0, 0]];

        client.tick(initial_frames as f64 + 3.0 + i as f64 * 33.333333); // Milliseconds
        if let ClientPhase::SimulatingOnline(phase) = &client.phase {
            frames.push((i, phase.frame.1, phase.frame_advantage));

        } else {
            panic!("Should not leave simulation phase");
        }
    }

    assert_eq!(frames[160..170].to_vec(), vec![
        (160, 171, 3.5),
        (161, 172, 3.5),
        (162, 173, 3.5),
        (163, 174, 3.5),
        (164, 175, 3.5),
        (165, 176, 3.5),
        (166, 177, 3.5),
        (167, 178, 3.5),
        (168, 179, 3.5),
        (169, 179, 3.0)

    ], "Does correct input advantage and stalling back down once peer input is received.");

    if let ClientPhase::SimulatingOnline(phase) = &client.phase {
        assert_eq!(phase.frame.0, 170, "Should have committed 170 frames");
        assert_eq!(phase.frame.1, 179, "Should have simulated a total of 179 frames");
        assert_eq!(phase.state.id, 178, "Should have simulated a total of 178 frames");
        assert_eq!(client.local_inputs.len(), 11, "Should have local pending inputs left not confirmed by the other peers");
        assert_eq!(client.local_hashes.len(), 1, "Should have one local state hash");
        assert_eq!(client.resync_states.len(), 7, "Should have one resync states");
        assert_eq!(client.peers[0].rtc.as_mut().unwrap().packets_sent.len(), 230, "Should send exactly 230 packets, one for each tick during the simulation phase");

    } else {
        panic!("Should not leave simulation phase");
    }

}

#[test]
fn test_simulation_deviation() {

    let mut client = client_simulation();

    assert_eq!(client.resync_states.len(), 1);
    assert_eq!(client.handler.syncs, vec![
        (0, TestState { id: 0, v: 0 }, 3.0)
    ]);

    clear_sent(&mut client.peers[0]);
    clear_sent(&mut client.peers[2]);

    // We start at 3 seconds and 1 frame
    let mut frames: Vec<(u8, u32, f32)> = Vec::new();
    let initial_frames = 60;
    let mut t = 0.0f64;
    for i in 0..initial_frames {
        let e = (t / 33.33333).floor() as u8;
        client.peers[0].rtc.as_mut().unwrap().packets_received = vec![vec![e + 5, 0,  1,  0, 0, e,  0, 0,  3, 0, 0, 0]];
        client.peers[2].rtc.as_mut().unwrap().packets_received = vec![vec![e + 5, 0,  1,  0, 0, e,  0, 0,  3, 0, 0, 0]];

        client.tick(3.0 + i as f64 * 33.333333); // Milliseconds
        t += 24.0;
        if let ClientPhase::SimulatingOnline(phase) = &client.phase {
            frames.push((i, phase.frame.1, phase.frame_advantage));

        } else {
            panic!("Should not leave simulation phase");
        }
    }

    if let ClientPhase::SimulatingOnline(phase) = &client.phase {
        assert_eq!(phase.frame.0, 44, "Should have committed 44 frames");
        assert_eq!(phase.frame.1, 59, "Should have simulated a total of only 59 local frames due to the frame_deviation");
        assert_eq!(phase.frame_advantage, 6.5, "Should have a frame_advantage over the slower remote");
        assert_eq!((phase.frame_deviation * 100.0).floor() / 100.0, 1.03, "Should lag behind the optimal frame number due to the slow down required to correct the frame_advantage");
    }

}

#[test]
fn test_simulation_state_committal() {

    let mut client = client_simulation();

    // Receive inputs from other peers
    client.peers[0].rtc.as_mut().unwrap().packets_received = vec![vec![
        3, 0,

        1, 0, 0, 0,  0, 0,   1,  64
    ]];
    client.peers[2].rtc.as_mut().unwrap().packets_received = vec![vec![
        3, 0,
        1, 0, 0, 0,  0, 0,   1,  128
    ]];

    assert_eq!(client.handler.applied, vec![], "Should not apply any states before the first tick");
    assert_eq!(client.local_inputs.len(), 0);

    client.tick(33.0);
    assert_eq!(client.handler.applied, vec![(0, TestState { id: 1, v: 192 }, 33.0), (1, TestState { id: 1, v: 192 }, 33.0)], "Should have applied 2 states: The initial one, and the prediction of the next one");

    client.tick(36.0);
    assert_eq!(client.local_inputs.len(), 2);
    assert_eq!(client.local_inputs.len(), 2, "Should have remote the confirmed local input for frame #0");
    assert_eq!(client.local_hashes.len(), 1, "Should have created a local hash for frame #0");

    assert_eq!(client.handler.committed, vec![(0, TestState { id: 0, v: 0 }, 36.0)], "Should commit the first state after receiving remote inputs for #0 frame");
    assert_eq!(client.handler.reverted, vec![], "Should not revert any states before the first tick");
    assert_eq!(client.handler.applied, vec![(0, TestState { id: 1, v: 192 }, 33.0), (1, TestState { id: 1, v: 192 }, 33.0), (2, TestState { id: 2, v: 384 }, 36.0)], "Should have applied 3 states: The initial one, and the next two predictions");

    client.tick(68.0);
    assert_eq!(client.handler.applied, vec![
        (0, TestState { id: 1, v: 192 }, 33.0),
        (1, TestState { id: 1, v: 192 }, 33.0),
        (2, TestState { id: 2, v: 384 }, 36.0)
    ]);

    // Receive more inputs from other peers
    client.peers[0].rtc.as_mut().unwrap().packets_received = vec![vec![
        4, 1,

        1, 0, 0, 1,  0, 0,   1,  16
    ]];
    client.peers[2].rtc.as_mut().unwrap().packets_received = vec![vec![
        4, 1,
        1, 0, 0, 1,  0, 0,   1,  24
    ]];

    client.tick(69.0);
    assert_eq!(client.handler.applied, vec![
        (0, TestState { id: 1, v: 192 }, 33.0),
        (1, TestState { id: 1, v: 192 }, 33.0),
        (2, TestState { id: 2, v: 384 }, 36.0),
        (1, TestState { id: 2, v: 232 }, 69.0),
        (2, TestState { id: 3, v: 272 }, 69.0),
        (3, TestState { id: 3, v: 272 }, 69.0)

    ], "Should have apply newly predicted states after receiving actual inputs");

    assert_eq!(client.local_inputs.len(), 3);
    assert_eq!(client.handler.committed, vec![
        (0, TestState { id: 0, v: 0 }, 36.0),
        (1, TestState { id: 1, v: 192 }, 69.0)

    ], "Should commit the first state after receiving remote inputs for #0 frame");

    assert_eq!(client.handler.reverted, vec![
        (2, TestState { id: 2, v: 384 }, 69.0)

    ], "Should revert state #2 after receiving remote inputs for #1 frame");

}

#[test]
fn test_simulation_peer_inputs() {

    let mut client = client_input_simulation();

    // Receive inputs from other peers
    client.peers[0].rtc.as_mut().unwrap().packets_received = vec![vec![
        3, 0,

        1, 0, 0, 0,  0, 0,   1,  64
    ]];
    client.peers[2].rtc.as_mut().unwrap().packets_received = vec![vec![
        3, 0,
        1, 0, 0, 0,  0, 0,   1,  128
    ]];

    client.tick(36.0);


    // Receive more inputs from other peers but in different order
    client.peers[2].rtc.as_mut().unwrap().packets_received = vec![vec![
        4, 1,
        1, 0, 0, 1,  0, 0,   1,  24
    ]];
    client.tick(45.0);
    client.peers[0].rtc.as_mut().unwrap().packets_received = vec![vec![
        4, 1,

        1, 0, 0, 1,  0, 0,   1,  16
    ]];
    client.tick(69.0);
    assert_eq!(client.resync_states.len(), 2, "Should create new resync state every 2 committed frames.");

    // Receive more inputs from other peers but in different order again
    client.peers[0].rtc.as_mut().unwrap().packets_received = vec![vec![
        5, 1,

        1, 0, 0, 2,  0, 0,   1,  8
    ]];
    client.tick(90.0);
    client.peers[2].rtc.as_mut().unwrap().packets_received = vec![vec![
        5, 1,
        1, 0, 0, 2,  0, 0,   1,  96
    ]];
    client.tick(102.0);

    // Now receive inputs across two local ticks - these should never be seen after the re-sync to
    // the previous frame
    client.peers[2].rtc.as_mut().unwrap().packets_received = vec![vec![
        6, 1,
        1, 0, 0, 3,  0, 0,   1,  4
    ]];
    client.tick(135.0);
    client.peers[0].rtc.as_mut().unwrap().packets_received = vec![vec![
        6, 1,

        1, 0, 0, 3,  0, 0,   1,  2
    ]];
    client.tick(168.0);
    assert_eq!(client.resync_states.len(), 3, "Should create new resync state every 2 committed frames.");

    // Now timeout remote peers
    client.tick(19999.0); // Timeout happens
    //client.tick(20000.0); // Timeout happens
    client.tick(20000.0); // Local Re-sync happens
    assert_eq!(client.resync_states.len(), 1, "Should drop old sync states after re-sync");

    client.tick(20032.0);
    client.tick(20065.0);
    client.tick(20098.0);
    client.tick(20131.0);

    assert_eq!(client.handler.syncs, vec![
        (0, TestInputState { frames: vec![], inputs: vec![] }, 3.0),
        (2, TestInputState { frames: vec![0, 1], inputs: vec![
            (0, PeerID(0), Some(64)),
            (0, PeerID(1), Some(0)),
            (0, PeerID(2), Some(128)),

            (1, PeerID(0), Some(16)),
            (1, PeerID(1), Some(0)),
            (1, PeerID(2), Some(24))]

        }, 20000.0)

    ], "Should re-sync to last local sync state");

    assert_eq!(client.handler.committed, vec![
        (0, TestInputState { frames: vec![], inputs: vec![] }, 36.0),
        (1, TestInputState { frames: vec![0], inputs: vec![
            (0, PeerID(0), Some(64)),
            (0, PeerID(1), Some(0)),
            (0, PeerID(2), Some(128))

        ] }, 69.0),
        (2, TestInputState { frames: vec![0, 1], inputs: vec![
            (0, PeerID(0), Some(64)),
            (0, PeerID(1), Some(0)),
            (0, PeerID(2), Some(128)),

            (1, PeerID(0), Some(16)),
            (1, PeerID(1), Some(0)),
            (1, PeerID(2), Some(24))

        ] }, 102.0),
        (3, TestInputState { frames: vec![0, 1, 2], inputs: vec![
            (0, PeerID(0), Some(64)),
            (0, PeerID(1), Some(0)),
            (0, PeerID(2), Some(128)),

            (1, PeerID(0), Some(16)),
            (1, PeerID(1), Some(0)),
            (1, PeerID(2), Some(24)),

            (2, PeerID(0), Some(8)),
            (2, PeerID(1), Some(0)),
            (2, PeerID(2), Some(96))

        ] }, 168.0),

        // Expect Re-sync
        (2, TestInputState { frames: vec![0, 1], inputs: vec![
            (0, PeerID(0), Some(64)),
            (0, PeerID(1), Some(0)),
            (0, PeerID(2), Some(128)),

            (1, PeerID(0), Some(16)),
            (1, PeerID(1), Some(0)),
            (1, PeerID(2), Some(24))

        ] }, 20032.0),

        // Expect disconnected peers to have None inputs
        (3, TestInputState { frames: vec![0, 1, 2], inputs: vec![
            (0, PeerID(0), Some(64)),
            (0, PeerID(1), Some(0)),
            (0, PeerID(2), Some(128)),

            (1, PeerID(0), Some(16)),
            (1, PeerID(1), Some(0)),
            (1, PeerID(2), Some(24)),

            (2, PeerID(0), None),
            (2, PeerID(1), Some(0)),
            (2, PeerID(2), None)

        ] }, 20065.0),
        (4, TestInputState { frames: vec![0, 1, 2, 3], inputs: vec![
            (0, PeerID(0), Some(64)),
            (0, PeerID(1), Some(0)),
            (0, PeerID(2), Some(128)),

            (1, PeerID(0), Some(16)),
            (1, PeerID(1), Some(0)),
            (1, PeerID(2), Some(24)),

            (2, PeerID(0), None),
            (2, PeerID(1), Some(0)),
            (2, PeerID(2), None),

            (3, PeerID(0), None),
            (3, PeerID(1), Some(0)),
            (3, PeerID(2), None)

        ] }, 20098.0),
        (5, TestInputState { frames: vec![0, 1, 2, 3, 4], inputs: vec![
            (0, PeerID(0), Some(64)),
            (0, PeerID(1), Some(0)),
            (0, PeerID(2), Some(128)),

            (1, PeerID(0), Some(16)),
            (1, PeerID(1), Some(0)),
            (1, PeerID(2), Some(24)),

            (2, PeerID(0), None),
            (2, PeerID(1), Some(0)),
            (2, PeerID(2), None),

            (3, PeerID(0), None),
            (3, PeerID(1), Some(0)),
            (3, PeerID(2), None),

            (4, PeerID(0), None),
            (4, PeerID(1), Some(0)),
            (4, PeerID(2), None)

        ] }, 20131.0)
    ]);

}

