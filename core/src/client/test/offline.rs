// Internal Dependencies ------------------------------------------------------
use super::{client_create, phase_simulation_offline, phase_paused_offline, ClientPhase};
use crate::phase;
use crate::PeerID;
use crate::test::{TestState, TestStateConfig, StateTransition};


// Tests ----------------------------------------------------------------------
#[test]
fn test_offline_start() {

    let mut client = client_create();
    client.handler.command.disconnected = Some(phase::Disconnected::Command::Start(TestStateConfig::default()));
    client.tick(1.0);

    assert_eq!(client.peers.len(), 1, "Should add a fake local peer for offline play");
    assert_eq!(client.peers[0].is_local(), true, "Offline peer should be local");
    assert_eq!(client.peers[0].is_ready(), true, "Offline peer should be ready");
    assert_eq!(client.handler.peer_connects, vec![
        (PeerID(16384), 1.0),
    ]);

    assert_eq!(client.phase, phase_simulation_offline((0, 0)), "Should start simulation directly");

    assert_eq!(client.handler.transitions, vec![
        StateTransition::Leave(ClientPhase::Disconnected, 1.0),
        StateTransition::Enter(phase_simulation_offline((0, 0)), 1.0)
    ]);

}

#[test]
fn test_offline_stop() {

    let mut client = client_create();
    client.handler.command.disconnected = Some(phase::Disconnected::Command::Start(TestStateConfig::default()));
    client.tick(2.0);
    assert_eq!(client.phase, phase_simulation_offline((0, 0)));

    client.handler.command.simulating_offline = Some(phase::SimulatingOffline::Command::Stop);
    client.tick(4.0);
    assert_eq!(client.phase, ClientPhase::Disconnected, "Should immediately disconnect when offline");
    assert_eq!(client.peers.len(), 0);
    assert_eq!(client.handler.peer_disconnects, vec![(PeerID(16384), 4.0)], "Should disconnect offline peer");

}

#[test]
fn test_offline_pause() {

    let mut client = client_create();
    client.handler.command.disconnected = Some(phase::Disconnected::Command::Start(TestStateConfig::default()));
    client.tick(2.0);
    assert_eq!(client.phase, phase_simulation_offline((0, 0)));

    client.handler.command.simulating_offline = Some(phase::SimulatingOffline::Command::Pause);
    client.tick(4.0);
    assert_eq!(client.phase, phase_paused_offline((0, 0)), "Should immediately pause when offline");
    assert_eq!(client.peers.len(), 1);
    assert_eq!(client.handler.peer_disconnects, vec![], "Should not disconnect offline peer");

}

#[test]
fn test_offline_pause_resume() {

    let mut client = client_create();
    client.handler.command.disconnected = Some(phase::Disconnected::Command::Start(TestStateConfig::default()));
    client.tick(2.0);
    assert_eq!(client.phase, phase_simulation_offline((0, 0)));

    // Issue disconnect
    client.handler.command.simulating_offline = Some(phase::SimulatingOffline::Command::Pause);
    client.tick(4.0);
    assert_eq!(client.phase, phase_paused_offline((0, 0)), "Should immediately pause when offline");
    client.handler.command.paused_offline = Some(phase::PausedOffline::Command::Resume);
    client.tick(4.0);
    assert_eq!(client.phase, phase_simulation_offline((0, 0)), "Should immediately resume when offline");

}

#[test]
fn test_offline_pause_stop() {

    let mut client = client_create();
    client.handler.command.disconnected = Some(phase::Disconnected::Command::Start(TestStateConfig::default()));
    client.tick(2.0);
    assert_eq!(client.phase, phase_simulation_offline((0, 0)));

    // Issue disconnect
    client.handler.command.simulating_offline = Some(phase::SimulatingOffline::Command::Pause);
    client.tick(4.0);
    assert_eq!(client.phase, phase_paused_offline((0, 0)), "Should immediately pause when offline");
    client.handler.command.paused_offline = Some(phase::PausedOffline::Command::Stop);
    client.tick(4.0);
    assert_eq!(client.phase, ClientPhase::Disconnected, "Should immediately disconnect when offline");
    assert_eq!(client.peers.len(), 0);
    assert_eq!(client.handler.peer_disconnects, vec![(PeerID(16384), 4.0)], "Should disconnect offline peer");

}

#[test]
fn test_offline_ticking() {

    let mut client = client_create();
    client.handler.command.disconnected = Some(phase::Disconnected::Command::Start(TestStateConfig::default()));
    client.tick(3.0);

    assert_eq!(client.resync_states.len(), 1);
    assert_eq!(client.handler.syncs, vec![
        (0, TestState { id: 0, v: 0 }, 3.0)
    ]);

    // We start at 3 seconds and 1 frame
    for i in 0..600 {
        client.tick(3.0 + i as f64 * 33.333333); // Milliseconds
    }

    assert_eq!(client.handler.ticks.len(), 601, "Should have executed exactly 601 ticks");

    if let ClientPhase::SimulatingOffline(phase) = client.phase {
        assert_eq!(phase.frame.0, 604, "Should have comitted 604 frames");
        assert_eq!(phase.frame.1, 606, "Should have simulated 303 frames");
        assert_eq!(phase.state.id, 606, "Should have simulated 303 states");
        assert_eq!(client.local_inputs.len(), 1, "Should only have one local input pending");
        assert_eq!(client.local_hashes.len(), 0, "Should have created no local hashes");
        assert_eq!(client.resync_states.len(), 16, "Should not have generated more than 16 resync states");

        assert_eq!(client.resync_states[0].frame, 150);
        assert_eq!(client.resync_states[0].frame_elapsed_time, 4999.99995);
        assert_eq!(client.resync_states[0].tick_total, 151);

        assert_eq!(client.resync_states[15].frame, 600);
        assert_eq!(client.resync_states[15].frame_elapsed_time, 19833.333135);
        assert_eq!(client.resync_states[15].tick_total, 596);


        assert!(&client.peers[0].rtc.is_none(), "Should send input (requests & payload) / hash requests (every 4 tick) to other peers");

    } else {
        panic!("Should not leave simulation phase");
    }

}

