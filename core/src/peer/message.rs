// STD Dependencies -----------------------------------------------------------
use std::cmp;


// Internal Dependencies ------------------------------------------------------
use crate::PeerID;
use crate::traits::Input;


// Peer Messaging -------------------------------------------------------------
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum PeerMessage<I: Input> {
    Starting(Vec<PeerID>),
    Input {
        requested_frame: u32,
        inputs: Vec<(u32, I)>
    },
    Hash {
        requested_frame: u32,
        hashes: Vec<(u32, u64)>
    },
    Sync {
        latest_sync_frame: u32
    },
    Disconnecting
}

impl<I: Input> PeerMessage<I> {

    pub fn is_starting(&self) -> bool {
        if let PeerMessage::Starting(_) = self {
            true

        } else {
            false
        }
    }

    pub fn into_bytes(self) -> Vec<u8> {
        match self {
            PeerMessage::Starting(peers) => {
                assert!(peers.len() < 256, "Peer count exceeds 8 bit length");
                let mut bytes = vec![0, peers.len() as u8];
                for p in peers {
                    bytes.push((p.0 >> 8) as u8);
                    bytes.push(p.0 as u8);
                }
                bytes
            },
            PeerMessage::Input { requested_frame, inputs }=> {
                if inputs.is_empty() {
                    let mut bytes = vec![2];
                    bytes.append(&mut Self::u24_to_bytes(requested_frame));
                    bytes.push(0);
                    bytes

                } else {
                    let base_frame = cmp::min(requested_frame, inputs[0].0);
                    assert!(base_frame < 16_777_216, "Base frame exceeds 24 bit rate");

                    let remote_frame = requested_frame - base_frame;
                    assert!(remote_frame < 256, "Remote frame exceeds 8 bit offset");

                    let input_frame = inputs[0].0 - base_frame;
                    assert!(input_frame < 256, "Input frame exceeds 8 bit offset");

                    let mut bytes = vec![1];
                    bytes.append(&mut Self::u24_to_bytes(base_frame));
                    bytes.push(remote_frame as u8);
                    bytes.push(input_frame as u8);

                    assert!(inputs.len() < 256, "Input count exceeds 8 bit length");
                    bytes.push(inputs.len() as u8);
                    for i in inputs {
                        bytes.push(i.1.into_byte());
                    }
                    bytes
                }
            },
            PeerMessage::Hash { requested_frame, hashes } => {
                let mut bytes = vec![3];
                bytes.append(&mut Self::u24_to_bytes(requested_frame));
                assert!(hashes.len() < 256, "Hash count exceeds 8 bit length");
                bytes.push(hashes.len() as u8);
                for h in hashes {
                    bytes.append(&mut Self::u24_to_bytes(h.0));
                    bytes.append(&mut Self::u64_to_bytes(h.1));
                }
                bytes
            },
            PeerMessage::Sync { latest_sync_frame }=> {
                let mut bytes = vec![4];
                bytes.append(&mut Self::u24_to_bytes(latest_sync_frame));
                bytes
            },
            PeerMessage::Disconnecting => vec![5]
        }
    }

    pub fn all_from_bytes(bytes: &[u8]) -> Vec<Self> {

        let mut messages = Vec::new();
        let mut slice = &bytes[..];
        while !slice.is_empty() {
            if let Some((msg, len)) = PeerMessage::from_bytes(slice) {
                messages.push(msg);
                slice = &slice[len..];

            } else {
                break;
            }
        }

        messages

    }

    fn from_bytes(bytes: &[u8]) -> Option<(Self, usize)> {
        if bytes.len() >= 2 && bytes[0] == 0 {
            let peer_count = bytes[1] as usize;
            let mut peers = Vec::with_capacity(peer_count);
            if bytes.len() > 1 + peer_count * 2 {
                for i in 0..peer_count {
                    let s = &bytes[2 + i * 2..];
                    peers.push(PeerID(u16::from(s[0]) << 8 | u16::from(s[1])));
                }
                Some((PeerMessage::Starting(peers), 2 + peer_count * 2))

            } else {
                None
            }

        } else if bytes.len() >= 8 && bytes[0] == 1 {
            let base_frame = Self::u24_from_bytes(&bytes[1..4]);
            let requested_frame = base_frame + u32::from(bytes[4]);
            let input_frame = base_frame + u32::from(bytes[5]);
            let input_count = bytes[6] as usize;

            if bytes.len() >= 7 + input_count {
                let inputs: Vec<(u32, I)> = bytes[7..7 + input_count].iter().enumerate().map(|(index, v)| {
                    (input_frame + index as u32, I::from_byte(*v))

                }).collect();

                Some((PeerMessage::Input {
                    requested_frame,
                    inputs

                }, 7 + input_count))

            } else {
                None
            }

        } else if bytes.len() >= 5 && bytes[0] == 2 {
            let base_frame = Self::u24_from_bytes(&bytes[1..4]);
            let requested_frame = base_frame + u32::from(bytes[4]);
            Some((PeerMessage::Input {
                requested_frame,
                inputs: Vec::new()

            }, 5))

        } else if bytes.len() >= 5 && bytes[0] == 3 {
            let requested_frame = Self::u24_from_bytes(&bytes[1..4]);
            let hash_count = bytes[4] as usize;
            if bytes.len() >= 4 + hash_count * 11 {
                let mut hashes = Vec::new();
                for i in 0..hash_count {
                    let offset = i * 11;
                    hashes.push((
                        Self::u24_from_bytes(&bytes[5 + offset..]),
                        Self::u64_from_bytes(&bytes[5 + offset + 3..])
                    ));
                }

                Some((
                    PeerMessage::Hash {
                        requested_frame,
                        hashes
                    },
                    5 + hash_count * 11
                ))

            } else {
                None
            }

        } else if bytes.len() >= 4 && bytes[0] == 4 {
            Some((PeerMessage::Sync {
                latest_sync_frame: Self::u24_from_bytes(&bytes[1..4])

            }, 4))

        } else if !bytes.is_empty() && bytes[0] == 5 {
            Some((PeerMessage::Disconnecting, 1))

        } else {
            None
        }

    }

    fn u24_to_bytes(i: u32) -> Vec<u8> {
        vec![
            (i >> 16) as u8,
            (i >> 8) as u8,
            i as u8
        ]
    }

    fn u24_from_bytes(bytes: &[u8]) -> u32 {
        u32::from(bytes[0]) << 16 | u32::from(bytes[1]) << 8 | u32::from(bytes[2])
    }

    fn u64_to_bytes(i: u64) -> Vec<u8> {
        vec![
            (i >> 56) as u8,
            (i >> 48) as u8,
            (i >> 40) as u8,
            (i >> 32) as u8,
            (i >> 24) as u8,
            (i >> 16) as u8,
            (i >> 8) as u8,
            i as u8
        ]
    }

    fn u64_from_bytes(bytes: &[u8]) -> u64 {
        u64::from(bytes[0]) << 56 | u64::from(bytes[1]) << 48 | u64::from(bytes[2]) << 40 | u64::from(bytes[3]) << 32 |
        u64::from(bytes[4]) << 24 | u64::from(bytes[5]) << 16 | u64::from(bytes[6]) << 8 | u64::from(bytes[7])
    }

}


// Tests ----------------------------------------------------------------------
#[cfg(test)]
mod test {

    use super::PeerMessage;
    use crate::PeerID;
    use crate::test::TestInput;

    #[test]
    fn test_u24_conversion() {
        assert_eq!(PeerMessage::<TestInput>::u24_to_bytes(12345123), vec![188, 95, 35]);
        assert_eq!(PeerMessage::<TestInput>::u24_from_bytes(&vec![188, 95, 35]), 12345123);
    }

    #[test]
    fn test_u64_conversion() {
        assert_eq!(PeerMessage::<TestInput>::u64_to_bytes(4070380235671476092), vec![56, 124, 229, 66, 47, 248, 23, 124]);
        assert_eq!(PeerMessage::<TestInput>::u64_from_bytes(&vec![56, 124, 229, 66, 47, 248, 23, 124]), 4070380235671476092);
    }

    #[test]
    fn test_starting_message() {
        assert_eq!(PeerMessage::<TestInput>::from_bytes(&vec![0, 3, 1, 2]), None, "Should not fail on malformed payload");
        assert_eq!(PeerMessage::<TestInput>::into_bytes(PeerMessage::Starting(vec![PeerID(0), PeerID(255), PeerID(2048), PeerID(32768)])), vec![
            0, 4,
            0, 0,
            0, 255,
            8, 0,
            128, 0
        ]);
        assert_eq!(PeerMessage::<TestInput>::from_bytes(&vec![0, 4, 0, 0, 0, 255, 8, 0, 128, 0]), Some((PeerMessage::Starting(
            vec![PeerID(0), PeerID(255), PeerID(2048), PeerID(32768)]

        ), 10)));
    }

    #[test]
    fn test_ignore_invalid() {
        assert_eq!(PeerMessage::<TestInput>::from_bytes(&vec![]), None);
        assert_eq!(PeerMessage::<TestInput>::from_bytes(&vec![2]), None);
        assert_eq!(PeerMessage::<TestInput>::from_bytes(&vec![8, 4, 1, 3, 2, 1]), None);
    }

    #[test]
    fn test_input_message() {

        let m = PeerMessage::Input {
            requested_frame: 22,
            inputs: vec![(25, TestInput(1)), (26, TestInput(2)), (27, TestInput(3))]
        };
        assert_eq!(m.clone().into_bytes(), vec![1, 0, 0, 22, 0, 3, 3, 1, 2, 3]);
        assert_eq!(PeerMessage::from_bytes(&vec![1, 0, 0, 22, 0, 3, 3, 1, 2, 3]), Some((m, 10)));

        let m = PeerMessage::Input {
            requested_frame: 29,
            inputs: vec![(25, TestInput(1)), (26, TestInput(2)), (27, TestInput(3))]
        };
        assert_eq!(m.clone().into_bytes(), vec![1, 0, 0, 25, 4, 0, 3, 1, 2, 3]);
        assert_eq!(PeerMessage::from_bytes(&vec![1, 0, 0, 25, 4, 0, 3, 1, 2, 3]), Some((m, 10)));

    }

    #[test]
    fn test_empty_input_message() {

        let m: PeerMessage<TestInput> = PeerMessage::Input {
            requested_frame: 22,
            inputs: vec![]
        };
        assert_eq!(m.clone().into_bytes(), vec![2, 0, 0, 22, 0]);
        assert_eq!(PeerMessage::from_bytes(&vec![2, 0, 0, 22, 0]), Some((m, 5)));

    }

    #[test]
    fn test_hash_message() {
        let m = PeerMessage::Hash {
            requested_frame: 180,
            hashes: vec![(0, 4070380235671476092), (60, 123123123), (120, 99123123)]
        };
        assert_eq!(PeerMessage::<TestInput>::into_bytes(m.clone()), vec![
            3, 0, 0, 180, 3, 0, 0, 0, 56, 124, 229, 66, 47, 248, 23, 124, 0, 0, 60, 0, 0, 0, 0, 7, 86, 181, 179, 0, 0, 120, 0, 0, 0, 0, 5, 232, 127, 179
        ]);
        assert_eq!(PeerMessage::<TestInput>::from_bytes(&vec![
            3, 0, 0, 180, 3, 0, 0, 0, 56, 124, 229, 66, 47, 248, 23, 124, 0, 0, 60, 0, 0, 0, 0, 7, 86, 181, 179, 0, 0, 120, 0, 0, 0, 0, 5, 232, 127, 179

        ]), Some((m, 38)));
    }

    #[test]
    fn test_sync_message() {
        assert_eq!(PeerMessage::<TestInput>::into_bytes(PeerMessage::Sync { latest_sync_frame: 120 }), vec![4, 0, 0, 120]);
        assert_eq!(PeerMessage::<TestInput>::from_bytes(&vec![4, 0, 0, 120]), Some((PeerMessage::Sync { latest_sync_frame: 120 }, 4)));
    }

    #[test]
    fn test_disconnecting_message() {
        assert_eq!(PeerMessage::<TestInput>::into_bytes(PeerMessage::Disconnecting), vec![5]);
        assert_eq!(PeerMessage::<TestInput>::from_bytes(&vec![5]), Some((PeerMessage::Disconnecting, 1)));
    }

    #[test]
    fn test_multiple_messages() {
        assert_eq!(PeerMessage::<TestInput>::all_from_bytes(&vec![
            0, 0,
            0, 0,
            1, 0, 0, 25, 4, 0, 3, 1, 2, 3,
            3, 0, 0, 180, 3, 0, 0, 0, 56, 124, 229, 66, 47, 248, 23, 124, 0, 0, 60, 0, 0, 0, 0, 7, 86, 181, 179, 0, 0, 120, 0, 0, 0, 0, 5, 232, 127, 179,
            2, 0, 0, 22, 0,
            2, 0, 0, 22, 0,

        ]), vec![
            PeerMessage::Starting(Vec::new()),
            PeerMessage::Starting(Vec::new()),
            PeerMessage::Input {
                requested_frame: 29,
                inputs: vec![(25, TestInput(1)), (26, TestInput(2)), (27, TestInput(3))]
            },
            PeerMessage::Hash {
                requested_frame: 180,
                hashes: vec![(0, 4070380235671476092), (60, 123123123), (120, 99123123)]
            },
            PeerMessage::Input {
                requested_frame: 22,
                inputs: vec![]
            },
            PeerMessage::Input {
                requested_frame: 22,
                inputs: vec![]
            },
        ]);
    }

}

