// STD Dependencies -----------------------------------------------------------
use std::mem;
use std::collections::VecDeque;


// Internal Dependencies ------------------------------------------------------
use crate::util::{Average, Sum};


// Statics --------------------------------------------------------------------
const PACKET_HEADER_SIZE: usize = 2;


// UDP Peer Connection Implementation -----------------------------------------
pub struct PeerConnection {
    local_seq: u8,
    last_remote_seq: u8,
    last_receive_time: f64,

    connected: bool,
    pub send_buffer: Vec<u8>,
    pub receive_buffer: VecDeque<Vec<u8>>,

    pub packet_ack: [(f64, bool); 256],
    pub packet_rtt: Average<f64>,
    pub packet_loss: Average<f64>,

    pub bytes_incoming: Sum<f64>,
    pub bytes_outgoing: Sum<f64>
}

impl PeerConnection {

    pub fn new(tick_rate: u32) -> Self {
        Self {
            local_seq: 0,
            last_remote_seq: 0,
            last_receive_time: 0.0,

            connected: false,
            send_buffer: Vec::new(),
            receive_buffer: VecDeque::new(),

            packet_ack: [(0.0, false); 256],
            packet_rtt: Average::new(tick_rate, 0.0),
            packet_loss: Average::new(tick_rate, 0.0),

            bytes_incoming: Sum::new(tick_rate, 0.0),
            bytes_outgoing: Sum::new(tick_rate, 0.0)
        }
    }

    pub fn reset(&mut self) {
        self.connected = false;
    }

    pub fn receive(&mut self, packet: &[u8], now: f64) -> bool {

        self.bytes_incoming.push(packet.len() as f64);

        if packet.len() >= PACKET_HEADER_SIZE {

            // Reset upon connection
            if !self.connected {
                self.connected = true;
                self.local_seq = 0;
                self.last_remote_seq = 0;
                self.last_receive_time = now;
                self.send_buffer.clear();
                self.receive_buffer.clear();
                for p in self.packet_ack.iter_mut() {
                    *p = (now, true);
                }
                self.packet_rtt.reset();
                self.packet_loss.reset();
                self.bytes_incoming.reset();
                self.bytes_outgoing.reset();
            }

            let remote_seq = packet[0];
            if PeerConnection::seq_is_more_recent(remote_seq, self.last_remote_seq) {

                let local_ack_seq = packet[1] as usize;
                self.packet_rtt.push((now - self.packet_ack[local_ack_seq].0).max(0.0));
                self.packet_ack[local_ack_seq].1 = true;

                let seq_diff = if self.last_remote_seq > remote_seq {
                    255 - (self.last_remote_seq - remote_seq)

                } else {
                    (remote_seq - self.last_remote_seq) - 1
                };

                self.packet_loss.push(f64::from(seq_diff));
                self.last_receive_time = now;
                self.last_remote_seq = remote_seq;
                self.receive_buffer.push_back(packet[PACKET_HEADER_SIZE..].to_vec());

                true

            } else {
                false
            }

        } else {
            false
        }

    }

    pub fn send(&mut self, now: f64) -> Vec<u8> {

        let mut packet = vec![self.local_seq, self.last_remote_seq];
        let mut payload = mem::replace(&mut self.send_buffer, Vec::new());
        packet.append(&mut payload);

        // Handle RTT in case we don't receive any packets at all
        let delay = (now - self.last_receive_time).max(0.0).min(15000.0);
        if delay > 1000.0 || !self.packet_ack[self.local_seq as usize].1 {
            self.packet_rtt.push(delay);
        }

        self.packet_ack[self.local_seq as usize] = (now, false);
        self.local_seq = self.local_seq.wrapping_add(1);

        self.bytes_outgoing.push(packet.len() as f64);

        packet

    }

    fn seq_is_more_recent(a: u8, b: u8) -> bool {
        (a > b) && (a - b <= 128) ||
        (b > a) && (b - a >  128)
    }

}

