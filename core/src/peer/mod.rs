// STD Dependencies -----------------------------------------------------------
use std::fmt;


// External Dependencies ------------------------------------------------------
use serde_derive::{Deserialize, Serialize};


// Internal Dependencies ------------------------------------------------------
use crate::traits::{Handler, State, Input, RTCConnection};
use crate::socket::{SocketRequest, SocketRequestPayload, SocketResponse};


// Modules --------------------------------------------------------------------
mod connection;
mod message;


// Internal Dependencies ------------------------------------------------------
use self::connection::PeerConnection;
pub use self::message::PeerMessage;


// Internal Types -------------------------------------------------------------
#[derive(Debug, Eq, PartialEq)]
enum PeeringMode {
    Inactive,
    Offer,
    Answer
}


// Peer Implementation --------------------------------------------------------
#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq, Ord, PartialOrd, Serialize, Deserialize)]
pub struct PeerID(pub(crate) u16);

impl fmt::Display for PeerID {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

pub struct Peer<H: Handler> {
    id: PeerID,
    index: u32,
    seed: u32,

    is_local: bool,
    is_ready: bool,
    is_starting: bool,
    tick_duration: f64,
    receive_last_time: f64,
    timed_out: bool,

    peering_started: f64,
    peering_last_time: f64,
    peering_mode: PeeringMode,
    peering_offers: Vec<String>,
    peering_answers: Vec<String>,
    peering_count: u32,

    input: PeerInput<<H::State as State>::Input>,
    hash: PeerHash,
    sync: PeerSync,
    connection: PeerConnection,
    state_config_hash: Option<u64>,

    pub(crate) rtc: Option<H::Connection>
}

impl<H: Handler> Peer<H> {

    pub fn id(&self) -> PeerID {
        self.id
    }

    pub fn seed(&self) -> u32 {
        self.seed
    }

    pub fn rtt(&self) -> f64 {
        (self.connection.packet_rtt.get() - self.tick_duration).max(0.0)
    }

    pub fn packet_loss(&self) -> f64 {
        self.connection.packet_loss.get()
    }

    pub fn bytes_incoming(&self) -> f64 {
        self.connection.bytes_incoming.get()
    }

    pub fn bytes_outgoing(&self) -> f64 {
        self.connection.bytes_outgoing.get()
    }

    pub fn is_local(&self) -> bool {
        self.is_local
    }

    pub fn is_remote(&self) -> bool {
        !self.is_local()
    }

    pub fn is_connected(&self) -> bool {
        if let Some(ref rtc) = self.rtc {
            rtc.is_connected()

        } else {
            self.is_local()
        }
    }

    pub fn is_ready(&self) -> bool {
        self.is_ready
    }

    pub fn is_starting(&self) -> bool {
        self.is_starting
    }

}


// Crate Internal -------------------------------------------------------------
impl<H: Handler> Peer<H> {

    pub(crate) fn new(id: PeerID, index: u32, seed: u32, is_local: bool, tick_rate: u32, now: f64) -> Self {
        Self {
            id,
            index,
            seed,

            is_local,
            is_ready: false,
            is_starting: false,
            tick_duration: 1000.0 / f64::from(tick_rate),
            receive_last_time: now,
            timed_out: false,

            peering_started: now,
            peering_last_time: 0.0,
            peering_mode: PeeringMode::Inactive,
            peering_offers: Vec::new(),
            peering_answers: Vec::new(),
            peering_count: 0,

            input: PeerInput::new(0),
            hash: PeerHash::new(0),
            sync: PeerSync::new(),
            connection: PeerConnection::new(tick_rate),
            state_config_hash: None,

            rtc: None
        }
    }

    pub(crate) fn offline(tick_rate: u32, now: f64) -> Self {
        let mut peer = Peer::new(
            PeerID(16384),
            0,
            255,
            true,
            tick_rate,
            now
        );
        peer.is_ready = true;
        peer
    }

    pub(crate) fn receive_delay(&self, now: f64) -> f64 {
        if self.is_local() {
            0.0

        } else if self.timed_out {
            1_000_000.0

        } else if self.is_connected() {
            now - self.receive_last_time

        } else {
            0.0
        }
    }

    pub(crate) fn is_present(&self, now: f64, timeout: f64) -> bool {
        self.is_local() || (now - self.receive_last_time < timeout)
    }

    pub(crate) fn start(&mut self) {
        self.is_starting = true;
    }

    pub(crate) fn input(&mut self) -> &mut PeerInput<<H::State as State>::Input> {
        &mut self.input
    }

    pub(crate) fn hash(&mut self) -> &mut PeerHash {
        &mut self.hash
    }

    pub(crate) fn state_config_hash(&self) -> Option<u64> {
        self.state_config_hash
    }

    pub(crate) fn sync(&mut self) -> &mut PeerSync {
        &mut self.sync
    }

    pub(crate) fn sync_ref(&self) -> &PeerSync {
        &self.sync
    }

    pub(crate) fn timeout(&mut self) {
        self.timed_out = true;
    }

    pub(crate) fn disconnect(&mut self) -> bool {
        if self.is_connected() && self.rtc.is_some() {
            self.rtc = None;
            true

        } else {
            false
        }
    }

    // RTC Connection ---------------------------------------------------------
    pub(crate) fn send(&mut self, msg: PeerMessage<<H::State as State>::Input>) {
        if self.is_remote() && self.is_connected() {
            self.connection.send_buffer.append(&mut msg.into_bytes());
        }
    }

    pub(crate) fn receive(&mut self) -> Vec<PeerMessage<<H::State as State>::Input>> {
        let mut messages = Vec::new();
        for received in self.connection.receive_buffer.drain(0..) {
            messages.append(&mut PeerMessage::all_from_bytes(&received));
        }
        messages
    }

    pub(crate) fn tick_receive(&mut self, now: f64) {
        if self.is_remote() && self.is_connected() {
            if let Some(ref mut rtc) = self.rtc {
                if let Some(packets) = rtc.receive() {
                    for p in &packets {
                        // Only update receive times for packages with newer sequence numbers
                        if self.connection.receive(p, now) {
                            self.receive_last_time = now;
                        }
                    }
                }
            }
        }
    }

    pub(crate) fn tick_send(&mut self, now: f64) {
        if self.is_remote() && self.is_connected() {
            if let Some(ref mut rtc) = self.rtc {
                let packet = self.connection.send(now);
                rtc.send(packet);
            }
        }
    }

    pub(crate) fn reset(&mut self, base_frame: u32) {
        self.input = PeerInput::new(base_frame);
        self.hash = PeerHash::new(base_frame);
        self.sync = PeerSync::new();
    }


    // Gateway Responses ------------------------------------------------------
    pub(crate) fn gateway_index(&self) -> u32 {
        self.index
    }

    pub(crate) fn gateway_receive(&mut self, response: SocketResponse, is_ready: bool, now: f64) {

        // Whether this peer is connected to the same subset of peers as the local client
        self.is_ready = is_ready;

        // Update receive time to keep peer present
        self.receive_last_time = now;

        // Update server side index for sorting
        self.index = response.index;

        match (&self.peering_mode, response.request.payload) {
            (PeeringMode::Answer, SocketRequestPayload::PeeringOffer(ref signals)) => if !signals.is_empty() {
                self.gateway_signal_offer(signals);
            },
            (PeeringMode::Offer, SocketRequestPayload::PeeringAnswer(ref signals)) => if !signals.is_empty() {
                self.gateway_signal_answer(signals);
            },
            (_, SocketRequestPayload::Advertise(ref config)) => {
                // Ignore mal-formed configuration
                if let Some(hash) = config.hash_value::<H::State>() {
                    self.state_config_hash = Some(hash);
                }
            },
            _ => {}
        }

    }


    // RTC Peering ------------------------------------------------------------
    pub(crate) fn rtc_initialize(&mut self, local_peer_seed: u32, handler_version: u32, now: f64) -> Option<SocketRequest> {
        if self.is_connected() {
            None

        } else {
            // Decide whether the local peer should make an offer to or accept one from this peer
            let peering_mode = if local_peer_seed >= self.seed { PeeringMode::Offer } else { PeeringMode::Answer };

            // Detect chances in the peer mode and update
            if peering_mode != self.peering_mode {

                // Reset peering
                self.is_ready = false;
                self.peering_started = now;
                self.peering_last_time = 0.0;
                self.peering_answers.clear();
                self.peering_offers.clear();
                self.peering_mode = peering_mode;
                self.peering_count += 1;
                self.connection.reset();

                // Create RTCConnection
                self.rtc = Some(H::Connection::new(
                    format!("{}#{}", self.id.to_string(), self.peering_count),
                    self.peering_mode == PeeringMode::Offer
                ));

            }

            // Update RTCConnection
            let mut disconnected = false;
            let result = if let Some(ref mut rtc) = self.rtc {

                // Check for actual disconnect
                disconnected = rtc.was_disconnected();

                // Also disconnect in cases where ICE gets confused
                if now - self.peering_started >= 2500.0 || self.peering_offers.len() > 10 || self.peering_answers.len() > 10 {
                    disconnected = true;
                }

                // Limit peering signal rate
                if now - self.peering_last_time < 500.0 {
                    None

                // Collected generated peering signals
                } else {
                    if let Some(mut signals) = rtc.signals() {
                        match self.peering_mode {
                            PeeringMode::Offer => self.peering_offers.append(&mut signals),
                            PeeringMode::Answer => self.peering_answers.append(&mut signals),
                            _ => {}
                        }
                    }

                    // Send generated peering offers / answers
                    if self.peering_mode == PeeringMode::Offer && !self.peering_offers.is_empty() {
                        self.peering_last_time = now;
                        Some(SocketRequest {
                            to: Some(self.id),
                            peers: Vec::new(),
                            version: handler_version,
                            payload: SocketRequestPayload::PeeringOffer(self.peering_offers.to_vec())
                        })

                    } else if self.peering_mode == PeeringMode::Answer && !self.peering_answers.is_empty() {
                        self.peering_last_time = now;
                        Some(SocketRequest {
                            to: Some(self.id),
                            peers: Vec::new(),
                            version: handler_version,
                            payload: SocketRequestPayload::PeeringAnswer(self.peering_answers.to_vec())
                        })

                    } else {
                        None
                    }
                }

            } else {
                None
            };

            // Clean up state in case RTCConnection gets lost
            if disconnected {
                self.peering_mode = PeeringMode::Inactive;
                self.rtc = None;
            }

            result

        }
    }

    fn gateway_signal_offer(&mut self, received_offers: &[String]) {
        if self.peering_offers.is_empty() {

            // Wait for initial offer
            let initial_signal = received_offers[0].clone();
            self.peering_offers.push(initial_signal.clone());

            if let Some(ref mut rtc) = self.rtc {
                rtc.signal(initial_signal);
            }

        } else {
            // Support extension of the offer
            if received_offers.len() > self.peering_offers.len() {
                let ext_offer = received_offers[self.peering_offers.len()].clone();
                self.peering_offers.push(ext_offer.clone());

                if let Some(ref mut rtc) = self.rtc {
                    rtc.signal(ext_offer);
                }
            }
        }
    }

    fn gateway_signal_answer(&mut self, received_answers: &[String]) {
        if self.peering_answers.is_empty() {

            // Wait for initial answer
            let initial_signal = received_answers[0].clone();
            self.peering_answers.push(initial_signal.clone());

            if let Some(ref mut rtc) = self.rtc {
                rtc.signal(initial_signal);
            }

        } else {
            // Support extension of the answer
            if received_answers.len() > self.peering_answers.len() {
                let ext_answer = received_answers[self.peering_answers.len()].clone();
                self.peering_answers.push(ext_answer.clone());

                if let Some(ref mut rtc) = self.rtc {
                    rtc.signal(ext_answer);
                }
            }
        }
    }

}

pub(crate) struct PeerInput<I: Input> {
    pub received: Vec<(u32, Option<I>)>,
    pub requested_frame_by_remote: u32,
    pub requested_frame_by_local: u32
}

impl<I: Input> PeerInput<I> {
    fn new(base_frame: u32) -> Self {
        Self {
            received: Vec::new(),
            requested_frame_by_remote: base_frame,
            requested_frame_by_local: base_frame
        }
    }
}

pub(crate) struct PeerHash {
    pub received: Vec<(u32, u64)>,
    pub requested_frame_by_remote: u32,
    pub requested_frame_by_local: u32
}

impl PeerHash {
    fn new(base_frame: u32) -> Self {
        Self {
            received: Vec::new(),
            requested_frame_by_remote: base_frame,
            requested_frame_by_local: base_frame
        }
    }
}

pub(crate) struct PeerSync {
    pub latest_sync_frame: Option<u32>
}

impl PeerSync {
    fn new() -> Self {
        Self {
            latest_sync_frame: None
        }
    }
}

