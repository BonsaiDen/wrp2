// External Dependencies ------------------------------------------------------
use serde_derive::{Deserialize, Serialize};


// Internal Dependencies ------------------------------------------------------
use crate::{PeerID, Session, State, StateConfig};


// Structs --------------------------------------------------------------------
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct SocketResponse {
    pub local: bool,
    pub from: PeerID,
    pub index: u32,
    pub seed: u32,
    pub request: SocketRequest
}

impl SocketResponse {
    pub fn from_str(text: &str) -> Result<SocketResponse, ()> {
        serde_json::from_str::<SocketResponse>(&text).map_err(|_| ())
    }

    pub fn to_string(&self) -> String {
        serde_json::to_string(self).unwrap_or_else(|_| "".to_string())
    }
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct SocketRequest {
    pub to: Option<PeerID>,
    pub peers: Vec<PeerID>,
    pub version: u32,
    pub payload: SocketRequestPayload
}

impl SocketRequest {
    pub fn from_str(text: &str) -> Result<SocketRequest, ()> {
        serde_json::from_str::<SocketRequest>(&text).map_err(|_| ())
    }

    pub fn to_string(&self) -> String {
        serde_json::to_string(self).unwrap_or_else(|_| "".to_string())
    }
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum SocketRequestPayload {
    Advertise(SocketRequestConfig),
    ConfigRequest(SocketRequestConfig),
    PeeringOffer(Vec<String>),
    PeeringAnswer(Vec<String>)
}

// The actual config is kept as a string in order to allow the session
// server to be used across multiple handler implementations
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct SocketRequestConfig(pub String);

impl SocketRequestConfig {

    pub fn hash_value<S: State>(&self) -> Option<u64> {
        serde_json::from_str::<S::Config>(&self.0).ok().map(|c| c.hash_value())
    }

    pub fn to_inner<S: State>(&self) -> Option<(u64, S::Config)> {
        serde_json::from_str::<S::Config>(&self.0).ok().map(|c| (c.hash_value(), c))
    }

    pub fn from_session<S: State>(session: &Session<S>) -> Self {
        SocketRequestConfig(serde_json::to_string(&session.state_config).unwrap_or_else(|_| "".to_string()))
    }

    pub fn from_config<S: State>(config: S::Config) -> Self {
        SocketRequestConfig(serde_json::to_string(&config).unwrap_or_else(|_| "".to_string()))
    }
}

