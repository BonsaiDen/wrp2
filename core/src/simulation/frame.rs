// Internal Dependencies ------------------------------------------------------
use crate::PeerID;
use crate::traits::Input;
use crate::simulation::input::PeerInput;


// Internal Types -------------------------------------------------------------
pub type FrameID = u32;

#[derive(Debug, PartialEq, Eq)]
pub enum FrameInputResult {
    Predicted,
    Mismatch
}


// Simulation Frame Implementation --------------------------------------------
#[derive(Debug)]
pub struct SimulationFrame<I: Input> {
    id: FrameID,
    inputs: Vec<(PeerID, PeerInput<I>)>
}

impl<I: Input> SimulationFrame<I> {

    pub fn id(&self) -> FrameID {
        self.id
    }

    pub fn inputs(&self) -> impl Iterator<Item=(&PeerID, Option<&I>)> {
        self.inputs.iter().map(|(t, i)| (t, i.inner()))
    }

    pub(in crate::simulation) fn new(id: FrameID, ids: Vec<PeerID>) -> Self {
        SimulationFrame {
            id,
            inputs: ids.into_iter().map(|t| (t, PeerInput::default()) ).collect()
        }
    }

    pub(in crate::simulation) fn is_complete(&self) -> bool {
        self.inputs.iter().all(|(_, i)| i.is_received())
    }

    pub(in crate::simulation) fn input(&mut self, id: PeerID, input: PeerInput<I>) -> FrameInputResult {

        if self.is_complete() {
            return FrameInputResult::Predicted;
        }

        let mut mismatch = false;
        for (t, i) in &mut self.inputs {
            if *t == id {
                // Replace existing predicted inputs with local / remote ones
                if !i.is_received() {
                    // Record any mis-match
                    if !i.cmp(&input) {
                        mismatch = true;
                    }
                    *i = input;
                }
                break;
            }
        }

        if mismatch {
            FrameInputResult::Mismatch

        } else {
            FrameInputResult::Predicted
        }

    }

    pub(in crate::simulation) fn correct_inputs(&mut self, previous: &[(PeerID, PeerInput<I>)]) {
        for ((_, current), (_, prev)) in self.inputs.iter_mut().zip(previous.iter()) {
            if !current.is_received() {
                *current = prev.predict();
            }
        }
    }

    pub(in crate::simulation) fn missing_inputs(&self) -> Vec<(FrameID, PeerID)> {
        self.inputs.iter().filter(|(_, i)| !i.is_received()).map(|(t, _)| (self.id, *t)).collect()
    }

    pub(in crate::simulation) fn raw_inputs(&self) -> Vec<(PeerID, PeerInput<I>)> {
        self.inputs.clone()
    }

    pub(in crate::simulation) fn next(&self) -> Self {
        SimulationFrame {
            id: self.id + 1,
            inputs: self.inputs.iter().map(|(t, i)| {
                (*t, i.predict())

            }).collect()
        }
    }

}

