// STD Dependencies -----------------------------------------------------------
use std::collections::VecDeque;


// Internal Dependencies ------------------------------------------------------
use crate::{Event, PeerID};
use crate::traits::{Handler, State};


// Modules --------------------------------------------------------------------
#[cfg(test)]
mod test;

mod frame;
mod input;
use self::{frame::FrameInputResult, input::PeerInput};
pub use self::{frame::FrameID, frame::SimulationFrame, input::SimulationInput};


// Types ----------------------------------------------------------------------
type PendingInput<S> = (SimulationInput<<S as State>::Input>, bool);

type CorrectedInput<S> = (PeerID, PeerInput<<S as State>::Input>);

type StateFrame<S> = (FrameID, S);


// Simulation Implementation --------------------------------------------------
#[derive(Debug)]
pub struct Simulation<S: State> {
    frames: VecDeque<SimulationFrame<S::Input>>,
    states: VecDeque<S>,
    inputs: Vec<PendingInput<S>>,
    buffer_size: usize
}

impl<S: State> Simulation<S> {

    pub fn new(buffer_size: usize) -> Simulation<S> {
        Simulation {
            frames: VecDeque::new(),
            states: VecDeque::new(),
            inputs: Vec::new(),
            buffer_size
        }
    }

    pub fn init(&mut self, base_frame: FrameID, state: S, ids: Vec<PeerID>) {
        self.frames.clear();
        self.frames.push_back(SimulationFrame::<S::Input>::new(base_frame, ids));
        self.states.clear();
        self.states.push_back(state);
        self.inputs.clear();
    }

    pub fn state(&self) -> S {
        self.states.back().unwrap().clone()
    }

    pub fn inputs(&mut self, inputs: Vec<SimulationInput<S::Input>>) {

        let first_frame = if let Some(f) = self.frames.front() { f.id() } else { 0 };

        // Put received inputs into the internal buffer and sort them by their
        // frame index
        self.inputs.append(&mut inputs.into_iter().filter(|i| {
            // Ignore inputs for frames which are no longer buffered
            i.frame_id() >= first_frame

        }).map(|i| (i, true)).collect());

    }

    pub fn step<H: Handler<State=S>>(
        &mut self,
        handler: &mut H,
        now: f64

    ) -> (Vec<StateFrame<S>>, Vec<StateFrame<S>>) {

        // Feed any received inputs into their corresponding frames
        let mut reverted_frame: Option<FrameID> = None;
        for frame in &mut self.frames {
            for (input, pending) in &mut self.inputs {
                if *pending && frame.id() == input.frame_id() {

                    // Detect when actually received inputs mismatch the previously
                    // made prediction...
                    if let FrameInputResult::Mismatch = frame.input(input.peer_id(), input.inner()) {
                        // ...and remember the lowest frame this occures
                        if reverted_frame.is_none() {
                            reverted_frame = Some(frame.id());
                        }
                    }

                    *pending = false;

                }
            }
        }

        self.inputs.retain(|(_, c)| *c);

        let mut reverted_states = Vec::new();
        if let Some(index) = reverted_frame {

            let mut corrected_state: Option<S> = None;
            let mut corrected_inputs: Option<Vec<CorrectedInput<S>>> = None;

            for (frame, state) in self.frames.iter_mut().zip(self.states.iter_mut()) {

                // Re-predict the current frame's input with the corrected inputs from the previous
                // frame
                if let Some(i) = corrected_inputs.take() {
                    frame.correct_inputs(&i);
                }

                // Rollback the current frame's state, replacing it with the state now predicted
                // from the previous frame
                if let Some(s) = corrected_state.take() {
                    reverted_states.push((frame.id(), std::mem::replace(state, s)));
                }

                if frame.id() >= index {
                    // Calculate the next base state from the updated frame
                    let mut next_state = state.next(frame);

                    // Forward state to handler
                    handler.event(Event::StateApply(frame.id(), &mut next_state), now);

                    corrected_state = Some(next_state);

                    // Remember the corrected inputs so we can use them to correct the inputs
                    // predictions of the next frame
                    corrected_inputs = Some(frame.raw_inputs());
                }
            }

        }

        // Continously drop all frames for which there are no outstanding inputs
        // Do this while we have more than one frame buffered and only until
        // we reach the first incomplete frame.
        let mut completed_states = Vec::new();
        while self.frames.len() > 1 {
            let drop = if let Some(f) = self.frames.front() {
                f.is_complete()

            } else {
                false
            };

            if drop {
                if let Some(frame) = self.frames.pop_front() {
                    if let Some(state) = self.states.pop_front() {
                        completed_states.push((frame.id(), state));
                    }
                }

            } else {
                break;
            }
        }

        if self.frames.len() < self.buffer_size {
            let (frame, mut state) = {
                let (lastest_frame, lastest_state) = (self.frames.back().unwrap(), self.states.back().unwrap());
                (
                    // Next base frame with predicted inputs
                    lastest_frame.next(),

                    // Next base state calculated by from the lastest frame
                    lastest_state.next(lastest_frame)
                )
            };

            // Forward newly created state to handler
            handler.event(Event::StateApply(frame.id(), &mut state), now);

            self.frames.push_back(frame);
            self.states.push_back(state);
        }

        (completed_states, reverted_states)

    }

    pub fn missing_inputs(&self) -> Vec<(FrameID, PeerID)> {
        let mut missing_inputs = Vec::new();
        for f in &self.frames {
            missing_inputs.append(&mut f.missing_inputs());
        }
        missing_inputs
    }

}

