// Internal Dependencies ------------------------------------------------------
use crate::PeerID;
use crate::traits::Input;
use crate::simulation::FrameID;


// Internal Types -------------------------------------------------------------
#[derive(Debug, Clone, Copy)]
pub enum PeerInput<I: Input> {
    Local(I),
    Remote(I),
    Predicted(I),
    Disconnected
}

impl<I: Input> PeerInput<I> {

    pub(in crate::simulation) fn inner(&self) -> Option<&I> {
        match *self {
            PeerInput::Local(ref i) | PeerInput::Remote(ref i) | PeerInput::Predicted(ref i) => Some(i),
            PeerInput::Disconnected => None
        }
    }

    pub(in crate::simulation) fn default() -> Self {
        PeerInput::Predicted(I::predict(None))
    }

    pub(in crate::simulation) fn cmp(&self, other: &Self) -> bool {
        self.inner() == other.inner()
    }

    pub(in crate::simulation) fn is_received(&self) -> bool {
        match *self {
            PeerInput::Local(_) | PeerInput::Remote(_) | PeerInput::Disconnected => true,
            PeerInput::Predicted(_) => false
        }
    }

    pub(in crate::simulation) fn predict(&self) -> Self {
        match *self {
            PeerInput::Local(i) | PeerInput::Remote(i) | PeerInput::Predicted(i) => {
                PeerInput::Predicted(I::predict(Some(i)))
            },
            PeerInput::Disconnected => PeerInput::Disconnected
        }
    }

}


// Simulation Frame Input Implementation --------------------------------------
#[derive(Debug, Copy, Clone)]
pub enum SimulationInput<I: Input> {
    Local(FrameID, PeerID, I),
    Remote(FrameID, PeerID, I),
    Disconnected(FrameID, PeerID)
}

impl<I: Input> SimulationInput<I> {

    pub(in crate::simulation) fn frame_id(&self) -> FrameID {
        match *self {
            SimulationInput::Local(id, _, _) | SimulationInput::Remote(id, _, _) | SimulationInput::Disconnected(id, _) => id
        }
    }

    pub(in crate::simulation) fn peer_id(&self) -> PeerID {
        match self {
            SimulationInput::Local(_, t, _) | SimulationInput::Remote(_, t, _) | SimulationInput::Disconnected(_, t) => *t
        }
    }

    pub(in crate::simulation) fn inner(&self) -> PeerInput<I> {
        match *self {
            SimulationInput::Local(_, _, input) => PeerInput::Local(input),
            SimulationInput::Remote(_, _, input) => PeerInput::Remote(input),
            SimulationInput::Disconnected(_, _) => PeerInput::Disconnected
        }
    }

}

