// Internal Dependencies ------------------------------------------------------
use super::{Simulation, SimulationInput};
use crate::{Config, PeerID};
use crate::test::{TestHandler, TestInput, TestState};


// Tests ----------------------------------------------------------------------
#[test]
fn test_simulation() {

    let mut handler = TestHandler::new(Config::default());
    let mut sim = Simulation::<TestState>::new(30);
    sim.init(0, TestState { id: 0, v: 0 }, vec![PeerID(256)]);

    sim.inputs(vec![
        SimulationInput::Local(1, PeerID(256), TestInput(2)),
        SimulationInput::Local(0, PeerID(256), TestInput(1)),
        SimulationInput::Local(2, PeerID(256), TestInput(3))
    ]);

    // Testing commits
    assert_eq!(sim.step(&mut handler, 1.0), (vec![], vec![]));
    assert_eq!(sim.state(), TestState { id: 1, v: 1 });

    assert_eq!(sim.step(&mut handler, 2.0), (vec![(0, TestState { id: 0, v: 0 })], vec![]));
    assert_eq!(sim.state(), TestState { id: 2, v: 3 });

    assert_eq!(sim.step(&mut handler, 4.0), (vec![(1, TestState { id: 1, v: 1 })], vec![]));
    assert_eq!(sim.state(), TestState { id: 3, v: 6 });

    assert_eq!(sim.step(&mut handler, 8.0), (vec![(2, TestState { id: 2, v: 3 })], vec![]));

    // Predicted inputs
    assert_eq!(sim.state(), TestState { id: 4, v: 9 });

    assert_eq!(sim.step(&mut handler, 16.0), (vec![], vec![]));
    assert_eq!(sim.state(), TestState { id: 5, v: 12 });

    assert_eq!(sim.step(&mut handler, 32.0), (vec![], vec![]));
    assert_eq!(sim.state(), TestState { id: 6, v: 15 });

    // This should trigger a commit for state 3 and 4
    // But rollback everything up until test latest state 6
    sim.inputs(vec![
        SimulationInput::Local(4, PeerID(256), TestInput(7)),
        SimulationInput::Local(3, PeerID(256), TestInput(0))
    ]);

    // Not yet corrected input prediction
    assert_eq!(sim.state(), TestState { id: 6, v: 15 });

    assert_eq!(
        sim.step(&mut handler, 64.0),
        (
            // Newly commited states with correct inputs
            // State 4 is correct since it is based on the inputs of the PREVIOUS states
            // State 5 will include the TestInput(7) from above once it receives its inputs
            vec![(3, TestState { id: 3, v: 6 }), (4, TestState { id: 4, v: 6 })],

            // Rolled back states with old predictions
            vec![(4, TestState { id: 4, v: 9 }), (5, TestState { id: 5, v: 12 }), (6, TestState { id: 6, v: 15 })]
        )
    );
    // value 6 on frame 4, plus 21 (frame 4, 5, 6) gives the value on frame 7
    assert_eq!(sim.state(), TestState { id: 7, v: 27 });

    sim.inputs(vec![
        SimulationInput::Local(6, PeerID(256), TestInput(1)),
        SimulationInput::Local(5, PeerID(256), TestInput(0)),
    ]);
    assert_eq!(sim.step(&mut handler, 128.0), (
        // 5 now correctly report 13 (value 6 on frame 4 + input 7 from frame 4)
        vec![(5, TestState { id: 5, v: 13 }), (6, TestState { id: 6, v: 13 })],
        vec![(6, TestState { id: 6, v: 20 }), (7, TestState { id: 7, v: 27 })]
    ));

    // Predicts the 13 plus 1 of actual inputs from frame 6 + 1 from predicted input of frame 7
    assert_eq!(sim.state(), TestState { id: 8, v: 15 });

    // Test handler forwaring
    assert_eq!(handler.applied, vec![
        (0, TestState { id: 1, v:  1 }, 1.0),
        (1, TestState { id: 1, v:  1 }, 1.0),
        (1, TestState { id: 2, v:  3 }, 2.0),
        (2, TestState { id: 2, v:  3 }, 2.0),
        (2, TestState { id: 3, v:  6 }, 4.0),
        (3, TestState { id: 3, v:  6 }, 4.0),
        (4, TestState { id: 4, v:  9 }, 8.0),
        (5, TestState { id: 5, v: 12 }, 16.0),
        (6, TestState { id: 6, v: 15 }, 32.0),
        (3, TestState { id: 4, v:  6 }, 64.0),
        (4, TestState { id: 5, v: 13 }, 64.0),
        (5, TestState { id: 6, v: 20 }, 64.0),
        (6, TestState { id: 7, v: 27 }, 64.0),
        (7, TestState { id: 7, v: 27 }, 64.0),
        (5, TestState { id: 6, v: 13 }, 128.0),
        (6, TestState { id: 7, v: 14 }, 128.0),
        (7, TestState { id: 8, v: 15 }, 128.0),
        (8, TestState { id: 8, v: 15 }, 128.0)
    ]);

    assert_eq!(handler.committed, vec![], "Comitted state events should be triggered by client");
    assert_eq!(handler.reverted, vec![], "Reverted state events should be triggered by client");

}

#[test]
fn test_simulation_base_frame() {

    let mut handler = TestHandler::new(Config::default());
    let mut sim = Simulation::<TestState>::new(30);

    // Does ignore any old inputs from before the initialization
    sim.inputs(vec![
        SimulationInput::Local(120, PeerID(256), TestInput(2)),
    ]);

    sim.init(120, TestState { id: 0, v: 0 }, vec![PeerID(256)]);

    // Should ignore any inputs below the base frame
    sim.inputs(vec![
        SimulationInput::Local(119, PeerID(256), TestInput(2)),
        SimulationInput::Local(118, PeerID(256), TestInput(2)),
        SimulationInput::Local(117, PeerID(256), TestInput(2))
    ]);

    // Compute frame 121
    assert_eq!(sim.step(&mut handler, 0.0), (vec![], vec![]));
    assert_eq!(sim.state(), TestState { id: 1, v: 0 });

    // Does use inputs starting from the base frame
    sim.inputs(vec![
        SimulationInput::Local(120, PeerID(256), TestInput(2)),
    ]);

    // Compute frame 122
    assert_eq!(sim.step(&mut handler, 0.0), (
        // Base frame does not yet include the result of the input, only the next frame will
        vec![(120, TestState { id: 0, v: 0 })], // frame 120
        // But frame 121 must have been re-rolled due to the mismatch of the predicted input
        vec![(121, TestState { id: 1, v: 0 })]  // frame 121
    ));

    // Frame 122
    assert_eq!(sim.state(), TestState { id: 2, v: 4 }); // frame 122

}

