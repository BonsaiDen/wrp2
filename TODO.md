# Tests

- Serialize PeerMessages in Tests to get rid of all the u8's

# Game

- onbeforeUnload 
    - Trigger disconnect transition?

# Core

- Limit number of players per session 
    - via config check on room connect and game logic that decices to leave?
    - or built this into the server logic? need to "translate" setting into specific command

- Allow to communicate pre-game settings
    - via JSON WebSocket 
        - De-Serialize / Serialize
        - Host advertises current config state
        - Clients can send config change requests to host
            - Host then merges based on game logic (e.g. players can change their own color / settings)
            - Host then sends the updated config in advertise

    - command to change config
    - SessionConfig Trait
    - Hosting::Command::Setting(key, value)
    - Peering::Command::Setting(key, value)

