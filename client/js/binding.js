/*eslint browser: true*/
import game_client from '../Cargo.toml';
import SimplePeer from 'simple-peer';


// Expose Web RTC to WASM -----------------------------------------------------
global.WebRTC = class WebRTC {

    constructor(token, initiator) {

        this.token = token;
        this.rtc = new SimplePeer({
            channelConfig: {
                maxRetransmits: 0,
                ordered: false
            },
            initiator
        });

        console.log("[WebRTCState] Created", token, this.rtc);

        this.rtc.on('signal', (data) => {
            game_client.web_rtc_signal(token, JSON.stringify(data));
        });

        this.rtc.on('data', (buffer) => {
            game_client.web_rtc_data(token, buffer);
        });

        this.rtc.on('connect', () => {
            game_client.web_rtc_connect(token);
        });

        this.rtc.on('close', () => {
            game_client.web_rtc_disconnect(token);
        });

        this.rtc.on('error', () => {
            game_client.web_rtc_disconnect(token);
        });

        global.WebRTC[token] = this;

    }

    send(buffer) {
        try {
            this.rtc.send(buffer);

        } catch(err) {
            console.log("[WebRTCState] Send failed:", err);
            game_client.web_rtc_disconnect(this.token);
        }
    }

    signal(buffer) {
        try {
            this.rtc.signal(buffer);

        } catch(err) {
            console.log("[WebRTCState] Signaling failed:", err);
            game_client.web_rtc_disconnect(this.token);
        }
    }

    drop() {
        console.log("[WebRTCState] Dropped", this.token, this);
        delete global.WebRTC[this.token];
    }

}

global.Input = class Input {

    constructor(elementId) {

        this.element = document.getElementById(elementId);
        this.key_down = (e) => {
            game_client.input_key_down(e.keyCode);
        };
        this.key_up = (e) => {
            game_client.input_key_up(e.keyCode);
        };
        this.mouse_down = (e) => {
            game_client.input_mouse_down(e.button, e.offsetX, e.offsetY);
        };
        this.mouse_up = (e) => {
            game_client.input_mouse_up(e.button, e.offsetX, e.offsetY);
        };
        this.blur = () => {
            game_client.input_blur();
        };

        console.log("[InputState] Created for element:", elementId);
        document.addEventListener('keydown', this.key_down);
        document.addEventListener('keyup', this.key_up);
        this.element.addEventListener('mousedown', this.mouse_down);
        this.element.addEventListener('mouseup', this.mouse_up);
        window.addEventListener('blur', this.blur);
    }

    drop() {
        console.log("[InputState] Dropped");
        document.removeEventListener('keydown', this.key_down);
        document.removeEventListener('keyup', this.key_up);
        this.element.removeEventListener('mousedown', this.mouse_down);
        this.element.removeEventListener('mouseup', this.mouse_up);
        window.removeEventListener('blur', this.blur);
    }

}

global.Canvas = class Canvas {

    constructor(elementId) {
        this.el = document.querySelector('canvas');
        this.el.id = elementId;
        this.context = this.el ? this.el.getContext('2d') : null;
        this.width = this.el.width;
        this.height = this.el.height;
        console.log("[CanvasState] Created for element:", elementId);
    }

    draw(operations) {
        const commands = this.parse(operations);
        commands.forEach((c) => {
            if (this.context[c.t] instanceof Function) {
                if (Array.isArray(c.a)) {
                    if (c.a[c.a.length - 1] === null) {
                        c.a.length -= 1;
                    }
                    this.context[c.t].apply(this.context, c.a);

                } else if (c.a !== undefined) {
                    this.context[c.t](c.a);

                } else {
                    this.context[c.t]();
                }

            } else {
                this.context[c.t] = c.a;
            }
        });
    }

    parse(operations) {
        return JSON.parse(operations);
    }

    drop() {
        console.log("[CanvasState] Dropped", this.id);
    }

};

// Exports --------------------------------------------------------------------
module.exports = game_client;

