(function() {

    const source = new Blob([`
        onmessage = function(e) {
            setTimeout(function() {
                postMessage(null);

            }, e.data);
        };

    `], {
        type: 'text/javascript'
    });

    let timer = {
        id: null,
        callback: null
    };
    document.addEventListener('visibilitychange', () => {
        if (document.hidden && timer.id) {
            global.clearTimeout(timer.id);
            timer.id = null;
            timer.callback();
            timer.callback = null;
        }

    }, false);

    const worker = new Worker(global.URL.createObjectURL(source));
    const originalTimeout = global.setTimeout;
    global.setTimeout = (callback, delay) => {
        if (delay === 0) {
            // Take care of nextTick implementation injected by parcel
            // This is used by simple peer for example
            return originalTimeout(callback, delay);

        } else if (document.hidden) {
            worker.onmessage = () => {
                callback();
            };
            worker.postMessage(delay);

        } else {
            timer.callback = callback;
            timer.id = originalTimeout(callback, delay);
        }
    };

})(window);

