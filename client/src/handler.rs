// External Dependencies ------------------------------------------------------
use stdweb::web::Date;
use core::{
    Config,
    Event,
    Handler,
    Peer,
    phase::{
        Disconnected,
        Connecting,
        Hosting,
        Peering,
        Started,
        Starting,
        ConnectionLost,
        Disconnecting,
        SimulatingOnline,
        SimulatingOffline,
        PausedOffline,
        Synchronizing,
        SynchronisationLost
    }
};


// Internal Dependencies ------------------------------------------------------
use crate::binding::{Canvas, Input, InputState, Key, WebRTC, TextBaseline, TextAlign, WebSocket, WebClient};
use crate::state::{PlayerInput, GameState, GameConfig, PLAYER_COLORS};


// Statics --------------------------------------------------------------------
pub const TICK_RATE: u32 = 30;


// Game Handler ---------------------------------------------------------------
pub struct GameHandler {
    canvas: Canvas,
    input: Input,
    last_tick_state: Option<GameState>
}

impl GameHandler {

    fn tick(&mut self) {
        self.input.update();
    }

    fn draw<D: FnMut(&mut Self)>(&mut self, mut drawer: D) {

        self.canvas.save();
        self.canvas.set_fill_style("#000000");
        self.canvas.fill_rect(0.0, 0.0, self.canvas.width(), self.canvas.height());
        self.canvas.set_text_baseline(TextBaseline::Top);
        self.canvas.set_font("bold 14px/1 \"Courier New\"");

        self.canvas.set_fill_style("#ffffff");
        self.canvas.set_text_align(TextAlign::Right);
        self.canvas.fill_text(format!("Version {}", Self::version()), self.canvas.width() - 10.0, 10.0, None);
        self.canvas.set_text_align(TextAlign::Left);

        drawer(self);

        self.canvas.restore();
        self.canvas.flush();

    }

    fn draw_peers(&mut self, peers: &[Peer<Self>]) {
        for (index, p) in peers.iter().enumerate() {

            if p.is_local() {
                self.canvas.set_global_alpha(1.0);
                self.canvas.set_line_width(2.0);
                self.canvas.set_stroke_style(PLAYER_COLORS[index]);
                self.canvas.stroke_rect(2.0, 2.0, self.canvas.width() - 3.0, self.canvas.height() - 3.0);
            }

            if p.is_connected() {
                self.canvas.set_global_alpha(1.0);

            } else {
                self.canvas.set_global_alpha(0.5);
            }

            if p.is_ready() {
                self.canvas.set_fill_style(PLAYER_COLORS[index]);

            } else {
                self.canvas.set_fill_style("#c0c0c0");
            }

            self.canvas.fill_text(
                format!("Peer {} {:.0}ms {:.1}% {:.1} / {:.1} bps", p.id(), p.rtt() * 0.5, p.packet_loss() * 100.0, p.bytes_incoming(), p.bytes_outgoing()),
                10.0,
                28.0 + index as f32 * 16.0,
                None
            );

        }
    }

}

impl GameHandler {
    pub fn new() -> Self {
        Self {
            canvas: Canvas::new("game"),
            input: Input::new("game"),
            last_tick_state: None
        }
    }
}

impl Handler for GameHandler {

    type State = GameState;
    type Socket = WebSocket;
    type Connection = WebRTC;

    fn log<S: Into<String>>(text: S) {
        console!(log, text.into());
    }

    fn version() -> u32 {
        1
    }

    fn config(&self) -> Config {
        Config {
            tick_rate: TICK_RATE,
            ..Config::default()
        }
    }

    fn now() -> f64 {
        Date::now()
    }

    fn state(&self, config: GameConfig, peers: &[Peer<Self>]) -> GameState {
        GameState::new(self.canvas.width() as u32, self.canvas.height() as u32, config, peers)
    }

    fn event(&mut self, _: Event<Self>, _: f64) where Self: Sized {
        // TODO apply: a sound resource and track it's ID in the state
        // TODO revert: stop/drop all sound resources created by this state
    }

    fn input(&self) -> PlayerInput {
        PlayerInput {
            up: self.input.pressed(Key::KeyW),
            right: self.input.pressed(Key::KeyD),
            down: self.input.pressed(Key::KeyS),
            left: self.input.pressed(Key::KeyA),
            fire: self.input.pressed(Key::Space)
        }
    }

}

// Disconnected ---------------------------------------------------------------
impl Disconnected::Phase for GameHandler {

    fn tick(&mut self, _: &[Peer<Self>], _: f64) -> Option<Disconnected::Command<GameState>> {
        self.tick();
        if self.input.hit(Key::Space) {
            Some(Disconnected::Command::Connect {
                host: WebClient::web_socket_host(),
                session_id: "foo".to_string()
            })

        } else if self.input.hit(Key::Enter) {
            Some(Disconnected::Command::Start(GameConfig::default()))

        } else {
            None
        }
    }

    fn draw(&mut self, _: &[Peer<Self>], _: f64, _: f32) {
        self.draw(|handler| {
            handler.canvas.fill_text("Disconnected - Press Space to Connect or Enter to Start", 10.0, 10.0, None);
        });
    }

}


// Connecting -----------------------------------------------------------------
impl Connecting::Phase<GameState> for GameHandler {
    fn tick(&mut self, _: &Connecting::State<GameState>, _: &[Peer<Self>], _: f64) -> Option<Connecting::Command> {
        self.tick();
        None
    }
    fn draw(&mut self, _: &Connecting::State<GameState>, _: &[Peer<Self>], _: f64, _: f32) {
        self.draw(|handler| {
            handler.canvas.fill_text("Connecting to Server...", 10.0, 10.0, None);
        });
    }
}


// Hosting --------------------------------------------------------------------
impl Hosting::Phase<GameState> for GameHandler {
    fn tick(&mut self, _: &Hosting::State<GameState>, _: &[Peer<Self>], _: f64) -> Option<Hosting::Command<GameState>> {
        self.tick();
        if self.input.hit(Key::Space) {
            Some(Hosting::Command::Start)

        } else {
            None
        }
    }
    fn draw(&mut self, _: &Hosting::State<GameState>, peers: &[Peer<Self>], _: f64, _: f32) {
        self.draw(|handler| {
            handler.canvas.fill_text("Waiting for Peers to Join...", 10.0, 10.0, None);
            handler.draw_peers(peers);
        });
    }
}


// Peering --------------------------------------------------------------------
impl Peering::Phase<GameState> for GameHandler {
    fn tick(&mut self, _: &Peering::State<GameState>, _: &[Peer<Self>], _: f64) -> Option<Peering::Command<GameState>> {
        self.tick();
        None
    }
    fn draw(&mut self, _: &Peering::State<GameState>, peers: &[Peer<Self>], _: f64, _: f32) {
        self.draw(|handler| {
            handler.canvas.fill_text("Waiting for Host to Start...", 10.0, 10.0, None);
            handler.draw_peers(peers);
        });
    }
}


// Started --------------------------------------------------------------------
impl Started::Phase<GameState> for GameHandler {
    fn tick(&mut self, _: &Started::State<GameState>, _: &[Peer<Self>], _: f64) -> Option<Started::Command> {
        self.tick();
        None
    }
    fn draw(&mut self, _: &Started::State<GameState>, peers: &[Peer<Self>], _: f64, _: f32) {
        self.draw(|handler| {
            let ready = peers.iter().filter(|p| p.is_ready()).count();
            handler.canvas.fill_text(format!("Started... ({} / {} peers ready)", ready, peers.len()), 10.0, 10.0, None);
            handler.draw_peers(peers);
        });
    }
}


// Starting -------------------------------------------------------------------
impl Starting::Phase<GameState> for GameHandler {
    fn tick(&mut self, _: &Starting::State<GameState>, _: &[Peer<Self>], _: f64) -> Option<Starting::Command> {
        self.tick();
        None
    }
    fn draw(&mut self, _: &Starting::State<GameState>, peers: &[Peer<Self>], _: f64, _: f32) {
        self.draw(|handler| {
            let ready = peers.iter().filter(|p| p.is_ready()).count();
            handler.canvas.fill_text(format!("Host has started... ({} / {} peers ready)", ready, peers.len()), 10.0, 10.0, None);
            handler.draw_peers(peers);
        });
    }
}


// ConnectionLost -------------------------------------------------------------
impl ConnectionLost::Phase for GameHandler {
    fn tick(&mut self, _: &[Peer<Self>], _: f64) -> Option<ConnectionLost::Command> {
        self.tick();
        if self.input.hit(Key::Space) {
            Some(ConnectionLost::Command::Connect {
                host: WebClient::web_socket_host(),
                session_id: "foo".to_string()
            })

        } else if self.input.hit(Key::Enter) {
            Some(ConnectionLost::Command::Reset)

        } else {
            None
        }
    }
    fn draw(&mut self, _: &[Peer<Self>], _: f64, _: f32) {
        self.draw(|handler| {
            handler.canvas.fill_text("Connection to server lost - Press Space to Reconnect - Enter to Reset", 10.0, 10.0, None);
        });
    }
}


// Disconnecting --------------------------------------------------------------
impl Disconnecting::Phase for GameHandler {
    fn tick(&mut self, _: &[Peer<Self>], _: f64) {
        self.tick();
    }
    fn draw(&mut self, peers: &[Peer<Self>], _: f64, _: f32) {
        self.draw(|handler| {
            handler.canvas.fill_text("Disconnecting...", 10.0, 10.0, None);
            handler.draw_peers(peers);
        });
    }
}


// Simulating -----------------------------------------------------------------
impl SimulatingOnline::Phase<GameState> for GameHandler {
    fn tick(&mut self, phase: &SimulatingOnline::State<GameState>, _: &[Peer<Self>], _: f64) -> Option<SimulatingOnline::Command> {
        self.tick();
        self.last_tick_state = Some(phase.state.clone());
        None
    }
    fn draw(&mut self, phase: &SimulatingOnline::State<GameState>, peers: &[Peer<Self>], _: f64, t: f32) {
        self.draw(|handler| {
            phase.state.draw(&handler.last_tick_state.as_ref().unwrap_or(&phase.state), &mut handler.canvas, t);
            handler.canvas.fill_text(
                format!(
                    "Simulating Frame {} / {} (input advantage {:.2}, speed {:.4})",
                    phase.frame.0,
                    phase.frame.1,
                    phase.frame_advantage,
                    phase.frame_speed
                ),
                10.0, 10.0, None
            );
            handler.draw_peers(peers);
        });
    }
}

impl SimulatingOffline::Phase<GameState> for GameHandler {
    fn tick(&mut self, phase: &SimulatingOffline::State<GameState>, _: &[Peer<Self>], _: f64) -> Option<SimulatingOffline::Command> {
        // TODO pause / stop commands
        self.tick();
        self.last_tick_state = Some(phase.state.clone());
        None
    }
    fn draw(&mut self, phase: &SimulatingOffline::State<GameState>, _: &[Peer<Self>], _: f64, t: f32) {
        self.draw(|handler| {
            phase.state.draw(&handler.last_tick_state.as_ref().unwrap_or(&phase.state), &mut handler.canvas, t);
            handler.canvas.fill_text(
                format!(
                    "Simulating Frame {}",
                    phase.frame.0
                ),
                10.0, 10.0, None
            );
        });
    }
}

impl PausedOffline::Phase<GameState> for GameHandler {
    fn tick(&mut self, phase: &PausedOffline::State<GameState>, _: &[Peer<Self>], _: f64) -> Option<PausedOffline::Command> {
        // TODO resume / stop commands
        self.tick();
        self.last_tick_state = Some(phase.state.clone());
        None
    }
    fn draw(&mut self, phase: &PausedOffline::State<GameState>, _: &[Peer<Self>], _: f64, t: f32) {
        self.draw(|handler| {
            phase.state.draw(&handler.last_tick_state.as_ref().unwrap_or(&phase.state), &mut handler.canvas, t);
            handler.canvas.fill_text(
                format!(
                    "Paused at Frame {}",
                    phase.frame.0
                ),
                10.0, 10.0, None
            );
        });
    }
}


// Synchronizing --------------------------------------------------------------
impl Synchronizing::Phase for GameHandler {
    fn tick(&mut self, _: &Synchronizing::State, _: &[Peer<Self>], _: f64) {
        self.tick();
    }
    fn draw(&mut self, _: &Synchronizing::State, peers: &[Peer<Self>], _: f64, _: f32) {
        self.draw(|handler| {
            handler.canvas.fill_text("Synchronizing state with other peers...", 10.0, 10.0, None);
            handler.draw_peers(peers);
        });
    }
}


// SynchronisationLost --------------------------------------------------------
impl SynchronisationLost::Phase for GameHandler {
    fn tick(&mut self, _: &SynchronisationLost::State, _: &[Peer<Self>], _: f64) -> Option<SynchronisationLost::Command> {
        self.tick();
        None
    }
    fn draw(&mut self, phase: &SynchronisationLost::State, peers: &[Peer<Self>], _: f64, _: f32) {
        self.draw(|handler| {
            handler.canvas.fill_text(format!("DE-SYNC @ frame {} reason: {:?}", phase.frame, phase.reason), 10.0, 10.0, None);
            handler.draw_peers(peers);
        });
    }
}

