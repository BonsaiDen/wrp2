// STD Dependencies -----------------------------------------------------------
use std::hash::{Hash, Hasher};


// External Dependencies ------------------------------------------------------
use seahash::SeaHasher;
use core::{Peer, State, StateConfig, PeerID, Frame};
use serde_derive::{Deserialize, Serialize};
use crate::handler::{GameHandler, TICK_RATE};
use crate::binding::{Canvas, LineJoin};


// Modules --------------------------------------------------------------------
mod input;
pub use self::input::PlayerInput;


// Game Config ----------------------------------------------------------------
#[derive(Debug, Clone, Hash, Default, PartialEq, Serialize, Deserialize)]
pub struct GameConfig {

}
impl StateConfig for GameConfig {
    fn merge(&self, _: Self, _: PeerID) -> Self {
        self.clone()
    }
}


// Game State -----------------------------------------------------------------
#[derive(Debug, Clone, Hash)]
pub struct GameState {
    width: u32,
    height: u32,
    border: u32,
    players: Vec<Player>
}

impl GameState {
    pub fn new(width: u32, height: u32, _: GameConfig, peers: &[Peer<GameHandler>]) -> Self {
        // TODO use seed from host to initialize rng?
        Self {
            width,
            height,
            border: 24,
            players: peers.iter().enumerate().map(|(index, p)| {
                Player {
                    id: p.id(),
                    ship: Some(Ship::new(
                        index as u8,
                        width as f32 / 2.0,
                        height as f32 / 2.0
                    ))
                }

            }).collect()
        }
    }

    pub fn draw(&self, previous: &Self, canvas: &mut Canvas, t: f32) {
        for (player, prev_player) in self.players.iter().zip(previous.players.iter()) {
            if let (Some(ship), Some(prev_ship)) = (&player.ship, &prev_player.ship) {
                ship.draw(&prev_ship, &self, canvas, t);
            }
        }
    }
}

impl State for GameState {

    type Input = PlayerInput;
    type Config = GameConfig;

    fn next(&self, frame: &Frame<PlayerInput>) -> Self {
        let players = frame.inputs().zip(&self.players).map(|((_, input), player)| {
            player.next(self, input)

        }).collect();

        Self {
            width: self.width,
            height: self.height,
            border: self.border,
            players
        }
    }

    fn hash_value(&self) -> u64 {
        let mut hasher = SeaHasher::new();
        self.hash(&mut hasher);
        hasher.finish()
    }

}

// Stuff ----------------------------------------------------------------------
use ordered_float::OrderedFloat;

pub const PLAYER_COLORS: [&str; 8] = [
    "#f20000",
    "#fd831c",
    "#fdda31",
    "#3cdc00",
    "#33d0d1",
    "#0f5cf9",
    "#820ce6",
    "#ec34a7"
];

#[derive(Debug, Clone, Hash, PartialEq)]
struct Player {
    id: PeerID,
    ship: Option<Ship>
}

impl Player {

    fn next(&self, state: &GameState, input: Option<&PlayerInput>) -> Self {
        Self {
            id: self.id,
            ship: self.ship.as_ref().map(|s| s.next(state, input))
        }
    }

}

#[derive(Debug, Clone, Hash, PartialEq)]
struct Ship {
    color: u8,
    x: OrderedFloat<f32>,
    y: OrderedFloat<f32>,
    mx: OrderedFloat<f32>,
    my: OrderedFloat<f32>,
    r: OrderedFloat<f32>
}

const SHIP_MAX_SPEED: f32 = 130.0;
const SHIP_ACCELERATION: f32 = 3.0;
const SHIP_ROTATION: f32 = 170.0;

impl Ship {
    fn new(color: u8, x: f32, y: f32) -> Self {
        Ship {
            color,
            x: OrderedFloat(x),
            y: OrderedFloat(y),
            mx: OrderedFloat(0.0),
            my: OrderedFloat(0.0),
            r: OrderedFloat(0.0)
        }
    }

    fn next(&self, state: &GameState, input: Option<&PlayerInput>) -> Self {
        if let Some(input) = input {

            let dt = 1.0 / TICK_RATE as f32;

            let mut r = self.r.into_inner();
            let mut x = self.x.into_inner();
            let mut y = self.y.into_inner();
            let mut mx = self.mx.into_inner();
            let mut my = self.my.into_inner();

            // Steering
            let mut steer = 0.0;
            if input.left {
                steer -= 1.0;
            }

            if input.right {
                steer += 1.0;
            }

            r += std::f32::consts::PI / 180.0 * SHIP_ROTATION * (steer / (1.0 / dt));

            // Acceleration
            if input.up {
                // Constant time acceleration
                let m = 60.0 / (1.0 / dt);
                mx += r.cos() * SHIP_ACCELERATION * dt * m;
                my += r.sin() * SHIP_ACCELERATION * dt * m;
            }

            // Limit diagonal speed
            let mr = my.atan2(mx);
            let mut s = ((mx * mx) + (my * my)).sqrt();

            // Allow for easier full stop
            if s < 0.15 {
                s *= 0.95;
            }

            // Limit max speed
            mx = mr.cos() * s.min(SHIP_MAX_SPEED * dt);
            my = mr.sin() * s.min(SHIP_MAX_SPEED * dt);
            x += mx;
            y += my;

            let width = (state.width + state.border * 2) as f32;
            if x < 0.0 {
                x += width;

            } else if x >= width {
                x -= width;
            }

            let height = (state.height + state.border * 2) as f32;
            if y < 0.0 {
                y += height;

            } else if y >= height {
                y -= height;
            }

            Ship {
                color: self.color,
                x: OrderedFloat(x),
                y: OrderedFloat(y),
                mx: OrderedFloat(mx),
                my: OrderedFloat(my),
                r: OrderedFloat(r)
            }

        } else {
            self.clone()
        }
    }

    fn draw(&self, prev: &Self, state: &GameState, canvas: &mut Canvas, t: f32) {

        let (x, last_x) = (self.x.into_inner(), prev.x.into_inner());
        let (y, last_y) = (self.y.into_inner(), prev.y.into_inner());
        let (r, last_r) = (self.r.into_inner(), prev.r.into_inner());
        let mr = r - last_r;

        // Skip interpolation if distance is too large too avoid glitching
        // when wrapping at the level boundaries occurs
        let x = if (last_x - x).abs() < state.width as f32 * 0.5 {
            last_x * (1.0 - t) + x * t

        } else {
            x
        };

        let y = if (last_y - y).abs() < state.height as f32 * 0.5 {
            last_y * (1.0 - t) + y * t

        } else {
            y
        };

        canvas.save();
        canvas.set_line_join(LineJoin::Miter);
        canvas.set_line_width(2.0);
        canvas.set_stroke_style(PLAYER_COLORS[self.color as usize]);
        canvas.translate(x - state.border as f32, y - state.border as f32);
        canvas.rotate((last_r + mr.sin().atan2(mr.cos()) * t) % (std::f32::consts::PI * 2.0));
        canvas.begin_path();
        canvas.move_to(12.0, 0.0);
        canvas.line_to(-12.0, 9.0);
        canvas.line_to(-12.0, -9.0);
        canvas.line_to(12.0, 0.0);
        canvas.close_path();
        canvas.stroke();
        canvas.restore();

    }

}

