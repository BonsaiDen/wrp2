// External Dependencies ------------------------------------------------------
use core::Input;


// Player Input Implementation ------------------------------------------------
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct PlayerInput {
    pub up: bool,
    pub right: bool,
    pub down: bool,
    pub left: bool,
    pub fire: bool
}

impl Input for PlayerInput {

    fn predict(previous: Option<PlayerInput>) -> PlayerInput {
        match previous {
            Some(i) => i,
            _ => PlayerInput {
                up: false,
                right: false,
                down: false,
                left: false,
                fire: false
            }
        }
    }

    fn into_byte(self) -> u8 {
        let mut b = 0;
        if self.up {
            b |= 1;
        }
        if self.right {
            b |= 2;
        }
        if self.down {
            b |= 4;
        }
        if self.left {
            b |= 8;
        }
        if self.fire {
            b |= 16;
        }
        b
    }

    fn from_byte(byte: u8) -> Self {
        PlayerInput {
            up: (byte & 1) == 1,
            right: (byte & 2) == 2,
            down: (byte & 4) == 4,
            left: (byte & 8) == 8,
            fire: (byte & 16) == 16
        }
    }

}

