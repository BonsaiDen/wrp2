// Features -------------------------------------------------------------------
#![feature(custom_attribute)]
#![allow(unused_attributes)]
#![allow(unused_macros)]


// Crates ---------------------------------------------------------------------
#[macro_use] extern crate stdweb;


// Modules --------------------------------------------------------------------
mod binding;
mod handler;
mod state;


// WASM Interface -------------------------------------------------------------
#[js_export]
pub fn init() {
    binding::WebClient::init();
}

