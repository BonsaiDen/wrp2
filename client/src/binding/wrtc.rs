// STD Dependencies -----------------------------------------------------------
use std::cell::RefCell;
use std::collections::{HashMap, VecDeque};


// External Dependencies ------------------------------------------------------
use core::RTCConnection;
use stdweb::web::TypedArray;


// Statics --------------------------------------------------------------------
thread_local! {
    static WEB_RTC_CONNECTIONS: RefCell<HashMap<String, WebRTCConnection>> = RefCell::new(HashMap::new());
}


// Web RTC Binding ------------------------------------------------------------
struct WebRTCConnection {
    has_connected: bool,
    was_disconnected: bool,
    signals: VecDeque<String>,
    data: VecDeque<Vec<u8>>
}

impl WebRTCConnection {
    fn new() -> Self {
        Self {
            has_connected: false,
            was_disconnected: false,
            signals: VecDeque::new(),
            data: VecDeque::new()
        }
    }
}

pub struct WebRTC {
    token: String
}

impl RTCConnection for WebRTC {
    fn new(token: String, initiator: bool) -> Self {

        let id = token.clone();
        js! {
            new global.WebRTC(@{id}, @{initiator});
        };

        WEB_RTC_CONNECTIONS.with(|c| {
            c.borrow_mut().insert(token.clone(), WebRTCConnection::new())
        });

        Self {
            token
        }
    }

    fn send(&mut self, bytes: Vec<u8>) {
        if self.is_connected() {
            let id = self.token.clone();
            js! {
                const token = @{id};
                const view = Uint8Array.from(@{bytes});
                if (global.WebRTC.hasOwnProperty(token)) {
                    global.WebRTC[token].send(view);
                }
            };
        }
    }

    fn signal(&mut self, signal: String) {
        WEB_RTC_CONNECTIONS.with(|c| {
            if c.borrow().contains_key(&self.token) {
                let id = self.token.clone();
                js! {
                    const token = @{id};
                    if (global.WebRTC.hasOwnProperty(token)) {
                        global.WebRTC[token].signal(@{signal});
                    }
                };
            }
        });
    }

    fn is_connected(&self) -> bool {
        WEB_RTC_CONNECTIONS.try_with(|c| {
            if let Some(conn) = c.borrow_mut().get_mut(&self.token) {
                conn.has_connected

            } else {
                false
            }

        }).unwrap_or(false)
    }

    fn was_disconnected(&self) -> bool {
        WEB_RTC_CONNECTIONS.try_with(|c| {
            if let Some(conn) = c.borrow_mut().get_mut(&self.token) {
                conn.was_disconnected

            } else {
                true
            }

        }).unwrap_or(true)
    }

    fn signals(&mut self) -> Option<Vec<String>> {
        WEB_RTC_CONNECTIONS.try_with(|c| {
            if let Some(conn) = c.borrow_mut().get_mut(&self.token) {
                Some(conn.signals.drain(0..).collect())

            } else {
                None
            }

        }).unwrap_or(None)
    }

    fn receive(&mut self) -> Option<Vec<Vec<u8>>> {
        WEB_RTC_CONNECTIONS.try_with(|c| {
            if let Some(conn) = c.borrow_mut().get_mut(&self.token) {
                Some(conn.data.drain(0..).collect())

            } else {
                None
            }

        }).unwrap_or(None)
    }

}

impl Drop for WebRTC {
    fn drop(&mut self) {
        WEB_RTC_CONNECTIONS.with(|c| {
            if c.borrow_mut().remove(&self.token).is_some() {
                let id = self.token.clone();
                js! {
                    const token = @{id};
                    if (global.WebRTC.hasOwnProperty(token)) {
                        global.WebRTC[token].drop();
                    }
                }
            }
        });
    }
}


// JS API ---------------------------------------------------------------------
#[allow(dead_code)]
#[js_export]
pub fn web_rtc_connect(token: &str) {
    WEB_RTC_CONNECTIONS.with(|c| {
        if let Some(conn) = c.borrow_mut().get_mut(token) {
            console!(log, "[WebRTCConnection] Connection established", token);
            conn.has_connected = true;
        }
    });
}

#[allow(dead_code)]
#[js_export]
pub fn web_rtc_disconnect(token: &str) {
    WEB_RTC_CONNECTIONS.with(|c| {
        if let Some(conn) = c.borrow_mut().get_mut(token) {
            console!(log, "[WebRTCConnection] Connection lost", token);
            conn.was_disconnected = true;
        }
    })
}

#[allow(dead_code)]
#[js_export]
pub fn web_rtc_signal(token: &str, signal: String) {
    WEB_RTC_CONNECTIONS.with(|c| {
        if let Some(conn) = c.borrow_mut().get_mut(token) {
            conn.signals.push_back(signal);
        }
    });
}

#[allow(dead_code)]
#[js_export]
pub fn web_rtc_data(token: &str, buffer: TypedArray<u8>) {
    WEB_RTC_CONNECTIONS.with(|c| {
        if let Some(conn) = c.borrow_mut().get_mut(token) {
            conn.data.push_back(buffer.into());
        }
    });
}

