// External Dependencies ------------------------------------------------------
use serde_derive::Serialize;
use stdweb::{Value, unstable::TryInto};


// Canvas Abstraction ---------------------------------------------------------
#[derive(Debug, Serialize)]
#[serde(tag="t", content="a")]
pub enum CanvasOperation {
    #[serde(rename="save")]
    Save,
    #[serde(rename="restore")]
    Restore,
    #[serde(rename="scale")]
    Scale(f32, f32),
    #[serde(rename="rotate")]
    Rotate(f32),
    #[serde(rename="translate")]
    Translate(f32, f32),
    #[serde(rename="transform")]
    Transform(f32, f32, f32, f32, f32, f32),
    #[serde(rename="setTransform")]
    SetTransform(f32, f32, f32, f32, f32, f32),
    #[serde(rename="clearRect")]
    ClearRect(f32, f32, f32, f32),
    #[serde(rename="fillRect")]
    FillRect(f32, f32, f32, f32),
    #[serde(rename="strokeRect")]
    StrokeRect(f32, f32, f32, f32),
    #[serde(rename="beginPath")]
    BeginPath,
    #[serde(rename="fill")]
    Fill,
    #[serde(rename="stroke")]
    Stroke,
    #[serde(rename="clip")]
    Clip,
    #[serde(rename="fillText")]
    FillText(String, f32, f32, Option<f32>),
    #[serde(rename="strokeText")]
    StrokeText(String, f32, f32, Option<f32>),
    #[serde(rename="setLineDash")]
    SetLineDash(Vec<f32>),
    #[serde(rename="closePath")]
    ClosePath,
    #[serde(rename="moveTo")]
    MoveTo(f32, f32),
    #[serde(rename="lineTo")]
    LineTo(f32, f32),
    #[serde(rename="quadraticCurveTo")]
    QuadraticCurveTo(f32, f32, f32, f32),
    #[serde(rename="bezierCurveTo")]
    BezierCurveTo(f32, f32, f32, f32, f32, f32),
    #[serde(rename="arcTo")]
    ArcTo(f32, f32, f32, f32, f32),
    #[serde(rename="rect")]
    Rect(f32, f32, f32, f32),
    #[serde(rename="arc")]
    Arc(f32, f32, f32, f32, f32, Option<bool>),
    #[serde(rename="globalAlpha")]
    GlobalAlpha(f32),
    #[serde(rename="globalCompositeOperation")]
    GlobalCompositeOperation(String),
    #[serde(rename="strokeStyle")]
    StrokeStyle(String),
    #[serde(rename="fillStyle")]
    FillStyle(String),
    #[serde(rename="shadowOffsetX")]
    ShadowOffsetX(f32),
    #[serde(rename="shadowOffsetY")]
    ShadowOffsetY(f32),
    #[serde(rename="shadowBlur")]
    ShadowBlur(f32),
    #[serde(rename="shadowColor")]
    ShadowColor(String),
    #[serde(rename="lineWidth")]
    LineWidth(f32),
    #[serde(rename="lineCap")]
    LineCap(LineCap),
    #[serde(rename="lineJoin")]
    LineJoin(LineJoin),
    #[serde(rename="miterLimit")]
    MiterLimit(f32),
    #[serde(rename="font")]
    Font(String),
    #[serde(rename="textAlign")]
    TextAlign(TextAlign),
    #[serde(rename="textBaseline")]
    TextBaseline(TextBaseline)
}

#[allow(dead_code)]
#[derive(Debug, Copy, Clone, Serialize)]
pub enum LineCap {
    #[serde(rename="butt")]
    Butt,
    #[serde(rename="round")]
    Round,
    #[serde(rename="square")]
    Square
}

#[allow(dead_code)]
#[derive(Debug, Copy, Clone, Serialize)]
pub enum LineJoin {
    #[serde(rename="round")]
    Round,
    #[serde(rename="bevel")]
    Bevel,
    #[serde(rename="miter")]
    Miter
}

#[allow(dead_code)]
#[derive(Debug, Copy, Clone, Serialize)]
pub enum TextAlign {
    #[serde(rename="start")]
    Start,
    #[serde(rename="end")]
    End,
    #[serde(rename="left")]
    Left,
    #[serde(rename="right")]
    Right,
    #[serde(rename="center")]
    Center
}

#[allow(dead_code)]
#[derive(Debug, Copy, Clone, Serialize)]
pub enum TextBaseline {
    #[serde(rename="top")]
    Top,
    #[serde(rename="hanging")]
    Hanging,
    #[serde(rename="middle")]
    Middle,
    #[serde(rename="alphabetic")]
    Alphabetic,
    #[serde(rename="ideographic")]
    Ideographic,
    #[serde(rename="bottom")]
    Bottom
}

pub struct Canvas {
    width: f32,
    height: f32,
    operations: Vec<CanvasOperation>
}

impl Canvas {

    pub fn new(element_id: &str) -> Self {
        let dimensions: Value = js! {
            const c = global.Canvas.Instance = new global.Canvas(@{element_id});
            return [c.width, c.height];
        };

        let dimensions: Vec<f64> = dimensions.try_into().unwrap();
        Self {
            width: *dimensions.get(0).unwrap_or(&0.0) as f32,
            height: *dimensions.get(1).unwrap_or(&0.0) as f32,
            operations: Vec::new()
        }
    }

    pub fn width(&self) -> f32 {
        self.width
    }

    pub fn height(&self) -> f32 {
        self.height
    }

    pub fn flush(&mut self) {
        // TODO faster way of doing this?
        let s = serde_json::to_string(&self.operations).unwrap();
        js! {
            if (global.Canvas.Instance) {
                global.Canvas.Instance.draw(@{s});
            }
        };
        self.operations.clear();
    }

}

#[allow(dead_code)]
impl Canvas {

    // state
    pub fn save(&mut self) {
        self.operations.push(CanvasOperation::Save);
    }

    pub fn restore(&mut self) {
        self.operations.push(CanvasOperation::Restore);
    }


    // transformations (default: transform is the identity matrix)
    pub fn scale(&mut self, x: f32, y: f32) {
        self.operations.push(CanvasOperation::Scale(x, y));
    }

    pub fn rotate(&mut self, angle: f32) {
        self.operations.push(CanvasOperation::Rotate(angle));
    }

    pub fn translate(&mut self, x: f32, y: f32) {
        self.operations.push(CanvasOperation::Translate(x, y));
    }

    #[allow(clippy::many_single_char_names)]
    pub fn transform(&mut self, a: f32, b: f32, c: f32, d: f32, e: f32, f: f32) {
        self.operations.push(CanvasOperation::Transform(a, b, c, d, e, f));
    }

    #[allow(clippy::many_single_char_names)]
    pub fn set_transform(&mut self, a: f32, b: f32, c: f32, d: f32, e: f32, f: f32) {
        self.operations.push(CanvasOperation::SetTransform(a, b, c, d, e, f));
    }


    // compositing
    pub fn set_global_alpha(&mut self, value: f32) {
        self.operations.push(CanvasOperation::GlobalAlpha(value));
    }

    pub fn set_global_composite_operation<S: Into<String>>(&mut self, value: S) {
        self.operations.push(CanvasOperation::GlobalCompositeOperation(value.into()));
    }


    // colors and styles (see also the CanvasDrawingStyles interface)
    pub fn set_stroke_style<S: Into<String>>(&mut self, value: S) {
        self.operations.push(CanvasOperation::StrokeStyle(value.into()));
    }

    pub fn set_fill_style<S: Into<String>>(&mut self, value: S) {
        self.operations.push(CanvasOperation::FillStyle(value.into()));
    }


    // shadows
    pub fn set_shadow_offset_x(&mut self, value: f32) {
        self.operations.push(CanvasOperation::ShadowOffsetX(value));
    }

    pub fn set_shadow_offset_y(&mut self, value: f32) {
        self.operations.push(CanvasOperation::ShadowOffsetY(value));
    }

    pub fn set_shadow_blur(&mut self, value: f32) {
        self.operations.push(CanvasOperation::ShadowBlur(value));
    }

    pub fn set_shadow_color<S: Into<String>>(&mut self, value: S) {
        self.operations.push(CanvasOperation::ShadowColor(value.into()));
    }


    // rects
    pub fn clear_rect(&mut self, x: f32, y: f32, w: f32, h: f32) {
        self.operations.push(CanvasOperation::ClearRect(x, y, w, h));
    }

    pub fn fill_rect(&mut self, x: f32, y: f32, w: f32, h: f32) {
        self.operations.push(CanvasOperation::FillRect(x, y, w, h));
    }

    pub fn stroke_rect(&mut self, x: f32, y: f32, w: f32, h: f32) {
        self.operations.push(CanvasOperation::StrokeRect(x, y, w, h));
    }


    // path API (see also CanvasPathMethods)
    pub fn begin_path(&mut self) {
        self.operations.push(CanvasOperation::BeginPath);
    }

    pub fn fill(&mut self) {
        self.operations.push(CanvasOperation::Fill);
    }

    pub fn stroke(&mut self) {
        self.operations.push(CanvasOperation::Stroke);
    }

    pub fn clip(&mut self) {
        self.operations.push(CanvasOperation::Clip);
    }


    // text (see also the CanvasDrawingStyles interface)
    pub fn fill_text<S: Into<String>>(&mut self, text: S, x: f32, y: f32, max_width: Option<f32>) {
        self.operations.push(CanvasOperation::FillText(text.into(), x, y, max_width));
    }

    pub fn stroke_text<S: Into<String>>(&mut self, text: S, x: f32, y: f32, max_width: Option<f32>) {
        self.operations.push(CanvasOperation::StrokeText(text.into(), x, y, max_width));
    }


    // line caps/joins
    pub fn set_line_width(&mut self, value: f32) {
        self.operations.push(CanvasOperation::LineWidth(value));
    }

    pub fn set_line_cap(&mut self, value: LineCap) {
        self.operations.push(CanvasOperation::LineCap(value));
    }

    pub fn set_line_join(&mut self, value: LineJoin) {
        self.operations.push(CanvasOperation::LineJoin(value));
    }

    pub fn set_miter_limit(&mut self, value: f32) {
        self.operations.push(CanvasOperation::MiterLimit(value));
    }


    // dashed lines
    pub fn set_line_dash(&mut self, segments: Vec<f32>) {
        self.operations.push(CanvasOperation::SetLineDash(segments));
    }


    // text
    pub fn set_font<S: Into<String>>(&mut self, value: S) {
        self.operations.push(CanvasOperation::Font(value.into()));
    }

    pub fn set_text_align(&mut self, value: TextAlign) {
        self.operations.push(CanvasOperation::TextAlign(value));
    }

    pub fn set_text_baseline(&mut self, value: TextBaseline) {
        self.operations.push(CanvasOperation::TextBaseline(value));
    }


    // shared path API methods
    pub fn close_path(&mut self) {
        self.operations.push(CanvasOperation::ClosePath);
    }

    pub fn move_to(&mut self, x: f32, y: f32) {
        self.operations.push(CanvasOperation::MoveTo(x, y));
    }

    pub fn line_to(&mut self, x: f32, y: f32) {
        self.operations.push(CanvasOperation::LineTo(x, y));
    }

    pub fn quadratic_curve_to(&mut self, cpx: f32, cpy: f32, x: f32, y: f32) {
        self.operations.push(CanvasOperation::QuadraticCurveTo(cpx, cpy, x, y));
    }

    pub fn bezier_curve_to(&mut self, cp1x: f32, cp1y: f32, cp2x: f32, cp2y: f32, x: f32, y: f32) {
        self.operations.push(CanvasOperation::BezierCurveTo(cp1x, cp1y, cp2x, cp2y, x, y));
    }

    pub fn arc_to(&mut self, x1: f32, y1: f32, x2: f32, y2: f32, radius: f32) {
        self.operations.push(CanvasOperation::ArcTo(x1, y1, x2, y2, radius));
    }

    pub fn rect(&mut self, x: f32, y: f32, w: f32, h: f32) {
        self.operations.push(CanvasOperation::Rect(x, y, w, h));
    }

    pub fn arc(&mut self, x: f32, y: f32, radius: f32, start_angle: f32, end_angle: f32, counterclockwise: Option<bool>) {
        self.operations.push(CanvasOperation::Arc(x, y, radius, start_angle, end_angle, counterclockwise));
    }

}

impl Drop for Canvas {
    fn drop(&mut self) {
        js! {
            if (global.Canvas.Instance) {
                global.Canvas.Instance.drop();
                delete global.Canvas.Instance;
            }
        }
    }
}

