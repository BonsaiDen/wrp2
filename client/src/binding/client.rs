// STD Dependencies -----------------------------------------------------------
use std::cell::RefCell;


// External Dependencies ------------------------------------------------------
use core::{Client, Handler};
use stdweb::web::{set_timeout, window};


// Internal Dependencies ------------------------------------------------------
use crate::handler::GameHandler;


// Statics --------------------------------------------------------------------
thread_local! {
    static WEB_CLIENT: RefCell<WebClient> = RefCell::new(WebClient {
        inner: Client::new(GameHandler::new(), GameHandler::now())
    });
}


// Client Binding -------------------------------------------------------------
pub struct WebClient {
    inner: Client<GameHandler>
}

impl WebClient {

    pub fn web_socket_host() -> String {
        format!("{}:28786", window().location().unwrap().hostname().unwrap())
    }

    pub fn init() {
        Self::outer_tick();
        Self::outer_draw(0.0);
    }

    fn outer_tick() {
        WEB_CLIENT.with(|c| {
            let delay = c.borrow_mut().inner.tick(GameHandler::now());
            set_timeout(Self::outer_tick, delay);
        });
    }

    fn outer_draw(_: f64) {
        WEB_CLIENT.with(|c| {
            window().request_animation_frame(Self::outer_draw);
            c.borrow_mut().inner.draw(GameHandler::now());
        });
    }

}

