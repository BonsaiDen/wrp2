// STD Dependencies -----------------------------------------------------------
use std::rc::Rc;
use std::error::Error;
use std::cell::RefCell;


// External Dependencies ------------------------------------------------------
use core::JSONSocket;
use stdweb::web::{
    event::{
        IMessageEvent,
        SocketMessageEvent
    },
    IEventTarget,
    SocketBinaryType,
    SocketReadyState,
    WebSocket as RawWebSocket
};


// WebSocket Binding ----------------------------------------------------------
pub struct WebSocket {
    inner: RawWebSocket,
    message_queue: Rc<RefCell<Vec<String>>>
}

impl JSONSocket for WebSocket {

    fn try_new(url: &str) -> Result<Self, Box<Error>> where Self: Sized {

        let socket = RawWebSocket::new(format!("ws://{}", url).as_str())?;
        let message_queue = Rc::new(RefCell::new(Vec::new()));

        let inner_queue = message_queue.clone();
        socket.set_binary_type(SocketBinaryType::ArrayBuffer);
        socket.add_event_listener(move |e: SocketMessageEvent| {
            if let Some(text) = e.data().into_text() {
                inner_queue.borrow_mut().push(text);
            }
        });

        Ok(Self {
            inner: socket,
            message_queue
        })

    }

    fn close(&mut self) {
        if let SocketReadyState::Open = self.inner.ready_state() {
            self.inner.close();
        }
    }

    fn send(&mut self, text: String) {
        if let SocketReadyState::Open = self.inner.ready_state() {
            self.inner.send_text(text.as_str()).ok();
        }
    }

    fn receive(&mut self) -> Vec<String> {
        self.message_queue.borrow_mut().drain(0..).collect()
    }

    fn is_open(&self) -> bool {
        if let SocketReadyState::Open = self.inner.ready_state() {
            true

        } else {
            false
        }
    }

    fn is_closed(&self) -> bool {
        if let SocketReadyState::Closed = self.inner.ready_state() {
            true

        } else {
            false
        }
    }

}

