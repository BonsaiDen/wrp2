// STD Dependencies -----------------------------------------------------------
use std::cell::RefCell;
use std::collections::{HashMap, VecDeque};


// Statics --------------------------------------------------------------------
const INPUT_MAX_BUTTONS: usize = 3;

thread_local! {
    static INPUT_STATE: RefCell<VecDeque<InputEvent>> = RefCell::new(VecDeque::new());
}


// Enums ----------------------------------------------------------------------
pub struct Keyboard;
pub struct Mouse;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
enum InputAction {
    Hit,
    Pressed,
    Released
}

#[derive(Debug)]
enum InputEvent {
    KeyDown(u32),
    KeyUp(u32),
    MouseDown(u32, i32, i32),
    MouseUp(u32, i32, i32),
    Blur
}


// JS Input Bridge ------------------------------------------------------------
pub trait InputState<E, T> {
    fn hit(&self, t: T) -> bool where Self: Sized;
    fn pressed(&self, t: T) -> bool where Self: Sized;
    fn released(&self, t: T) -> bool where Self: Sized;
    fn any(&self, e: E) -> bool where Self: Sized;
    fn position(&self, e: E) -> Option<(i32, i32)> where Self: Sized;
}

pub struct Input {
    mouse_position: Option<(i32, i32)>,
    mouse_state: [InputAction; INPUT_MAX_BUTTONS],
    key_state: HashMap<Key, InputAction>
}

impl Input {
    pub fn new(element_id: &str) -> Self {
        js! {
            global.Input.Instance = new global.Input(@{element_id});
        };

        INPUT_STATE.with(|w| w.borrow_mut().clear() );

        Self {
            mouse_position: None,
            mouse_state: [InputAction::Released; INPUT_MAX_BUTTONS],
            key_state: HashMap::new()
        }
    }

    pub fn update(&mut self) {

        // Advance States
        for i in 0..INPUT_MAX_BUTTONS {
            self.mouse_state[i] = match self.mouse_state[i] {
                InputAction::Hit => InputAction::Pressed,
                InputAction::Pressed => InputAction::Pressed,
                InputAction::Released => InputAction::Released
            };
        }

        for state in self.key_state.values_mut() {
            *state = match *state {
                InputAction::Hit => InputAction::Pressed,
                InputAction::Pressed => InputAction::Pressed,
                InputAction::Released => InputAction::Released
            };
        }

        INPUT_STATE.with(|i| {
            while let Some(event) = i.borrow_mut().pop_front() {
                match event {
                    InputEvent::KeyUp(key) => self.update_key_state(Key::from(key as usize), false),
                    InputEvent::KeyDown(key) => self.update_key_state(Key::from(key as usize), true),
                    InputEvent::MouseUp(button, x, y) => {
                        self.mouse_position = Some((x, y));
                        self.update_mouse_state(button as usize, false);
                    },
                    InputEvent::MouseDown(button, x, y) => {
                        self.mouse_position = Some((x, y));
                        self.update_mouse_state(button as usize, true);
                    },
                    InputEvent::Blur => {
                        self.blur();
                    }
                }
            }
        });

    }

    fn get_mouse(&self, button: MouseButton, state: InputAction) -> bool {
        let s = self.mouse_state[button as usize];
        if state == InputAction::Pressed {
            if let InputAction::Pressed = s {
                true

            } else {
                s == InputAction::Hit
            }

        } else {
            s == state
        }
    }

    fn get_key(&self, key: Key, state: InputAction) -> bool {
        if let Some(s) = self.key_state.get(&key) {
            if state == InputAction::Pressed {
                if let InputAction::Pressed = *s {
                    true

                } else {
                    *s == InputAction::Hit
                }

            } else {
                *s == state
            }

        } else  {
            state == InputAction::Released
        }
    }

    fn blur(&mut self) {
        for i in 0..INPUT_MAX_BUTTONS {
            self.mouse_state[i] = match self.mouse_state[i] {
                InputAction::Hit => InputAction::Pressed,
                InputAction::Pressed => InputAction::Released,
                InputAction::Released => InputAction::Released
            };
        }

        for state in self.key_state.values_mut() {
            *state = match *state {
                InputAction::Hit => InputAction::Pressed,
                InputAction::Pressed => InputAction::Released,
                InputAction::Released => InputAction::Released
            };
        }
    }

    fn update_key_state(&mut self, key: Key, down: bool) {
        if down {
            let state = self.key_state.entry(key).or_insert(InputAction::Hit);
            *state = match *state {
                InputAction::Hit => InputAction::Hit,
                InputAction::Pressed => InputAction::Pressed,
                InputAction::Released => InputAction::Hit
            };

        } else {
            let state = self.key_state.entry(key).or_insert(InputAction::Released);
            *state = match *state {
                InputAction::Hit => InputAction::Hit,
                InputAction::Pressed => InputAction::Released,
                InputAction::Released => InputAction::Released
            };
        }
    }

    fn update_mouse_state(&mut self, index: usize, down: bool) {
        if down {
            self.mouse_state[index] = match self.mouse_state[index] {
                InputAction::Hit => InputAction::Hit,
                InputAction::Pressed => InputAction::Pressed,
                InputAction::Released => InputAction::Hit
            };

        } else {
            self.mouse_state[index] = match self.mouse_state[index] {
                InputAction::Hit => InputAction::Hit,
                InputAction::Pressed => InputAction::Released,
                InputAction::Released => InputAction::Released
            };
        }
    }

}

impl Drop for Input {
    fn drop(&mut self) {
        INPUT_STATE.with(|i| {
            i.borrow_mut().clear();
            js! {
                if (global.Input.Instance) {
                    global.Input.Instance.drop();
                    delete global.Input.Instance;
                }
            };
        });
    }
}

impl InputState<Keyboard, Key> for Input {

    fn hit(&self, key: Key) -> bool {
        self.get_key(key, InputAction::Hit)
    }

    fn pressed(&self, key: Key) -> bool {
        self.get_key(key, InputAction::Pressed)
    }

    fn released(&self, key: Key) -> bool {
        self.get_key(key, InputAction::Released)
    }

    fn any(&self, _: Keyboard) -> bool {
        false
    }

    fn position(&self, _: Keyboard) -> Option<(i32, i32)> {
        None
    }

}

impl InputState<Mouse, MouseButton> for Input {

    fn hit(&self, button: MouseButton) -> bool {
        self.get_mouse(button, InputAction::Hit)
    }

    fn pressed(&self, button: MouseButton) -> bool {
        self.get_mouse(button, InputAction::Pressed)
    }

    fn released(&self, button: MouseButton) -> bool {
        self.get_mouse(button, InputAction::Released)
    }

    fn any(&self, _: Mouse) -> bool {
        false
    }

    fn position(&self, _: Mouse) -> Option<(i32, i32)> {
        self.mouse_position
    }

}

#[allow(dead_code)]
#[js_export]
pub fn input_key_down(key: u32) {
    INPUT_STATE.with(|i| {
        i.borrow_mut().push_back(InputEvent::KeyDown(key));
    });
}

#[allow(dead_code)]
#[js_export]
pub fn input_key_up(key: u32) {
    INPUT_STATE.with(|i| {
        i.borrow_mut().push_back(InputEvent::KeyUp(key));
    });
}

#[allow(dead_code)]
#[js_export]
pub fn input_mouse_down(button: u32, x: i32, y: i32) {
    INPUT_STATE.with(|i| {
        i.borrow_mut().push_back(InputEvent::MouseDown(button, x, y));
    });
}

#[allow(dead_code)]
#[js_export]
pub fn input_mouse_up(button: u32, x: i32, y: i32) {
    INPUT_STATE.with(|i| {
        i.borrow_mut().push_back(InputEvent::MouseUp(button, x, y));
    });
}

#[allow(dead_code)]
#[js_export]
pub fn input_blur() {
    INPUT_STATE.with(|i| {
        i.borrow_mut().push_back(InputEvent::Blur);
    });
}


// Enums ----------------------------------------------------------------------
#[allow(dead_code)]
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum MouseButton {
    Left = 0,
    Right = 1,
    Middle = 2
}

#[allow(dead_code)]
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum Key {
    Unknown,
    Backspace,
    Tab,
    Enter,
    Shift,
    Ctrl,
    Alt,
    Pause,
    CapsLock,
    Escape,
    Space,
    PageUp,
    PageDown,
    End,
    Home,
    ArrowLeft,
    ArrowUp,
    ArrowRight,
    ArrowDown,
    Insert,
    Delete,
    Key0,
    Key1,
    Key2,
    Key3,
    Key4,
    Key5,
    Key6,
    Key7,
    Key8,
    Key9,
    KeyA,
    KeyB,
    KeyC,
    KeyD,
    KeyE,
    KeyF,
    KeyG,
    KeyH,
    KeyI,
    KeyJ,
    KeyK,
    KeyL,
    KeyM,
    KeyN,
    KeyO,
    KeyP,
    KeyQ,
    KeyR,
    KeyS,
    KeyT,
    KeyU,
    KeyV,
    KeyW,
    KeyX,
    KeyY,
    KeyZ,
    MetaLeft,
    MetaRight,
    Select,
    NumPad0,
    NumPad1,
    NumPad2,
    NumPad3,
    NumPad4,
    NumPad5,
    NumPad6,
    NumPad7,
    NumPad8,
    NumPad9,
    NumPadMultiply,
    NumPadAdd,
    NumPadSubtract,
    NumPadDecimal,
    NumPadDivide,
    F1,
    F2,
    F3,
    F4,
    F5,
    F6,
    F7,
    F8,
    F9,
    F10,
    F11,
    F12,
    NumLock,
    ScrollLock,
    Semicolon,
    Equalsign,
    Comma,
    Minus,
    Period,
    Slash,
    Backquote,
    Bracketleft,
    Backslash,
    BraketRight,
    Quote
}

impl Into<usize> for Key {
    fn into(self) -> usize {
        match self {
            Key::Unknown => 0,
            Key::Backspace => 8,
            Key::Tab => 9,
            Key::Enter => 13,
            Key::Shift=> 16,
            Key::Ctrl=> 17,
            Key::Alt=> 18,
            Key::Pause => 19,
            Key::CapsLock => 20,
            Key::Escape => 27,
            Key::Space => 32,
            Key::PageUp => 33,
            Key::PageDown => 34,
            Key::End => 35,
            Key::Home => 36,
            Key::ArrowLeft => 37,
            Key::ArrowUp => 38,
            Key::ArrowRight => 39,
            Key::ArrowDown => 40,
            Key::Insert => 45,
            Key::Delete => 46,
            Key::Key0 => 48,
            Key::Key1 => 49,
            Key::Key2 => 50,
            Key::Key3 => 51,
            Key::Key4 => 52,
            Key::Key5 => 53,
            Key::Key6 => 54,
            Key::Key7 => 55,
            Key::Key8 => 56,
            Key::Key9 => 57,
            Key::KeyA => 65,
            Key::KeyB => 66,
            Key::KeyC => 67,
            Key::KeyD => 68,
            Key::KeyE => 69,
            Key::KeyF => 70,
            Key::KeyG => 71,
            Key::KeyH => 72,
            Key::KeyI => 73,
            Key::KeyJ => 74,
            Key::KeyK => 75,
            Key::KeyL => 76,
            Key::KeyM => 77,
            Key::KeyN => 78,
            Key::KeyO => 79,
            Key::KeyP => 80,
            Key::KeyQ => 81,
            Key::KeyR => 82,
            Key::KeyS => 83,
            Key::KeyT => 84,
            Key::KeyU => 85,
            Key::KeyV => 86,
            Key::KeyW => 87,
            Key::KeyX => 88,
            Key::KeyY => 89,
            Key::KeyZ => 90,
            Key::MetaLeft => 91,
            Key::MetaRight => 92,
            Key::Select => 93,
            Key::NumPad0 => 96,
            Key::NumPad1 => 97,
            Key::NumPad2 => 98,
            Key::NumPad3 => 99,
            Key::NumPad4 => 100,
            Key::NumPad5 => 101,
            Key::NumPad6 => 102,
            Key::NumPad7 => 103,
            Key::NumPad8 => 104,
            Key::NumPad9 => 105,
            Key::NumPadMultiply => 106,
            Key::NumPadAdd => 107,
            Key::NumPadSubtract => 109,
            Key::NumPadDecimal => 110,
            Key::NumPadDivide => 111,
            Key::F1 => 112,
            Key::F2 => 113,
            Key::F3 => 114,
            Key::F4 => 115,
            Key::F5 => 116,
            Key::F6 => 117,
            Key::F7 => 118,
            Key::F8 => 119,
            Key::F9 => 120,
            Key::F10 => 121,
            Key::F11 => 122,
            Key::F12 => 123,
            Key::NumLock => 144,
            Key::ScrollLock => 145,
            Key::Semicolon => 186,
            Key::Equalsign => 187,
            Key::Comma => 188,
            Key::Minus => 189,
            Key::Period => 190,
            Key::Slash => 191,
            Key::Backquote => 192,
            Key::Bracketleft => 219,
            Key::Backslash => 220,
            Key::BraketRight => 221,
            Key::Quote => 222
        }
    }
}

impl From<usize> for Key {
    fn from(key: usize) -> Key {
        match key {
            8 => Key::Backspace,
            9 => Key::Tab,
            13 => Key::Enter,
            16 => Key::Shift,
            17 => Key::Ctrl,
            18 => Key::Alt,
            19 => Key::Pause,
            20 => Key::CapsLock,
            27 => Key::Escape,
            32 => Key::Space,
            33 => Key::PageUp,
            34 => Key::PageDown,
            35 => Key::End,
            36 => Key::Home,
            37 => Key::ArrowLeft,
            38 => Key::ArrowUp,
            39 => Key::ArrowRight,
            40 => Key::ArrowDown,
            45 => Key::Insert,
            46 => Key::Delete,
            48 => Key::Key0,
            49 => Key::Key1,
            50 => Key::Key2,
            51 => Key::Key3,
            52 => Key::Key4,
            53 => Key::Key5,
            54 => Key::Key6,
            55 => Key::Key7,
            56 => Key::Key8,
            57 => Key::Key9,
            65 => Key::KeyA,
            66 => Key::KeyB,
            67 => Key::KeyC,
            68 => Key::KeyD,
            69 => Key::KeyE,
            70 => Key::KeyF,
            71 => Key::KeyG,
            72 => Key::KeyH,
            73 => Key::KeyI,
            74 => Key::KeyJ,
            75 => Key::KeyK,
            76 => Key::KeyL,
            77 => Key::KeyM,
            78 => Key::KeyN,
            79 => Key::KeyO,
            80 => Key::KeyP,
            81 => Key::KeyQ,
            82 => Key::KeyR,
            83 => Key::KeyS,
            84 => Key::KeyT,
            85 => Key::KeyU,
            86 => Key::KeyV,
            87 => Key::KeyW,
            88 => Key::KeyX,
            89 => Key::KeyY,
            90 => Key::KeyZ,
            91 => Key::MetaLeft,
            92 => Key::MetaRight,
            93 => Key::Select,
            96 => Key::NumPad0,
            97 => Key::NumPad1,
            98 => Key::NumPad2,
            99 => Key::NumPad3,
            100 => Key::NumPad4,
            101 => Key::NumPad5,
            102 => Key::NumPad6,
            103 => Key::NumPad7,
            104 => Key::NumPad8,
            105 => Key::NumPad9,
            106 => Key::NumPadMultiply,
            107 => Key::NumPadAdd,
            109 => Key::NumPadSubtract,
            110 => Key::NumPadDecimal,
            111 => Key::NumPadDivide,
            112 => Key::F1,
            113 => Key::F2,
            114 => Key::F3,
            115 => Key::F4,
            116 => Key::F5,
            117 => Key::F6,
            118 => Key::F7,
            119 => Key::F8,
            120 => Key::F9,
            121 => Key::F10,
            122 => Key::F11,
            123 => Key::F12,
            144 => Key::NumLock,
            145 => Key::ScrollLock,
            186 => Key::Semicolon,
            187 => Key::Equalsign,
            188 => Key::Comma,
            189 => Key::Minus,
            190 => Key::Period,
            191 => Key::Slash,
            192 => Key::Backquote,
            219 => Key::Bracketleft,
            220 => Key::Backslash,
            221 => Key::BraketRight,
            222 => Key::Quote,
            _ => Key::Unknown
        }
    }
}

