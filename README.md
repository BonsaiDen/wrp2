# P2P Multiplayer "Library" with State Rollback and Matchmaking Server

## What

The "core" library is frontend independent with the state rollback being based on: http://blog.hypersect.com/rollback-networking-in-inversus/

The "client" frontend is browser based and UDP P2P connections are implemented via WebRTC Data Channels.

The matchmaking server is written in Rust and messages are exchanged between clients via WebSocket connections.


## How

Client

```
npm install .
parcel client/index.html --port=28785
```

Server
```
cargo run server
```


# Copyright

Copyright 2018 Ivo Wetzel

